import {
     ChangeDetectorRef, Component, OnDestroy, OnInit,
    ViewEncapsulation
} from '@angular/core';

import { animate, state, style, transition, trigger } from '@angular/animations';
import {Subscription} from "rxjs/Rx";
import {AuthService} from "./firebase/auth.service";
import {LogService} from "./shared/log/log.service";
import {LocationService} from "./shared/geo/location.service";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    encapsulation: ViewEncapsulation.None,
    //changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [
        trigger('loading', [
            // ...
            state('loads', style({
                opacity: 1,
            })),
            state('loaded', style({
                opacity: 0,
            })),
            transition('loads => loaded', [
                animate('.3s')
            ]),
            transition('loaded => loads', [
                animate('.3s')
            ]),
        ]),
    ],
})
export class AppComponent implements OnInit, OnDestroy {
    public isAuthenticated: boolean;
    isLoading = true;
    bgImgLoaded = false;
    private subs: Subscription = new Subscription();
    private intervalId;

    constructor(
        private detector: ChangeDetectorRef,
        private authService: AuthService,
        public geoSvc: LocationService,
        public logSvc: LogService,
    ) {
        /*geoSvc.updateGeoAfterLatLonChanged();
        this.isAuthenticated = authService.isLogged;
        this.intervalId = setInterval(() => {
            logSvc.sendToServerInInterval();
        }, 1000*60*60);*/
    }

    ngOnInit(): void {
        this.allowLoadIframe();
        this.bgImgLoaded = true;
        this.isLoading = false;
    }

    ngOnDestroy(): void {
        this.subs.unsubscribe();
        clearInterval(this.intervalId);
    }

    authenticate(event) {
        this.isAuthenticated = event;
        setTimeout(() => {
            this.detector.detectChanges();
        }, 500)
    }

    private allowLoadIframe() {
        chrome.tabs.getCurrent(function (tab) {
            const tabId = tab.id;

            chrome.webRequest.onHeadersReceived.addListener(
                function (info) {
                    const headers = info.responseHeaders || [];

                    for (let i = headers.length - 1; i >= 0; --i) {
                        const header = headers[i].name.toLowerCase();

                        if (header === 'x-frame-options' || header === 'frame-options') {
                            headers.splice(i, 1);
                        } else if (header === 'content-security-policy') {
                            // Remove any frame-ancestors CSP directives, this is actually spec-compliant
                            headers[i].value = headers[i].value.split(';').filter(function (e) {
                                return e.trim().toLowerCase().indexOf('frame-ancestors') !== 0;
                            }).join(';');
                        }
                    }

                    return {
                        responseHeaders: headers
                    };
                },
                {
                    tabId: tabId,
                    urls: ['*://*/*'],
                    types: ['sub_frame']
                },
                ['blocking', 'responseHeaders']
            );
        });

        return false;
    }
}
