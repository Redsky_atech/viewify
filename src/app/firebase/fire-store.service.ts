import {Injectable, OnDestroy} from "@angular/core";
import {
    AngularFirestore, AngularFirestoreCollection, DocumentReference
} from "@angular/fire/firestore";
import {Observable, Subject, Subscription} from "rxjs/Rx";
import {AngularFireAuth} from "@angular/fire/auth";
import { map, take} from "rxjs/internal/operators";
import {ChromeStorageService} from "../shared/storage/services/chrome-storage.service";
import {base_storage_keys} from "../shared/model/storage-keys";
import {WriteBatch} from "firebase/firestore";
import {DeskService} from "../desk/desk.service";

@Injectable()
export class FireStoreService implements OnDestroy {
    private keysCollection: AngularFirestoreCollection;
    private userDoc: DocumentReference;
    private settingsDoc: DocumentReference;
    private logDoc: DocumentReference;
    private errorDoc: DocumentReference;
    syncWithFire: Subject<any> = new Subject<any>();
    private subs: Subscription = new Subscription();

    constructor(private afs: AngularFirestore,
                private deskSvc: DeskService,
                public authFire: AngularFireAuth,
                public storage: ChromeStorageService){
        this.afs.firestore.enablePersistence({experimentalTabSynchronization: true})
            .then(result=> {})
            .catch(error => {
                console.log(error);
            });
    }

    ngOnDestroy(): void {
        this.subs.unsubscribe();
    }

    getRootData(): Promise<any> {
        return new Promise<any>(resolve => {
            let storagePath = "users/"+this.authFire.auth.currentUser.uid+"/keys";
            this.keysCollection = this.afs.collection(storagePath);
            this.userDoc = this.afs.collection("users").doc(this.authFire.auth.currentUser.uid).ref;
            this.settingsDoc = this.afs.collection(storagePath).doc("settings").ref;
            this.logDoc = this.afs.collection(storagePath).doc("log").ref;
            this.errorDoc = this.afs.collection(storagePath).doc("errors").ref;

            this.firstLoad().then(() => resolve());
        });
    }

    clearRootData() {
        this.userDoc = null;
        this.keysCollection = null;
    }

    getAllData(): Promise<any> {
        return new Promise<any>(resolve => {
            this.getSingleDocData("settings")
                .then((settings: any) => {
                    if (settings) {
                        this.storage.get(base_storage_keys.sync_keys)
                            .pipe(take(1))
                            .subscribe(keys => {
                                //array of keys from app parts that need to update
                                let changed = [];
                                if (!keys) keys = {};
                                Object.keys(settings).forEach((key, ind, arr) => {
                                    //check if local data was changed and firestore didn't sync with it
                                    //then skip this data
                                    if (!keys[key]) {
                                        this.storage.get(key)
                                            .pipe(take(1))
                                            .subscribe(value => {
                                                //if local data is equal to data from firestore then skip it and don't update local
                                                if (key == base_storage_keys.links) {
                                                    if (value.todoItems && value.todoItems[0] && settings[key] && settings[key].todoItems && settings[key].todoItems[0]) {
                                                        value.todoItems[0].children.forEach(localFolder => {
                                                            if (localFolder && localFolder.children) {
                                                                //find same folder from firestore
                                                                let fireFolder = settings[key].todoItems[0].children.find(f => f.id == localFolder.id);

                                                                if (fireFolder) {
                                                                    //copy icon from local item into fire-item to compere data
                                                                    localFolder.children.forEach(item => {
                                                                        if (item && item.link && item.link.icon) {
                                                                            let fireItem = fireFolder.children.find(site => site.id == item.id);
                                                                            if (fireItem) fireItem.link.icon = item.link.icon;
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        });
                                                        //update local data if firestore data is different from local
                                                        if (JSON.stringify(value) != JSON.stringify(settings[key])) {
                                                            this.storage.set(key, settings[key]);
                                                            changed.push(key);
                                                        }
                                                    }
                                                } else if (JSON.stringify(settings[key]) != JSON.stringify(value)) {
                                                    this.storage.set(key, settings[key]);
                                                    changed.push(key);
                                                }
                                                //update all needed app data in the end of array
                                                if (ind == arr.length-1) {
                                                    setTimeout(() => {
                                                        this.syncWithFire.next({type: "data", keys: changed});
                                                    }, 500);
                                                    resolve();
                                                }
                                            });
                                    } else {
                                        //update all needed app data in the end of array
                                        if (ind == arr.length-1) {
                                            setTimeout(() => {
                                                this.syncWithFire.next({type: "data", keys: changed});
                                            }, 300);
                                            resolve();
                                        }
                                    }
                                });
                            });
                    } else resolve();
                });
        });
    }

    private listenFireChanges(): Promise<any> {
        return new Promise<any>(resolve => {
            this.subs.add(this.keysCollection.snapshotChanges()
                .subscribe((actions: any) => {
                    if (actions.length > 0) {
                        let mapData: any = {};
                        actions.forEach(action => {
                            if (action.payload.doc.exists) {
                                //this.storage.set(action.payload.doc.id, action.payload.doc.data());
                                if (action.payload.doc.metadata.fromCache === false) {
                                    let key: string = action.payload.doc.id;
                                    mapData[key] = action.payload.doc.data();
                                }
                            }
                        });
                        if (mapData) {
                            Object.keys(mapData).forEach((key, ind, arr) => {
                                this.storage.set(key, mapData[key]);
                                if (ind == arr.length-1) {
                                    this.syncWithFire.next({type: "data"});
                                    resolve();
                                }
                            });
                        } else resolve();
                    } else resolve();
                }, error => resolve()));
        });
    }

    private isFirstUpload(): Promise<any> {
        return new Promise<any>(resolve => {
            this.storage.get(base_storage_keys.first_local_upload)
                .pipe(take(1))
                .subscribe(version => {
                    if (version == '1.0.0') resolve('no');
                    else {
                        this.getSingleDocData(base_storage_keys.first_fire_upload)
                            .then((data: any) => {
                                if (data && data.version == '1.0.0') {
                                    this.storage.set(base_storage_keys.first_local_upload, '1.0.0');
                                    resolve('no');
                                } else resolve('yes');
                            })
                    }
                });
        });
    }

    private firstLoad(): Promise<any> {
        return new Promise<any>(resolve => {
            this.isFirstUpload()
                .then((isFirst: string) => {
                    if (isFirst == 'no') {
                        this.getAllData()
                            .then(() => {
                                this.storage.set(base_storage_keys.first_local_upload, '1.0.0');
                                resolve();
                            }).catch(error => resolve());
                    } else {
                        this.deskSvc.loadDefaultStorageData()
                            .then(() => {
                                this.syncWithFire.next({type: "data"});
                                this.storage.set(base_storage_keys.first_local_upload, '1.0.0');
                                this.setAllData()
                                    .then((result: any) => {
                                        if (result == 'success') {
                                            this.setDocData(base_storage_keys.first_fire_upload, {version: '1.0.0'});
                                        }
                                    });
                                resolve();
                            }).catch(error => resolve());
                    }
                }).catch(error => {
                    console.log(error);
                    resolve();
                    //throw new Error(error.message);
            });
        });
    }

    setAllData(): Promise<string> {
        return new Promise<string>(resolve => {
            this.storage.get(base_storage_keys.sync_keys)
                .pipe(take(1))
                .subscribe((storedKeys: any) => {
                    if (!storedKeys) resolve();
                    else {
                        this.getSingleDocData("settings")
                            .then((settings: any) => {
                                if (!settings) settings = {};
                                this.gatherStorageToObject(settings, storedKeys)
                                    .then(() => {
                                        setTimeout(() => {
                                            this.setDocData("settings", settings);
                                            this.storage.set(base_storage_keys.sync_keys, storedKeys);
                                            this.syncWithFire.next({type: "marker"});
                                        }, 1500);
                                        resolve("success");
                                    });
                            });
                    }
                });
        });
    }

    getSingleDocData(key: string): Promise<any> {
        return this.keysCollection.doc(key).get()
            .pipe(map(doc => {
                if (doc.exists) {
                    return doc.data();
                } else return null;
            })).toPromise();
    }

    setDocData(key: string, data: any): Promise<any> {
        return this.keysCollection.doc(key).set(data, {merge: true})
    }

    removeDoc(key: string): Promise<any> {
        return this.keysCollection.doc(key).delete();
    }

    getBackupData(): Observable<any> {
        return this.keysCollection.doc("settings").get({source: "cache"})
            .pipe(map(doc => {
                if (doc.exists) {
                    return doc.data();
                } else return null;
            }));
    }

    changeUserADMIN(id: string) {
        this.keysCollection = this.afs.collection("users/"+id+"/keys");
        this.userDoc = this.afs.collection("users").doc(id).ref;
        this.getAllData();
    }

    changeToOwnIdADMIN() {
        this.keysCollection = this.afs.collection("users/"+this.authFire.auth.currentUser.uid+"/keys");
        this.userDoc = this.afs.collection("users").doc(this.authFire.auth.currentUser.uid).ref;
        this.firstLoad();
    }

    private gatherStorageToObject(settings: any, storedKeys: any): Promise<any> {
        return new Promise<any>(resolve => {
            Object.keys(storedKeys).forEach((key, ind, arr) => {
                if (key) {
                    if (storedKeys[key] == 'remove') {
                        delete settings[key];
                        delete storedKeys[key];
                    } else if (storedKeys[key] == 'save') {
                        this.storage.get(key)
                            .pipe(take(1))
                            .subscribe(data => {
                                if (data && key == base_storage_keys.links) {
                                    if (data.todoItems && data.todoItems[0].children) {
                                        data.todoItems[0].children.forEach(folder => {
                                            if (folder.children) {
                                                folder.children.forEach(item => {
                                                    if (item && item.link && item.link.icon) {
                                                        item.link.icon = null;
                                                    }
                                                });
                                            }
                                        });
                                    }
                                }
                                settings[key] = data;
                                delete storedKeys[key];
                            });
                    }
                }
            });
            resolve();
        });
    }
}
