import 'rxjs/add/operator/map';

import {Injectable, OnDestroy} from '@angular/core';
import {AngularFireAuth} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {UserModel} from "../auth/model/user.model";
import {Observable, Subject} from "rxjs/Rx";
import {UserType} from "../shared/model/user-type";
import {base_storage_keys} from "../shared/model/storage-keys";
import {User} from "firebase";
import {ChromeStorageService} from "../shared/storage/services/chrome-storage.service";
import {take} from "rxjs/internal/operators";
import {MatSnackBar} from "@angular/material";
import {FireStoreService} from "./fire-store.service";
import {TabsService} from "../shared/storage/services/tabs.service";

@Injectable()
export class AuthService implements OnDestroy {
    userType: UserType = UserType.anonymous;
    authenticatedSubject: Subject<boolean> = new Subject<boolean>();
    isLogged: boolean = false;
    currentUser: User;
    verified: boolean = false;
    private intervalId: any;
    /*readonly keys = [
        base_storage_keys.pages
    ]*/

    registrationForm: boolean = true;
    submitName: string = "Sign Up";

    constructor(private afAuth: AngularFireAuth,
                private storage: ChromeStorageService,
                public fireStore: FireStoreService,
                private snackbar: MatSnackBar,
                private tabsSvc: TabsService) {
        afAuth.auth.setPersistence(firebase.auth.Auth.Persistence.LOCAL);
        afAuth.auth.onAuthStateChanged((nextState) => {
            this.currentUser = afAuth.auth.currentUser;
            if (nextState) {
                this.verified = nextState.emailVerified;
                if (this.verified) {
                    this.showDesk();
                    this.fireStore.getRootData()
                        .then(() => {
                            this.syncWithFire();
                            tabsSvc.endTabSync('out')
                                .pipe(take(1))
                                .subscribe(() => {
                                    tabsSvc.startTabSync();
                                });
                        });
                } else {
                    this.openSnack("Email isn't verified!", 'error', 'snack_error');
                    afAuth.auth.currentUser.sendEmailVerification();
                    this.signOut();
                }
            } else {
                this.verified = false;
                this.isLogged = false;
                this.fireStore.clearRootData();
                this.userType = UserType.anonymous;
                this.authenticatedSubject.next(false);
                tabsSvc.endTabSync('logged')
                    .pipe(take(1))
                    .subscribe(() => {
                        tabsSvc.startTabSync();
                    });
            }
        });
    }

    ngOnDestroy(): void {
        clearInterval(this.intervalId);
    }

    showDesk() {
        this.isLogged = true;
        this.userType = UserType.logedUser;
        this.authenticatedSubject.next(true);
    }

    authOnStartpup(): Promise<any> {
        return new Promise<any>(resolve => {
            this.afAuth.idToken.take(1).subscribe(id => {
                if (id) {
                    this.isLogged = true;
                    resolve();
                } else {
                    this.storage.get(base_storage_keys.isAnonymous)
                        .pipe(take(1))
                        .subscribe(data => {
                            if (data) this.isLogged = data;
                            resolve();
                        });
                }
            });
        });
    }

    signIn(provider: firebase.auth.AuthProvider): Promise<any> {
        return this.afAuth.auth.signInWithPopup(provider)
            .then((credential: firebase.auth.UserCredential) => {
                return true;
            })
            .catch(error => {
                console.log('ERROR @ AuthService#signIn() :', error);
                return false;
            });
    }

    signInAnonymously(): Promise<any> {
        return this.afAuth.auth.signInAnonymously()
            .then((credential: firebase.auth.UserCredential) => {
                return true;
            })
            .catch(error => {
                console.log('ERROR @ AuthService#signInAnonymously() :', error);
                return false;
            });
    }

    signInWithGithub(): Promise<any> {
        return this.signIn(new firebase.auth.GithubAuthProvider());
    }

    signInWithGoogle(): Promise<any> {
        return this.signIn(new firebase.auth.GoogleAuthProvider());
    }

    signInWithTwitter(): Promise<any> {
        return this.signIn(new firebase.auth.TwitterAuthProvider());
    }

    signInWithFacebook(): Promise<any> {
        return this.signIn(new firebase.auth.FacebookAuthProvider());
    }

    signInWithEmailPassword(user: UserModel): Promise<any> {
        return this.afAuth.auth.signInWithEmailAndPassword(user.email, user.pass)
            .then(credentials => {
                if (credentials) {
                    return true;
                } else return false;
            }).catch(error => {
                this.openSnack(error.message, 'error', 'snack_error');
            });
    }

    changePassword(newPass: string): Promise<any> {
        return this.afAuth.auth.currentUser.updatePassword(newPass);
    }

    changeEmail(newEmail: string): Promise<any> {
        return this.afAuth.auth.currentUser.updateEmail(newEmail);
    }

    createNewUser(user: UserModel): Promise<any> {
        return this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.pass)
            .then(credentials => {
                if (credentials) {
                    credentials.user.sendEmailVerification()
                        .then(() => {
                            this.signOut();
                            return true;
                        }).catch(error => {
                        this.openSnack(error.message, 'error', 'snack_error');
                    });
                    return true;
                } else return false;
            }).catch(error => {
                this.openSnack(error.message, 'error', 'snack_error');
            });
    }

    signOut(): void {
        this.afAuth.auth.signOut();
    }

    passworRecovery(email: string) {
        if (email && email.match('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')) {
            this.afAuth.auth.sendPasswordResetEmail(email)
                .then(() => {
                    this.openSnack('Recovery message was sent to your email!', 'success', 'snack_success');
                }).catch(error => {
                this.openSnack(error.message, 'error', 'snack_error');
            });
        } else this.openSnack('Invalid email!', 'error', 'snack_error');

    }

    private openSnack(message: string, action: string, styleClass: string) {
        this.snackbar.open(message, action, {
            duration: 4500,
            panelClass: ['snack-bar_base', styleClass]
        });
    }

    clearInterval() {
        clearInterval(this.intervalId);
    }

    syncWithFire() {
        if (this.afAuth.auth.currentUser) {
            this.storage.get(base_storage_keys.sync_last)
                .subscribe((last: number) => {
                    if (last && (new Date().getTime() - last) > 1000 * 60 * 60 * 3) {
                        this.fireStore.setAllData()
                            .then((result) => {
                                if (result == "success") {
                                    this.storage.set(base_storage_keys.sync_last, new Date().getTime());
                                    this.openSnack('synchronized with server!', 'success', 'snack_success');
                                } else if (result == 'error') {
                                    this.storage.set(base_storage_keys.sync_last, new Date().getTime());
                                    this.openSnack('synchronization failed!', 'error', 'snack_error');
                                }
                            });
                    }
                });

            this.intervalId = setInterval(() => {
                this.fireStore.setAllData()
                    .then((result) => {
                        if (result == "success") {
                            this.storage.set(base_storage_keys.sync_last, new Date().getTime());
                            this.openSnack('synchronized with server!', 'success', 'snack_success');
                        } else if (result == 'error') {
                            this.storage.set(base_storage_keys.sync_last, new Date().getTime());
                            this.openSnack('synchronization failed!', 'error', 'snack_error');
                        }
                    });
            }, 1000 * 60 * 60 * 3);
        }
    }
}
