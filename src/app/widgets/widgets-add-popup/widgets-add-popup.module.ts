import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {WidgetsAddPopupComponent} from './component/widgets-add-popup.component';
import {DragAndDropModule} from 'angular-draggable-droppable';
import {WidgetsDynamicModule} from '../widgets-dynamic/widgets-dynamic.module';
import {
    MatAutocompleteModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatTabsModule
} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        DragAndDropModule,
        WidgetsDynamicModule,

        MatInputModule,
        MatButtonModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatIconModule,
        MatSelectModule,
        MatTabsModule,
    ],
    declarations: [
        WidgetsAddPopupComponent,
    ],
    exports: [
        WidgetsAddPopupComponent,
    ],
})
export class WidgetsAddPopupModule {
}
