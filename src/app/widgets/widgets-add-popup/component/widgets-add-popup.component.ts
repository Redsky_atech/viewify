import { DOCUMENT } from '@angular/common';
import {
    Component, EventEmitter, Inject, Input, OnDestroy, OnInit, Output, ViewEncapsulation
} from '@angular/core';

import { Widget } from '../../../shared/base-structures/widget';
import { WidgetDragging } from '../../../shared/base-structures/widget-dragging';
import {FormControl} from "@angular/forms";
import {Observable, Subscription} from "rxjs/Rx";
import {map, take} from "rxjs/internal/operators";
import {WidgetType} from "../../../shared/base-structures/widget-type";
//import {WidgetRssComponent} from "../../widget-rss/component/widget-rss.component";
//import {WidgetRSSSettings} from "../../widget-rss/widget-rss-settings";
//import {RssService} from "../../widget-rss/rss.service";
import {widgetsList} from "../../widget/model/widgets.meta";
import {LocationService} from "../../../shared/geo/location.service";
import {PageInfo} from "../../../desk/model/page-info";
import {base_storage_keys} from "../../../shared/model/storage-keys";
import {FullWrapper, IconWrapper, PagesWrapper} from "../../../desk/component/desk.component";
import {ChromeStorageService} from "../../../shared/storage/services/chrome-storage.service";
import {TabsService} from "../../../shared/storage/services/tabs.service";
import {SnackService} from "../../../shared/utils/snack.service";
import {AngularFireAuth} from "@angular/fire/auth";
import {SharedService} from "../../../shared/utils/shared.service";
import {FireStoreService} from "../../../firebase/fire-store.service";

@Component({
    selector: 'app-widgets-add-popup',
    templateUrl: './widgets-add-popup.component.html',
    styleUrls: ['./widgets-add-popup.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class WidgetsAddPopupComponent implements OnInit, OnDestroy {
    readonly iconList = {
        'w-calculator': {className: 'icon ion-ios-calculator'},
        'w-movies': {className: 'icon ion-md-film'},
        'w-events': {className: 'icon ion-md-calendar'},
        'w-news': {className: 'icon ion-md-paper'},
        'w-weather': {className: 'icon ion-ios-cloudy-night'},
        'w-todo': {className: 'icon ion-ios-list-box'},
        'w-time': {className: 'icon ion-md-time'},
        'w-youtube-music': {className: 'icon ion-ios-musical-notes'},
        'w-whatsapp': {className: 'icon ion-logo-whatsapp'},
        'w-stock': {className: 'icon ion-ios-trending-up'},
        'w-translate': {className: 'material-icons', tagText: 'g_translate'},
        'w-currency': {className: 'icon ion-ios-cash'},
        'w-g-mail': {className: 'icon ion-ios-mail-unread'},
        'w-facebook': {className: 'icon ion-logo-facebook'},
        'w-google-analytics': {className: 'icon ion-md-analytics'},
        'w-youtube': {className: 'icon ion-logo-youtube'},
        'w-instagram': {className: 'icon ion-logo-instagram'},
        'w-twitter': {className: 'icon ion-logo-twitter'},
        'w-reddit': {className: 'icon ion-logo-reddit'},
        'w-g-classroom': {className: 'material-icons', tagText: 'school'},
        'w-g-keep': {className: 'icon ion-ios-bulb'},
        'w-g-maps': {className: 'icon ion-md-map'},
        'w-custom': {className: 'icon ion-logo-chrome'},
        'w-quotes': {className: 'icon ion-ios-quote'},
        'w-calendar': {className: 'icon ion-ios-calendar'},
        'w-drive': {className: 'fab fa-google-drive'},
        'w-rss': {className: 'icon ion-logo-rss'},
        'w-dictionary': {className: 'icon ion-ios-journal'},
        'w-radio': {className: 'icon ion-ios-radio'},
        'w-unit-converter': {className: 'icon ion-ios-swap'},
    }

    @Input() widgets: Widget[];
    @Input() viewIndex: number;
    @Input() currentView: string;
    @Output() dragStart: EventEmitter<WidgetDragging> = new EventEmitter();
    @Output() dragEnd: EventEmitter<WidgetDragging> = new EventEmitter();

    searchControl = new FormControl();
    forSearchWidget: Observable<Widget[]>;
    widgetIndex: number = 0;
    canDrag = false;
    isDragging = false;
    showTabContent: boolean = false;

    nativeWidgets: Widget[];
    webWidgets: Widget[];
    rssWidgets: Widget[];

    fullViews: PageInfo[] = [];
    homePage: PageInfo = new PageInfo();
    selectedPage: PageInfo;
    private homeKey = base_storage_keys.iconView_key+"left";
    private existsInHome: boolean = false;
    private sub: Subscription = new Subscription();

    widgetTypeSelected: string = 'all';
    widgetCatSelected: string = 'all';
    widgetCountrySelected: string = 'World';
    selectField: FormControl = new FormControl();

    widgetTypeOptions: any = [
        {value: 'all', text: 'All Widgets'},
        {value: WidgetType.native, text: 'Native Widgets'},
        {value: WidgetType.web, text: 'Web Widgets'},
        {value: WidgetType.rss, text: 'RSS Widgets'}
    ];
    allCategoryOptions: any = [{value: 'all', text: 'All Category'}];
    allCountryOptions: any = [{value: 'World', text: 'World'}];
    webCategoryOptions: any = [{value: 'all', text: 'All Category'}];
    nativeCategoryOptions: any = [{value: 'all', text: 'All Category'}];
    rssCategoryOptions: any = [{value: 'all', text: 'All Category'}];
    webCountryOptions: any = [{value: 'World', text: 'World'}];
    nativeCountryOptions: any = [{value: 'World', text: 'World'}];
    rssCountryOptions: any = [{value: 'WORLD', text: 'World'}];
    widgetsForFilter: Widget[];

    constructor(@Inject(DOCUMENT) public document: Document,
                private geoSvc: LocationService,
                private storage: ChromeStorageService,
                private tabsSvc: TabsService,
                private snack: SnackService,
                private afAuth: AngularFireAuth,
                private sharedSvc: SharedService,
                private fireStore: FireStoreService) {
        this.forSearchWidget = this.searchControl.valueChanges
            .pipe(map((inputText: string) => {
                if(inputText.length > 0) {
                    return this.widgets.filter((widget: Widget) => {
                        if(widget.settings.widgetDisabled) {
                            return false;
                        }
                        return widget.title.toLowerCase().startsWith(inputText.toLowerCase());
                    });
                } else return [];
            }));

        this.selectField.valueChanges
            .subscribe(page => {
                if (page) this.selectedPage = page;
            });

        if (this.afAuth.auth.currentUser) {
            this.sub.add(this.fireStore.syncWithFire.subscribe((reset) => {
                if (reset.type == "marker") {

                } else if (reset.type == "data" && (!reset.keys || reset.keys.includes(base_storage_keys.pages))) {
                    this.prepareFilterData();
                    this.getPages()
                        .then(() => {
                            if (this.currentView == "full") this.selectField.setValue(this.fullViews.find(p => p.index == this.viewIndex));
                            else if (this.currentView == "icon" && this.fullViews.length > 0) this.selectField.setValue(this.fullViews[0]);
                        });
                }
            }));
        }
    }

    ngOnInit() {
        this.prepareFilterData();
        this.getPages()
            .then(() => {
                if (this.currentView == "full") this.selectField.setValue(this.fullViews.find(p => p.index == this.viewIndex));
                else if (this.currentView == "icon" && this.fullViews.length > 0) this.selectField.setValue(this.fullViews[0]);
            });
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }

    iconClass(key: string): string {
        if (key.startsWith('w-rss')) return this.iconList['w-rss'].className;
        else return this.iconList[key].className;
    }

    iconText(key: string): string {
        if (key.startsWith('w-rss')) return '';
        else return this.iconList[key].tagText;
    }

    onDragStart($event, type, widget) {
        this.isDragging = true;
        this.dragStart.emit({$event, type, widget});
    }

    onDragEnd($event, type) {
        this.isDragging = true;
        this.dragEnd.emit({$event, type});
    }

    onDragIconOver($event) {
        if (!this.isDragging) {
            this.canDrag = true;
        }
    }

    onDragIconOut($event) {
        if (!this.isDragging) {
            this.canDrag = false;
        }
    }

    public onUserDataChanged(widget: Widget, userData: any): void {
        const wd = this.widgets.find(x => x === widget);
        if (wd) {
            wd.userData = userData;
        }
    }

    getValidateDragFn() {
        return coords => this.validateDrag(coords);
    }

    validateDrag(coords) {
        return this.canDrag;
    }

    displayFn(widget?: Widget): string | undefined {
        return "";
    }

    changeTab(widget: Widget) {
        this.showTabContent = true;
        this.widgets.find((w, i) => {
            if (widget.title == w.title) {
                this.widgetIndex = i;
                return true;
            }
        });
    }

    onTabChange(widget: Widget) {
        if (!widget.settings.widgetDisabled) this.showTabContent = true;
    }

    prepareFilterData() {
        this.nativeWidgets = this.widgets.filter(widget => widget.settings.types.includes(WidgetType.native));
        this.webWidgets = this.widgets.filter(widget => widget.settings.types.includes(WidgetType.web));
        this.rssWidgets = this.widgets.filter(widget => widget.settings.types.includes(WidgetType.rss));
        this.prepareCountries();
        this.prepareCategories();

        this.widgets = this.addDisabledCategoryWidget(this.webWidgets, this.nativeWidgets, this.rssWidgets);

        this.widgetsForFilter = this.widgets;
        this.filterWidgetList('type');
    }

    addDisabledCategoryWidget(wWeb: Widget[], wNative: Widget[], wRss: Widget[]): Widget[] {
        let wUpdate: Widget[] = [];

        if(this.nativeWidgets.length > 0) {
            wUpdate = wUpdate.concat(wNative);
        }

        if(this.webWidgets.length > 0) {
            wUpdate = wUpdate.concat(wWeb);
        }

        if(this.rssWidgets.length > 0) {
            wUpdate = wUpdate.concat(wRss);
        }
        return wUpdate;
    }

    addToHomeMouseIn(widget: Widget, target: any) {
        this.storage.get(this.homeKey)
            .pipe(take(1))
            .subscribe((widgets: any) => {
                if (widgets) {
                    if (widgets.iconWidgets.find(w => w.key == widget.key)) {
                        $(target).addClass('button-disabled');
                        this.existsInHome = true;
                    }
                }
            });
    }

    addToHomeMouseOut(w: Widget, target: any) {
        if (this.existsInHome) {
            this.existsInHome = false;
            $(target).removeClass('button-disabled');
        }
    }

    addWidgetToFullPage(widget: Widget) {
        if (this.showTabContent && this.selectedPage) {
            if (widget.settings.types.includes(WidgetType.full)) {
                let key = base_storage_keys.fullView_key+this.selectedPage.index;
                this.storage.get(key)
                    .pipe(take(1))
                    .subscribe((fullView: FullWrapper) => {
                        if (fullView && fullView.fullWidgets.length > 0) {
                            let height = 0;
                            let quit = false;
                            fullView.fullWidgets.forEach((cell: any) => {
                                if (cell.v > height) {
                                    height = cell.v;
                                }
                            });
                            height++;

                            if (!fullView.pageSettings || !fullView.pageSettings.col_count) fullView.pageSettings = {col_count: 4};

                            for (let v = 0; v <= height; v++) {
                                if (quit) break;
                                for (let c = 0; c < fullView.pageSettings.col_count; c++) {
                                    if (!fullView.fullWidgets.find(cell => {
                                            return (cell.v == v && cell.c == c);
                                        })) {
                                        fullView.fullWidgets.push({
                                            c: c,
                                            h: 0,
                                            k: widget.key,
                                            userData: widget.userData,
                                            v: v,
                                            m: false
                                        });

                                        this.storage.set(key, fullView);
                                        this.syncPage(key);
                                        if ((this.currentView != "icon") && (this.selectedPage.index == this.viewIndex)) this.sharedSvc.reloadView(key);
                                        quit = true;
                                        break;
                                    }
                                }
                            }
                        } else {
                            let fullView = {
                                fullWidgets: [{
                                    c: 0,
                                    h: 0,
                                    k: widget.key,
                                    userData: widget.userData,
                                    v: 0,
                                    m: false
                                }],
                                pageSettings: {col_count: this.columnsCount()}
                            }
                            this.storage.set(key, fullView);
                            this.syncPage(key);
                            if ((this.currentView != "icon") && (this.selectedPage.index != this.viewIndex)) this.sharedSvc.reloadView(key);
                        }
                    });
            } else {
                this.snack.openSnack("This Widget for home Page only", "error", "snack_error");
            }
        }
    }

    addWidgetToHomePage(widget: Widget) {
        if (this.showTabContent) {
            if (!this.existsInHome) {
                this.existsInHome = true;
                this.storage.get(this.homeKey)
                    .pipe(take(1))
                    .subscribe((widgets: IconWrapper) => {
                        if (widgets) {
                            let maxIndex = -1;
                            widgets.iconWidgets.forEach(w => {
                                if (w.layerIndex > maxIndex) maxIndex = w.layerIndex;
                            });
                            widgets.iconWidgets.push({
                                key: widget.key,
                                layerIndex: maxIndex+1,
                                userData: widget.userData
                            });
                            this.storage.set(this.homeKey, widgets);
                            this.syncPage(this.homeKey);
                            if (this.currentView == "icon") this.sharedSvc.reloadView(this.homeKey);
                        } else {
                            let widgets = {
                                iconWidgets: [{
                                    key: widget.key,
                                    layerIndex: 0,
                                    userData: widget.userData
                                }]
                            }
                            this.storage.set(this.homeKey, widgets);
                            this.syncPage(this.homeKey);
                            if (this.currentView == "icon") this.sharedSvc.reloadView(this.homeKey);
                        }
                    });
            } else this.snack.openSnack("This widget already exist on your home page quick access bar", "error", "snack_error")
        }
    }

    filterWidgetList(filterType: string) {
        this.showTabContent = false;
        switch (filterType) {
            case 'type':
                this.widgetCatSelected = 'all';
                if (this.widgetTypeSelected == WidgetType.web) {
                    this.baseSelectedCountry(this.webCountryOptions);
                } else if (this.widgetTypeSelected == WidgetType.native) {
                    this.baseSelectedCountry(this.nativeCountryOptions);
                } else if (this.widgetTypeSelected == WidgetType.rss) {
                    this.baseSelectedCountry(this.rssCountryOptions);
                } else if (this.widgetTypeSelected == "all") {
                    this.baseSelectedCountry(this.allCountryOptions);
                }

            case 'category':
            case 'country':
                if (this.widgetTypeSelected == WidgetType.rss) {
                    this.widgets = this.filterWidgets(this.rssWidgets, WidgetType.rss);
                } else if (this.widgetTypeSelected == WidgetType.web) {
                    this.widgets = this.filterWidgets(this.webWidgets, WidgetType.web);
                } else if (this.widgetTypeSelected == WidgetType.native) {
                    this.widgets = this.filterWidgets(this.nativeWidgets, WidgetType.native);
                } else if (this.widgetTypeSelected == "all") {
                    this.widgets = this.addDisabledCategoryWidget(
                        this.filterWidgets(this.webWidgets, WidgetType.web),
                        this.filterWidgets(this.nativeWidgets, WidgetType.native),
                        this.filterWidgets(this.rssWidgets, WidgetType.rss))
                }
                break;
        }
    }

    private syncPage(key: string) {
        if (this.afAuth.auth.currentUser) {
            this.sharedSvc.syncKey(key);
        }

        this.tabsSvc.addChangedTabKey(key);
    }

    private prepareCountries() {
        this.webWidgets.forEach(w => {
            if(widgetsList[w.key]) {
                widgetsList[w.key].countries.forEach((c: string) => {
                    if (!this.webCountryOptions.find(country => country.text == c)) this.webCountryOptions.push({value: c, text: c});
                });
            }
        });
        this.nativeWidgets.forEach(w => {
            if(widgetsList[w.key]) {
                widgetsList[w.key].countries.forEach((c: string) => {
                    if (!this.nativeCountryOptions.find(country => country.text == c)) this.nativeCountryOptions.push({value: c, text: c});
                });
            }
        });

        //push users country to country list and select this country as active
        if (this.geoSvc && this.geoSvc.location && this.geoSvc.location.countryCode) {
            let countryOption = {value: this.geoSvc.location.countryCode.toUpperCase(), text: this.geoSvc.location.countryCode};
            if (this.webCountryOptions.find(c => c.value == countryOption.value)) {
                this.widgetCountrySelected = countryOption.value;
            } else {
                this.webCountryOptions.push(countryOption);
                this.widgetCountrySelected = countryOption.value;
            }
            if (!this.nativeCountryOptions.find(c => c.value == countryOption.value)) {
                this.nativeCountryOptions.push(countryOption);
            }
            if (!this.rssCountryOptions.find(c => c.text == countryOption.value)) {
                this.rssCountryOptions.push(countryOption);
            }
        }

        this.webCountryOptions.concat(this.nativeCountryOptions).concat(this.rssCountryOptions).forEach(option => {
            if (!this.allCountryOptions.find(o => o.value.toLowerCase() == option.value.toLowerCase())) this.allCountryOptions.push(option);
        });
    }

    private prepareCategories() {
        this.webWidgets.forEach(w => {
            if(widgetsList[w.key]) {
                widgetsList[w.key].categories.forEach((c: string) => {
                    if (!this.webCategoryOptions.find(category => category.text == c)) this.webCategoryOptions.push({value: c, text: c});
                });
            }
        });
        this.nativeWidgets.forEach(w => {
            if(widgetsList[w.key]) {
                widgetsList[w.key].categories.forEach((c: string) => {
                    if (!this.nativeCategoryOptions.find(category => category.text == c)) this.nativeCategoryOptions.push({value: c, text: c});
                });
            }
        });

        this.webCategoryOptions.concat(this.nativeCategoryOptions).concat(this.rssCategoryOptions).forEach(option => {
            if (!this.allCategoryOptions.find(o => o.value.toLowerCase() == option.value.toLowerCase())) this.allCategoryOptions.push(option);
        });
    }

    private getPages(): Promise<any> {
        return new Promise<any>(resolve => {
            this.storage.get(base_storage_keys.pages)
                .pipe(take(1))
                .subscribe((pages: PagesWrapper) => {
                    if (pages && pages.allPages) {
                        this.homePage = pages.allPages.shift();
                        this.fullViews = pages.allPages;
                        resolve();
                    } else resolve();
                });
        });
    }

    private columnsCount(): number {
        let colsCount = 4;
        let windowWidth = window.innerWidth;
        if (windowWidth < 1120 && windowWidth > 780) colsCount = 3;
        else if (windowWidth < 780) colsCount = 2;
        return colsCount;
    }

    private filterWidgets(widgets: Widget[], type: string): Widget[] {
        if (type == WidgetType.web || type == WidgetType.native) {
            return widgets.filter((w: Widget) => {
                if(this.widgetCatSelected == 'all') {
                    if(w.settings.widgetDisabled) return false;
                    return (widgetsList[w.key].countries[0] == 'World') || widgetsList[w.key].countries.find(c => c == this.widgetCountrySelected);
                } else return widgetsList[w.key].categories.find(c => c == this.widgetCatSelected) && (widgetsList[w.key].countries[0] == 'World' || widgetsList[w.key].countries.includes(this.widgetCountrySelected));
            });
        } else if (WidgetType.rss) {
            return widgets.filter((widget: Widget) => {
                if(this.widgetCatSelected == 'all') {
                    if(widget.settings.widgetDisabled) return false;
                    return (widget.settings.country == 'WORLD') || (widget.settings.country == this.widgetCountrySelected);
                } else return (widget.settings.category == this.widgetCatSelected &&  widget.settings.country == this.widgetCountrySelected);
            });
        } else return [];
    }

    private baseSelectedCountry(options: any) {
        if (this.geoSvc && this.geoSvc.location && this.geoSvc.location.countryCode) {
            if (options.find(c => c.value == this.geoSvc.location.countryCode)) {
                this.widgetCountrySelected = this.geoSvc.location.countryCode;
            } else this.widgetCountrySelected = 'World';
        }
    }
}
