import { AppStorageService } from 'src/app/shared/storage/services/app-storage.service';

import {
    Component, ElementRef, Input
} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { WidgetBaseComponent } from '../../../shared/base-structures/widget-base.component';
import { IframeSettings } from '../models/iframe-settings';
import { WidgetIframeSettings } from '../widget-iframe-settings';
import {FireStoreService} from "../../../firebase/fire-store.service";
import {TabsService} from "../../../shared/storage/services/tabs.service";

@Component({
    selector: 'app-widget-iframe',
    templateUrl: './widget-iframe.component.html',
    styleUrls: ['./widget-iframe.component.scss']
})
export class WidgetIframeComponent extends WidgetBaseComponent<IframeSettings> {
    @Input() defaultSettings: WidgetIframeSettings;

    constructor(
        protected sanitizer: DomSanitizer,
        protected appStorage: AppStorageService,
        protected element: ElementRef,
        protected fireStore: FireStoreService,
        protected tabsSvc: TabsService) {
        super(appStorage, fireStore, tabsSvc);
    }

    widgetInit() {
        if(!this.initialized) {
            this.startWork()
                .then(() => {
                    this.showIframe(this.defaultSettings, this.sanitizer);
                    this.listenSync();
                });
            this.initialized = true;
        }
    }

    public reload(): void {
        const iframe = this.element.nativeElement.querySelector('iframe');
        if (iframe) {
            iframe.src = iframe.src;
        }
    }
}
