import { Component, OnInit } from '@angular/core';
import { WidgetBaseComponent } from 'src/app/shared/base-structures/widget-base.component';
import { UnitConverterSettings } from '../model/unit-converter-settings';
import { AppStorageService } from 'src/app/shared/storage/services/app-storage.service';
import { FireStoreService } from 'src/app/firebase/fire-store.service';
import { TabsService } from 'src/app/shared/storage/services/tabs.service';
import { toUnicode } from 'punycode';

@Component({
  selector: 'app-widget-unit-conveter',
  templateUrl: './widget-unit-conveter.component.html',
  styleUrls: ['./widget-unit-conveter.component.scss']
})
export class WidgetUnitConveterComponent extends WidgetBaseComponent<UnitConverterSettings> {

  fromUnitCode:string = "";
  toUnitCode:string = "";
  fromUnitValue:number;
  toUnitValue:number;
  selectedUnit:any = {};
  categoryId = 0;
  fromUnit:any = {};
  toUnit:any = {};
  PRECISION:number = 6;
  categories:any = [
    {id:0, name:"Select Category"},
    {id:1, name:"Length"},
    {id:2, name:"Area"},
    {id:3, name:"Volume"},
    {id:4, name:"Mass"},
    {id:5, name:"Density"},
    {id:6, name:"Speed"},
    {id:7, name:"Temperature"},
    {id:8, name:"High Pressure"},
    {id:9, name:"Low Pressure"},
    {id:10, name:"Torque"},
    {id:11, name:"Dynamic Viscosity"},
    {id:12, name:"Kinematic Viscosity"}
  ];

  units:any = [
    {id:0, unit:[]},
    {id:1, unit:[
      {code:"mm", name:"Milimeters", value:1000000},
      {code:"cm", name:"Centimeters", value:100000},
      {code:"m", name:"Meters", value:1000},
      {code:"km", name:"Kilometers", value:1},
      {code:"in", name:"inches", value:39370.08},
      {code:"ft", name:"Feet", value:3280.04},
      {code:"yd", name:"Yards", value:1093.613},
      {code:"mi", name:"Miles", value:0.621371}]
    },
    {id:2, unit:[
      {code:"mm", name:"Milimeter square", value:1000000},
      {code:"cm", name:"Centimeter square", value:10000},
      {code:"m", name:"Meter square", value:1},
      {code:"in", name:"inche square", value:1550.003},
      {code:"ft", name:"Foot square", value:10.76391},
      {code:"yd", name:"Yard square", value:1.19599}]
    },
    {id:3, unit:[
      {code:"cm", name:"Centimeter cube", value:28317},
      {code:"m", name:"Meter cube", value:0.028317},
      {code:"ltr", name:"Liter", value:28.31685},
      {code:"in", name:"inche cube", value:1728},
      {code:"ft", name:"Foot cube", value:1},
      {code:"US gal", name:"US gallons", value:7.481333},
      {code:"Imp. gal", name:"Imperial gallons", value:6.229712},
      {code:"US brl", name:"US barrel (oil)", value:0.178127}
    ]},
    {id:4, unit:[
      {code:"g", name:"Grams", value:907200},
      {code:"kg", name:"Kilograms", value:907.2},
      {code:"tonne", name:"Metric tonnes", value:0.9072},
      {code:"shton", name:"Short ton", value:1},
      {code:"Lton", name:"Long ton", value:0.892913},
      {code:"lb", name:"Pounds", value:2000},
      {code:"oz", name:"Ounces", value:32000}
    ]},
    {id:5, unit:[
      {code:"g/ml", name:"Gram/milliliter", value:1},
      {code:"kg/m", name:"Kilogram/meter cube", value:1000},
      {code:"lb/ft", name:"pound/foor cube", value:62.42197},
      {code:"lb/in", name:"pound/inch cube", value:0.036127}
    ]},
    {id:6, unit:[
      {code:"m/s", name:"Meter/second", value:0.2778},
      {code:"m/min", name:"Meter/minute", value:16.66467},
      {code:"km/h", name:"Kilometer/hour", value:1},
      {code:"ft/s", name:"Foot/second", value:0.911417},
      {code:"ft/min", name:"Foot/minute", value:54.68504},
      {code:"mi/h", name:"Miles/hour", value:0.621477}]
    },
    {id:7, unit:[
      {code:"C", name:"Degree Celsium", value:0},
      {code:"F", name:"Degree Fahrenheit", value:0},
      {code:"K", name:"Kelvin", value:0}
    ]},
    {id:8, unit:[
      {code:"bar", name:"Bar", value:10},
      {code:"psi", name:"Pound/square inch", value:145.03},
      {code:"kPa", name:"Kilopascal", value:1000},
      {code:"mPa", name:"Megapascal", value:1},
      {code:"kgf/cm", name:"Kilogram force/centimeter square", value:10.197},
      {code:"mmHg", name:"Millimeter of mercury", value:7500.2},
      {code:"atm", name:"Atmospheres", value:9.8717}
    ]},
    {id:9, unit:[
      {code:"mH2O", name:"Meter of water", value:0.135937},
      {code:"ftH2O", name:"Foot of water", value:0.445969},
      {code:"cmHg", name:"Centimeter of mercury", value:1},
      {code:"inHg", name:"inches of mercury", value:0.39368},
      {code:"inH2O", name:"inches of water", value:5.351265},
      {code:"Pa", name:"pascal", value:1333}
    ]},
    {id:10, unit:[
      {code:"Nm", name:"Newton meter", value:1.35582},
      {code:"kgfm", name:"Kilogram force meter", value:0.138255},
      {code:"ftlb", name:"Foot pound", value:1},
      {code:"inlb", name:"inch pound", value:12}
    ]},
    {id:11, unit:[
      {code:"cp", name:"Centipoise", value:1488.16},
      {code:"poise", name:"Poise", value:14.8816},
      {code:"lb/(ft.s)", name:"Pound/foot.second", value:1}
    ]},
    {id:12, unit:[
      {code:"cs", name:"Centistoke", value:1000000},
      {code:"St", name:"Stoke", value:10000},
      {code:"ft2/s", name:"Foot square/second", value:10.76392},
      {code:"m2/s", name:"Meter square/second", value:1}
    ]},
  ]
  constructor(protected appStorage: AppStorageService,
    protected fireStore: FireStoreService,
    protected tabsSvc: TabsService
    ) {
      super(appStorage, fireStore, tabsSvc);
    }

  public reload(): void {
    this.categoryId = 0;
    this.resetValues()
  }

  public widgetInit() {
    if (!this.initialized) {
      this.startWork()
        .then(() => {
          this.listenSync();
        });
      this.initialized = true;
    }
  }
  selectCategoryUnits(category) {
    this.categoryId = category.id;
    this.resetValues();
    let selectedUnit = this.units.find(unit=> unit.id == category.id);
    if((selectedUnit && selectedUnit.id == 0) || !selectedUnit ){
      this.selectedUnit = {};
    } else {
      this.selectedUnit = selectedUnit;
    }
  }

  selectUnit(unit:any, type:string) {
    if(type == "from") {
      this.fromUnit = unit;
      if(this.toUnitCode=="") {
        this.fromUnitValue = 1;
      } else if(this.toUnitCode != "") {
        if(this.categoryId == 7) {
          this.convertTemperature("to");
        } else {
          this.convertUnits("to");
        }
      }
    } else if(type == "to") {
      this.toUnit = unit;
      if(this.fromUnitCode == "") {
        this.toUnitValue = 1;
      } else if(this.fromUnitCode != "") {
        if(this.categoryId == 7) {
          this.convertTemperature("from");
        } else {
          this.convertUnits("from"); 
        }
      }
    }
  }

  convertUnits(type) {
    if(this.categoryId == 7) {
      this.convertTemperature(type);
    } else if(this.toUnitCode != "" && this.fromUnitCode != "") {
      if(type == "from") {
        this.toUnitValue = +((this.toUnit.value/this.fromUnit.value) * this.fromUnitValue).toFixed(this.PRECISION);
      } else if(type == "to") {
        this.fromUnitValue = +((this.fromUnit.value/this.toUnit.value) * this.toUnitValue).toFixed(this.PRECISION);
      }
    }
  }

  resetValuesByName(type) {
    if(type == "from") {
      this.fromUnitValue = null;
      this.fromUnit = {};
    } else if(type == "to") {
      this.toUnitValue = null;
      this.toUnit = {};
    }
  }

  convertTemperature(type) {
    if(this.toUnitCode != "" && this.fromUnitCode != "") {
      if(type == "to") {
        if(this.fromUnitCode == "C") {
          this.fromUnitValue = this.convertTempToCelsius(this.toUnitCode, this.toUnitValue);;
        } else if(this.fromUnitCode == "F") {
          this.fromUnitValue = this.convertTempToFahrenheit(this.toUnitCode, this.toUnitValue);
        } else if(this.fromUnitCode == "K"){
          this.fromUnitValue = this.convertTempToKelvin(this.toUnitCode, this.toUnitValue);
        }
      } else if(type == "from") {
        if(this.toUnitCode == "C") {
          this.toUnitValue = this.convertTempToCelsius(this.fromUnitCode, this.fromUnitValue);;
        } else if(this.toUnitCode == "F") {
          this.toUnitValue = this.convertTempToFahrenheit(this.fromUnitCode, this.fromUnitValue);
        } else if(this.toUnitCode == "K"){
          this.toUnitValue = this.convertTempToKelvin(this.fromUnitCode, this.fromUnitValue);
        }
      }
    }
  }

  convertTempToCelsius(from:string, value:number) {
    let returnValue:number ;
    if(from == "F") {
      returnValue = +((value-32)*5/9).toFixed(this.PRECISION);
    } else if(from == "C") {
      returnValue = value;
    } else if(from == "K") {
      returnValue = value-273.15;
    }
    return returnValue;
  }

  convertTempToFahrenheit(from:string, value:number) {
    let returnValue ;
    if(from == "F") {
      returnValue = value;
    } else if(from == "C") {
      returnValue = +((value*9/5)+32).toFixed(this.PRECISION);
    } else if(from == "K") {
      returnValue = +((1.8 * value)-459.67).toFixed(this.PRECISION);
    }
    return returnValue;
  }

  convertTempToKelvin(from:string, value:number) {
    let returnValue ;
    if(from == "F") {
      returnValue = +((value + 459.67)/1.8).toFixed(this.PRECISION);
    } else if(from == "C") {
      returnValue = value + 273.15
    } else if(from == "K") {
      returnValue = value;
    }
    return returnValue;
  }

  resetValues() {
    this.selectedUnit = {};
    this.toUnitCode = "";
    this.fromUnitCode = "";
    this.fromUnit = {};
    this.toUnit = {};
    this.fromUnitValue = null;
    this.toUnitValue = null;
  }
}
