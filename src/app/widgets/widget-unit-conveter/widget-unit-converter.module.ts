import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WidgetModule } from '../widget/widget.module';
import { BaseStructuresModule } from 'src/app/shared/base-structures/base-structures.module';
import { WidgetUnitConveterComponent } from './component/widget-unit-conveter.component';
import { MatInputModule, MatFormFieldModule, MatSelectModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    WidgetModule,
    BaseStructuresModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule
  ],
  declarations: [WidgetUnitConveterComponent],
  exports: [WidgetUnitConveterComponent]
})
export class WidgetUnitConverterModule { }
