import {Component, ViewEncapsulation} from '@angular/core';
import {CalculatorSettings} from "../model/calculator-settings";
import {WidgetBaseComponent} from "../../../shared/base-structures/widget-base.component";
import {AppStorageService} from "../../../shared/storage/services/app-storage.service";
import {FireStoreService} from "../../../firebase/fire-store.service";
import {TabsService} from "../../../shared/storage/services/tabs.service";

@Component({
    selector: 'app-widget-calculator',
    templateUrl: './widget-calculator.component.html',
    styleUrls: ['./widget-calculator.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class WidgetCalculatorComponent extends WidgetBaseComponent<CalculatorSettings> {
    operation: string = '';
    result: string= '';

    constructor(protected appStorage: AppStorageService,
                protected fireStore: FireStoreService,
                protected tabsSvc: TabsService) {
        super(appStorage, fireStore, tabsSvc);
    }

    public widgetInit(): void {
        if(!this.initialized) {
            this.startWork()
                .then(() => this.listenSync());
            this.initialized = true;
        }
    }

    public reload(): void {
        this.operation = '';
        this.result = '';
    }

    onClose() {
        this.result = '';
        this.operation = '';
    }


    append(element: string){
        this.operation += element;
    }

    complexAppend(element: string) {
        this.operation += element;
    }

    undo(){
        if (this.operation != ''){
          this.operation = this.operation.slice(0, -1);
        }
    }

    clear(){
        this.operation = '';
        this.result = '';
    }

    evaluate(){
        try{
          this.result = eval(this.operation);
        } catch(err) {
          if (err)
          this.result = 'Error';
        }
    }
}
