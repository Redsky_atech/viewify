import {WidgetIframeCustomModule} from './../widget-iframe-custom/widget-iframe-custom.module';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DynamicAttributesDirective, DynamicComponent, DynamicDirective, DynamicModule} from 'ng-dynamic-component';
import {WidgetTodoModule} from '../widget-todo/widget-todo.module';
import {WidgetTimeModule} from '../widget-time/widget-time.module';
import {WidgetWeatherModule} from '../widget-weather/widget-weather.module';
import {WidgetIframeModule} from '../widget-iframe/widget-iframe.module';
import {WidgetTodoComponent} from '../widget-todo/component/widget-todo.component';
import {WidgetTimeComponent} from '../widget-time/component/widget-time.component';
import {WidgetWeatherComponent} from '../widget-weather/component/widget-weather.component';
import {WidgetIframeComponent} from '../widget-iframe/component/widget-iframe.component';
import {WidgetIframeCustomComponent} from '../widget-iframe-custom/component/widget-iframe-custom.component';
import {WidgetsDynamicService} from './widgets-dynamic.service';
import {WidgetCalculatorComponent} from "../widget-calculator/component/widget-calculator.component";
import {WidgetCalculatorModule} from "../widget-calculator/widget-calculator.module";
import {WidgetQuoteModule} from "../widget-quotes/widget-quotes.module";
import {WidgetQuoteComponent} from "../widget-quotes/component/widget-quotes.component";
import {WidgetCalendarComponent} from "../widget-calendar/component/widget-calendar.component";
import {WidgetCalendarModule} from "../widget-calendar/widget-calendar.module";
import { WidgetDictionaryComponent } from '../widget-dictionary/component/widget-dictionary.component';
import { WidgetDictionaryModule } from '../widget-dictionary/widget-dictionary.module';
import { WidgetRadioComponent } from '../widget-radio/component/widget-radio.component';
import { WidgetRadioModule } from '../widget-radio/widget-radio.module';
import { WidgetUnitConverterModule } from '../widget-unit-conveter/widget-unit-converter.module';
import { WidgetUnitConveterComponent } from '../widget-unit-conveter/component/widget-unit-conveter.component';

@NgModule({
    imports: [
        CommonModule,

        WidgetTodoModule,
        WidgetTimeModule,
        WidgetWeatherModule,
        WidgetIframeModule,
        WidgetIframeCustomModule,
        WidgetCalculatorModule,
        WidgetQuoteModule,
        WidgetCalendarModule,
        WidgetDictionaryModule,
        WidgetRadioModule,
        WidgetUnitConverterModule,

        DynamicModule.withComponents([
            WidgetTodoComponent,
            WidgetTimeComponent,
            WidgetWeatherComponent,
            WidgetIframeComponent,
            WidgetIframeCustomComponent,
            WidgetCalculatorComponent,
            WidgetQuoteComponent,
            WidgetCalendarComponent,
            WidgetDictionaryComponent,
            WidgetRadioComponent,
            WidgetUnitConveterComponent
        ]),
    ],
    declarations: [],
    providers: [
        WidgetsDynamicService,
    ],
    exports: [
        DynamicComponent,
        DynamicDirective,
        DynamicAttributesDirective,
        DynamicModule,
    ]
})
export class WidgetsDynamicModule {
}

