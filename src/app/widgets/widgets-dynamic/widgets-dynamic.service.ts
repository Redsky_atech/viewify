import { BehaviorSubject, Observable } from 'rxjs';

import { Injectable } from '@angular/core';

import { Widget } from '../../shared/base-structures/widget';
import { WidgetDefaultSettings } from '../../shared/base-structures/widget-default-settings';
import { WidgetType } from '../../shared/base-structures/widget-type';
import {
    WidgetIframeCustomComponent
} from '../widget-iframe-custom/component/widget-iframe-custom.component';
import { WidgetIframeComponent } from '../widget-iframe/component/widget-iframe.component';
import { WidgetIframeSettings } from '../widget-iframe/widget-iframe-settings';
import { WidgetTimeComponent } from '../widget-time/component/widget-time.component';
import { WidgetTodoComponent } from '../widget-todo/component/widget-todo.component';
import { WidgetWeatherComponent } from '../widget-weather/component/widget-weather.component';
import {WidgetCalculatorComponent} from "../widget-calculator/component/widget-calculator.component";
import {WidgetQuoteComponent} from "../widget-quotes/component/widget-quotes.component";
import {WidgetCalendarComponent} from "../widget-calendar/component/widget-calendar.component";
import { WidgetDictionaryComponent } from '../widget-dictionary/component/widget-dictionary.component';
import { WidgetRadioComponent } from '../widget-radio/component/widget-radio.component';
import { WidgetUnitConveterComponent } from '../widget-unit-conveter/component/widget-unit-conveter.component';

@Injectable()
export class WidgetsDynamicService {
    private widgetsList: BehaviorSubject<Widget[]> = new BehaviorSubject([
        new Widget('w-todo', 'Todo', WidgetTodoComponent, <WidgetDefaultSettings>{
            cellsCount: 2,
            height: 400,
            types: [WidgetType.icon, WidgetType.full, WidgetType.native],
            showHeaderByDefault: true,
            headerNameByDefault: 'Todo'
        }),
        new Widget('w-quotes', 'Quotes', WidgetQuoteComponent, <WidgetDefaultSettings>{
            cellsCount: 2,
            height: 250,
            types: [WidgetType.icon, WidgetType.full, WidgetType.native],
            showHeaderByDefault: true,
            headerNameByDefault: 'Quotes'
        }),
        new Widget('w-calculator', 'Calculator', WidgetCalculatorComponent, <WidgetDefaultSettings>{
            cellsCount: 2,
            height: 350,
            types: [WidgetType.icon, WidgetType.full, WidgetType.native],
            showHeaderByDefault: true,
            headerNameByDefault: 'Calculator'
        }),
        new Widget('w-weather', 'Weather', WidgetWeatherComponent, <WidgetDefaultSettings>{
            cellsCount: 2,
            height: 250,
            types: [WidgetType.icon, WidgetType.full, WidgetType.native],
            showHeaderByDefault: true,
            headerNameByDefault: 'Weather'
        }),
        new Widget('w-time', 'Time', WidgetTimeComponent, <WidgetDefaultSettings>{
            cellsCount: 2,
            height: 185,
            types: [WidgetType.icon, WidgetType.full, WidgetType.native],
            showHeaderByDefault: false
        }),
        new Widget('w-calendar', 'Calendar', WidgetCalendarComponent, <WidgetDefaultSettings>{
            cellsCount: 2,
            height: 400,
            types: [WidgetType.icon, WidgetType.full, WidgetType.native],
            showHeaderByDefault: true,
            headerNameByDefault: 'Calendar'
        }),
        new Widget('w-g-mail', 'Gmail', WidgetIframeComponent, <WidgetIframeSettings>{
            cellsCount: 2,
            height: 450,
            types: [WidgetType.icon, WidgetType.full, WidgetType.web],
            fontSet: 'far',
            fontIcon: 'fa-envelope',
            url: 'https://mail.google.com/mail/mu/mp/?authuser=0',
            showHeaderByDefault: false,
            headerNameByDefault: 'mail.google.com'
        }),
        new Widget('w-custom', 'Custom', WidgetIframeCustomComponent, <WidgetIframeSettings>{
            cellsCount: 2,
            height: 450,
            types: [WidgetType.icon, WidgetType.full, WidgetType.web],
            fontSet: 'fab',
            fontIcon: 'fa-chrome',
            showHeaderByDefault: false,
        }),
        new Widget('w-dictionary', 'Dictionary', WidgetDictionaryComponent, <WidgetDefaultSettings>{
            cellsCount: 2,
            height: 250,
            types: [WidgetType.icon, WidgetType.full, WidgetType.native],
            showHeaderByDefault: true,
            headerNameByDefault: 'Dictionary'
        }),
        new Widget('w-radio', 'Radio', WidgetRadioComponent, <WidgetDefaultSettings>{
            cellsCount: 2,
            height: 250,
            types: [WidgetType.icon, WidgetType.full, WidgetType.native],
            showHeaderByDefault: true,
            headerNameByDefault: 'Radio'
        }),
        new Widget('w-unit-converter', 'Unit Converter', WidgetUnitConveterComponent, <WidgetDefaultSettings>{
            cellsCount: 2,
            height: 250,
            types: [WidgetType.icon, WidgetType.full, WidgetType.native],
            showHeaderByDefault: true,
            headerNameByDefault: 'Unit Converter'
        })
    ]);

    public readonly widgets: Observable<Widget[]> = this.widgetsList.asObservable();

    constructor() { }
}
