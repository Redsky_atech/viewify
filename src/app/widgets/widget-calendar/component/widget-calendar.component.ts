import {CalendarSettings} from "../model/calendar-settings";
import {WidgetBaseComponent} from "../../../shared/base-structures/widget-base.component";
import {AppStorageService} from "../../../shared/storage/services/app-storage.service";
import {FireStoreService} from "../../../firebase/fire-store.service";
import {TabsService} from "../../../shared/storage/services/tabs.service";

import {
    Component, ChangeDetectionStrategy, ViewChild, TemplateRef, ViewEncapsulation, Input,
    OnDestroy
} from '@angular/core';
import {startOfDay, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth, addHours} from 'date-fns';
import { Subject } from 'rxjs';
import {CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarView, CalendarDateFormatter} from 'angular-calendar';

import { CustomDateFormatter } from './custom-date-formatter.provider';
import {MatDialog, MatDialogRef} from "@angular/material";
import {ChromeStorageService} from "../../../shared/storage/services/chrome-storage.service";
import {base_storage_keys, widget_storage_keys} from "../../../shared/model/storage-keys";
import {take} from "rxjs/internal/operators";
import {TodoListWrapper} from "../../widget-todo/component/widget-todo.component";
import {SharedService} from "../../../shared/utils/shared.service";
const colors: any = {
    red: {
        primary: '#ad2121',
        secondary: '#FAE3E3'
    },
    blue: {
        primary: '#1e90ff',
        secondary: '#D1E8FF'
    },
    yellow: {
        primary: '#e3bc08',
        secondary: '#FDF1BA'
    }
};

export class EventsWrapper {
    events: any[];
}

@Component({
    selector: 'app-widget-calendar',
    templateUrl: './widget-calendar.component.html',
    styleUrls: ['./widget-calendar.component.scss'],
    providers: [
        {
            provide: CalendarDateFormatter,
            useClass: CustomDateFormatter
        }
    ],
    encapsulation: ViewEncapsulation.None
})
export class WidgetCalendarComponent extends WidgetBaseComponent<CalendarSettings> implements OnDestroy {
    @ViewChild('modalContent')
    modalContent: TemplateRef<any>;
    @ViewChild('modalAddRemoveEvent')
    modalAddRemoveEvent: TemplateRef<any>;
    @Input() dWidth: boolean;
    view: CalendarView = CalendarView.Month;
    CalendarView = CalendarView;
    multiWidget = false;
    viewDate: Date = new Date();
    modalData: {
        action: string;
        event: CalendarEvent;
    };
    actions: CalendarEventAction[] = [
        {
            label: '<i class="fa fa-fw fa-pencil"></i>',
            onClick: ({ event }: { event: CalendarEvent }): void => {
                this.handleEvent('Edited', event);
            }
        },
        {
            label: '<i class="fa fa-fw fa-times"></i>',
            onClick: ({ event }: { event: CalendarEvent }): void => {
                this.events = this.events.filter(iEvent => iEvent !== event);
                this.handleEvent('Deleted', event);
            }
        }
    ];
    refresh: Subject<any> = new Subject();
    dialogRef: MatDialogRef<any, any>;
    events: CalendarEvent[] = [];
    activeDayIsOpen: boolean = true;
    syncEvent: boolean = false;

    //view: string = "month";

    constructor(protected appStorage: AppStorageService,
                protected fireStore: FireStoreService,
                protected tabsSvc: TabsService,
                private dialog: MatDialog,
                private storage: ChromeStorageService,
                private sharedSvc: SharedService) {
        super(appStorage, fireStore, tabsSvc);
    }

    public widgetInit(): void {
        if(!this.initialized) {
            this.startWork()
                .then(() => {
                    this.getEvents();

                    this.rootSubs.add(this.fireStore.syncWithFire.subscribe((reset) => {
                        if (reset.type == "marker") {
                            this.syncEvent = false;
                            this.syncable = false;
                        } else if (reset.type == "data") {
                            this.startWork()
                                .then(() => {
                                    this.getEvents();
                                });
                        }
                    }));

                    this.rootSubs.add(this.tabsSvc.addTabObserver(base_storage_keys.calendar)
                        .subscribe(() => {
                            this.getEvents();
                        }));
                });
            this.initialized = true;
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        //this.saveSettings();
    }

    public saveSettings(): void {
        super.saveSettings();
        this.sync();
    }

    public reload(): void {
        // this.operation = '';
        // this.result = '';
        // view = "month";
        // viewDate = new Date();
    }

    dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
        if (isSameMonth(date, this.viewDate)) {
            this.viewDate = date;
            if (
                (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
                events.length === 0
            ) {
                this.activeDayIsOpen = false;
            } else {
                this.activeDayIsOpen = true;
            }
        }
    }

    eventTimesChanged({event, newStart, newEnd}: CalendarEventTimesChangedEvent): void {
        event.start = newStart;
        event.end = newEnd;
        this.handleEvent('Dropped or resized', event);
        this.refresh.next();
    }

    handleEvent(action: string, event: CalendarEvent): void {
        this.modalData = { event, action };
        this.sync();
        /*this.dialogRef = this.dialog.open(this.modalContent, {
            panelClass: "calendar-modal-overlay",
            hasBackdrop: true,
            backdropClass: "modal-overlay_back-drop"
        });*/
    }

    addEvent(): void {
        this.events.push({
            title: 'New event',
            id: this.uniqueId(),
            start: startOfDay(new Date()),
            end: endOfDay(new Date()),
            actions: this.actions,
            color: colors.red,
            draggable: true,
            resizable: {
                beforeStart: true,
                afterEnd: true
            }
        });
        this.sync();
        this.refresh.next();
    }

    addRemoveEvent(): void{
        this.dialogRef = this.dialog.open(this.modalAddRemoveEvent, {
            panelClass: "calendar-modal-overlay",
            hasBackdrop: true,
            backdropClass: "modal-overlay_back-drop"
        });
        this.dialogRef.beforeClose()
            .pipe(take(1))
            .subscribe(() => {
                this.sync();
                let todoEvents = this.events.filter(e => {
                    return (typeof e.meta == 'string' || e.meta instanceof String) && e.meta.startsWith(widget_storage_keys.todos);
                });
                let todoLists = [];
                todoEvents.forEach(e => {
                   if (!todoLists.includes(e.meta)) todoLists.push(e.meta);
                });
                todoLists.forEach(list => {
                    this.storage.get(list)
                        .pipe(take(1))
                        .subscribe((data: TodoListWrapper) => {
                            if (data && data.todoList) {
                                let notDated = data.todoList.filter(t => !t.createdAt);
                                data.todoList = data.todoList.filter(t => {
                                    return !!todoEvents.find(e => e.id == 'todo-'+t.id);
                                });
                                data.todoList.forEach(t => {
                                   let event = todoEvents.find(e => {
                                       return (e.meta == list) && (e.id == 'todo-'+t.id);
                                   });
                                   t.createdAt = event.start.toDateString();
                                   if (event.end) t.finishedAt = event.end.toDateString();
                                });
                                this.storage.set(list, {todoList: JSON.parse(JSON.stringify([...data.todoList, ...notDated]))});
                                this.tabsSvc.addChangedTabKey(list);
                            }
                        });
                });
            });
    }

    closeModal() {
        if (this.dialogRef) {
            this.dialogRef.close();
            this.dialogRef = null;
        }
    }

    private sync() {
        this.storage.set(base_storage_keys.calendar, {events: JSON.parse(JSON.stringify(this.events))});
        this.tabsSvc.addChangedTabKey(base_storage_keys.calendar);
        if (!this.syncEvent && this.fireStore.authFire.auth.currentUser) {
            this.syncEvent = true;
            this.sharedSvc.syncKey(base_storage_keys.calendar);
        }
    }

    private getEvents() {
        this.storage.get(base_storage_keys.calendar)
            .pipe(take(1))
            .subscribe((w: EventsWrapper) => {
                if (w && w.events) {
                    w.events.forEach(e => {
                        e.start = new Date(e.start);
                        e.end = new Date(e.end);
                    });
                    this.events = w.events;
                }
                else {
                    this.events = this.addDefaultEvents();
                    this.sync();
                }
            });
    }

    private addDefaultEvents(): CalendarEvent[] {
        let events = [];
        events.push({
            start: subDays(startOfDay(new Date()), 1),
            end: addDays(new Date(), 1),
            title: 'A 3 day event',
            id: this.uniqueId(),
            color: colors.red,
            actions: this.actions,
            allDay: true,
            resizable: {
                beforeStart: true,
                afterEnd: true
            },
            draggable: true
        });
        events.push({
            start: startOfDay(new Date()),
            title: 'An event with no end date',
            id: this.uniqueId(),
            color: colors.yellow,
            actions: this.actions
        });
        events.push({
            start: subDays(endOfMonth(new Date()), 3),
            end: addDays(endOfMonth(new Date()), 3),
            title: 'A long event that spans 2 months',
            id: this.uniqueId(),
            color: colors.blue,
            allDay: true
        });
        events.push({
            start: addHours(startOfDay(new Date()), 2),
            end: new Date(),
            title: 'A draggable and resizable event',
            id: this.uniqueId(),
            color: colors.yellow,
            actions: this.actions,
            resizable: {
                beforeStart: true,
                afterEnd: true
            },
            draggable: true
        });
        return events;
    }
}
