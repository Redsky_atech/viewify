import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClientService } from '../../shared/utils/http-client.service';

@Injectable({
  providedIn: 'root'
})
export class DictionaryService {

  language = "en";
  constructor(private _http: HttpClientService) { }

  urlList = {
    iChrome: 'https://api.ichro.me/dictionary/v1/definition/' ,
    googleApi: 'https://googledictionaryapi.eu-gb.mybluemix.net/?define='
  }

  getDefinition(word:string): Observable<any> {
      return this._http.getData(this.getDictionaryUrl(word));
  }

  getDictionaryUrl(word: string):string {
    return this.urlList.iChrome+word+"?lang="+this.language;
  }


}
