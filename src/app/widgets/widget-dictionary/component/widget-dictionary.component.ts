import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { DictionarySettings } from '../model/dictionary-settings';
import { WidgetBaseComponent } from 'src/app/shared/base-structures/widget-base.component';
import { AppStorageService } from 'src/app/shared/storage/services/app-storage.service';
import { FireStoreService } from 'src/app/firebase/fire-store.service';
import { TabsService } from 'src/app/shared/storage/services/tabs.service';
import { DictionaryService } from '../dictionary.service';

@Component({
  selector: 'app-widget-dictionary',
  templateUrl: './widget-dictionary.component.html',
  styleUrls: ['./widget-dictionary.component.scss']
})
export class WidgetDictionaryComponent extends WidgetBaseComponent<DictionarySettings> {

  word: string;
  definition: any = {};
  showForms: boolean = false;
  showMoreButton: string = "MORE";
  wordDefinitions:any;
  
  constructor(
    protected appStorage: AppStorageService,
    protected fireStore: FireStoreService,
    protected tabsSvc: TabsService,
    private _dictionary: DictionaryService
  ) {
    super(appStorage, fireStore, tabsSvc);

  }

  public reload(): void {
    this.definition = {};
  }

  getDefinition(word: string) {
    this.definition = {};
    if(word || word != "") {
      this._dictionary.getDefinition(word).subscribe(data=> {
        this.definition = data;
        this.wordDefinitions = this.definition.uses[0].definitions[0];
      }) 
    }
  }

  public widgetInit() {
    if (!this.initialized) {
      this.startWork()
        .then(() => {
          this.listenSync();
        });
      this.initialized = true;
    }
  }

  showFormsDetail() {
    if(this.showForms) {
      this.showForms = false;
      this.showMoreButton = "MORE";
      this.wordDefinitions = this.definition.uses[0].definitions[0];
    } else {
      this.showForms = true;
      this.showMoreButton = "LESS";
      this.wordDefinitions = this.definition.uses[0].definitions;
    }
  }

}
