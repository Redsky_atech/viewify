import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetDictionaryComponent } from './widget-dictionary.component';

describe('WidgetDictionaryComponent', () => {
  let component: WidgetDictionaryComponent;
  let fixture: ComponentFixture<WidgetDictionaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetDictionaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetDictionaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
