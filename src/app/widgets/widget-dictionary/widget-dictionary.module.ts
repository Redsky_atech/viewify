import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WidgetDictionaryComponent } from './component/widget-dictionary.component';
import { WidgetModule } from '../widget/widget.module';
import {BaseStructuresModule} from "../../shared/base-structures/base-structures.module";

@NgModule({
  imports: [
    CommonModule,
    WidgetModule,
    BaseStructuresModule
  ],
  declarations: [WidgetDictionaryComponent],
  exports:[WidgetDictionaryComponent]
})
export class WidgetDictionaryModule { }
