import { Injectable } from '@angular/core';
import { HttpClientService } from 'src/app/shared/utils/http-client.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RadioService {

  token: string = "fca96b64da337a7ce79f71cfab";
  constructor(private _http: HttpClientService) { }

  getPopularStations(): Observable<any> {
    return this._http.getData("http://api.dirble.com/v2/stations/popular?token="+this.token);
  }

  getCountries(): Observable<any> {
    return this._http.getData("http://api.dirble.com/v2/countries?token="+this.token);
  }

  getStationByCountry(code:string, page:number): Observable<any> {
    return this._http.getData("http://api.dirble.com/v2/countries/"+code+"/stations?page="+page+"&per_page=10&token="+this.token);
  }
}
