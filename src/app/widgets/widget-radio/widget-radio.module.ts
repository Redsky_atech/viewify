import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WidgetModule } from '../widget/widget.module';
import { BaseStructuresModule } from 'src/app/shared/base-structures/base-structures.module';
import { WidgetRadioComponent } from './component/widget-radio.component';

@NgModule({
  imports: [
    CommonModule,
    WidgetModule,
    BaseStructuresModule
  ],
  declarations: [WidgetRadioComponent],
  exports: [WidgetRadioComponent]
})
export class WidgetRadioModule { }
