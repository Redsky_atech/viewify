import { Component, OnInit } from '@angular/core';
import { RadioSettings } from '../model/radio-settings';
import { WidgetBaseComponent } from 'src/app/shared/base-structures/widget-base.component';
import { AppStorageService } from 'src/app/shared/storage/services/app-storage.service';
import { FireStoreService } from 'src/app/firebase/fire-store.service';
import { TabsService } from 'src/app/shared/storage/services/tabs.service';
import { RadioService } from '../radio.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-widget-radio',
  templateUrl: './widget-radio.component.html',
  styleUrls: ['./widget-radio.component.scss']
})
export class WidgetRadioComponent extends WidgetBaseComponent<RadioSettings> {

  showPlayer: boolean = false;
  previousPlayStationId: any;
  id: string = "id";
  stations: any = [];
  playingStation: any = {};
  isPlay: boolean = true;
  countries: any = [];
  countryCode:string;
  page: number = 0;
  hasMoreStations; boolean = true;

  constructor( protected appStorage: AppStorageService,
    protected fireStore: FireStoreService,
    protected tabsSvc: TabsService,
    private _radio: RadioService
  ) {
    super(appStorage, fireStore, tabsSvc);
  }

  public reload(): void {
    this.getCountries();
  }

  public widgetInit() {
    if (!this.initialized) {
      this.startWork()
        .then(() => {
          this.getCountries();
          this.listenSync();
        });
      this.initialized = true;
    }
  }

  playAudio(station, id) {
    this.showPlayer = true;
    this.playingStation = station;
    if(this.previousPlayStationId) {
      this.pauseAudio(this.previousPlayStationId);
    }
    this.previousPlayStationId = id;
    this.isPlay = true;
    var audElem = <HTMLMediaElement> document.getElementById(id);
    var playPromise = audElem.play();

    if (playPromise !== undefined) {
      playPromise.then(_ => {
      })
      .catch(error => {
        console.log(error);
      });
    };
  }

  pauseAudio(id) {
    this.isPlay = false;
    var audElem = <HTMLMediaElement> document.getElementById(id);
    audElem.pause();
  }

  getStationByCountry(code:string) {
    this.countryCode = code;
    if(this.page == 0) {
      this.stations = [];
      this.hasMoreStations = true;
    }
    this.page = this.page+1;
    this._radio.getStationByCountry(code, this.page).subscribe(data=> {
      if(data.length<10) {
        this.hasMoreStations = false;
      }
      this.stations = this.stations.concat(data);
      this.stations = _.sortBy(_.uniqBy(this.stations, 'name'), 'name');
    });
  }

  getCountries() {
    this.page = 0;
    this.countryCode = null;
    this.countries = [];
    this.stations = [];
    this._radio.getCountries().subscribe(data=> {
      this.countries = data;
    });
  }

}
