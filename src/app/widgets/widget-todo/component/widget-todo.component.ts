import { SortablejsOptions } from 'angular-sortablejs';
import { Component, ViewEncapsulation } from '@angular/core';
import { WidgetBaseComponent } from '../../../shared/base-structures/widget-base.component';
import { AppStorageService } from '../../../shared/storage/services/app-storage.service';
import { TodoSettings } from '../models/todo-settings';
import {TodoItem} from "../models/todo-item";
import {WidgetType} from "../../../shared/base-structures/widget-type";
import {FireStoreService} from "../../../firebase/fire-store.service";
import {base_storage_keys, widget_storage_keys} from "../../../shared/model/storage-keys";
import {filter, take} from "rxjs/internal/operators";
import {ChromeStorageService} from "../../../shared/storage/services/chrome-storage.service";
import {TabsService} from "../../../shared/storage/services/tabs.service";
import {startOfDay, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth, addHours} from 'date-fns';
import {CalendarEvent} from 'angular-calendar';
import {EventsWrapper} from "../../widget-calendar/component/widget-calendar.component";
import {SharedService} from "../../../shared/utils/shared.service";

export class TodoListWrapper {
    todoList: TodoItem[];
}
const colors: any = {
    red: {
        primary: '#ad2121',
        secondary: '#FAE3E3'
    },
    blue: {
        primary: '#1e90ff',
        secondary: '#D1E8FF'
    },
    yellow: {
        primary: '#e3bc08',
        secondary: '#FDF1BA'
    }
};

@Component({
    selector: 'app-widget-todo',
    templateUrl: './widget-todo.component.html',
    styleUrls: ['./widget-todo.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class WidgetTodoComponent extends WidgetBaseComponent<TodoSettings> {
    options: SortablejsOptions;
    public todoInput: string;
    public todos: TodoItem[] = [];
    private syncTodos: boolean = false;
    private syncEvent: boolean = false;
    public openInStart: boolean = false;
    private todosKey: string;

    constructor(
        protected appStorage: AppStorageService,
        protected fireStore: FireStoreService,
        protected tabsSvc: TabsService,
        private storage: ChromeStorageService,
        private sharedSvc: SharedService
    ) {
        super(appStorage, fireStore, tabsSvc);
        this.options = {
            handle: '.todo-handle',
            ghostClass: 'sortable-list_background',
            onUpdate: this.postChangesToStorage.bind(this)
        };
    }

    postChangesToStorage(event) {
        this.sync();
    }

    widgetInit(): void {
        if(!this.initialized) {
            if (!this.defaultSettings) this.defaultSettings = {types: [WidgetType.icon], showHeaderByDefault: false, headerNameByDefault: ""}
            this.startWork()
                .then(() => {
                    this.openInStart = this.settings.openInStart;
                    if (this.userData) this.todosKey = widget_storage_keys.todos+this.userData;
                    else if (this.key = 'fixed-w-todo') this.todosKey = widget_storage_keys.todos+'fixed-w-todo';
                    this.getAllTodos();
                    this.rootSubs.add(this.tabsSvc.addTabObserver(this.todosKey)
                        .subscribe(() => {
                            this.getAllTodos();
                        }));

                    this.rootSubs.add(this.fireStore.syncWithFire.subscribe((reset) => {
                        if (reset.type == "marker") {
                            this.syncable = false;
                            this.syncTodos = false;
                            this.syncEvent = false;
                        } else if (reset.type == "data") {
                            this.startWork();
                        }
                    }));
                });
            this.initialized = true;
        }
    }

    public saveSettings(): void {
        this.settings.openInStart = this.openInStart;
        if (this.key == 'fixed-w-todo') {
            //need to skip first todo saveSettings that happens on every open tab, only for authenticated users
            if (this.sharedSvc.todoSkipSave) this.sharedSvc.todoSkipSave = false;
            else super.saveSettings();
        } else super.saveSettings();
    }

    public addTodo(): void {
        if (this.todoInput === '') {
            return;
        }
        const newTodo: TodoItem = {
            id: this.todos.length + 1,
            content: this.todoInput,
            isCompleted: false,
            hasFlag: false,
            createdAt: ''
        };
        this.todos.push(newTodo);
        this.sync();
        this.todoInput = '';
    }

    public checkTodo(item: TodoItem) {
        item.isCompleted = !item.isCompleted;
        this.sync();
    }

    public onTodoDateChange(date: Date, id: number): void {
        this.todos.find(x => x.id === id).createdAt = date.toString();
        this.sync();
    }

    public toggleTodoFlag(id: number): void {
        const todo = this.todos.find(x => x.id === id);
        todo.hasFlag = !todo.hasFlag;
        this.sync();
    }

    public todoDate(date: string): Date {
        return new Date(date);
    }

    public reload(): void {
        // Todo
    }

    removeDate (todoItem: TodoItem) {
        todoItem.createdAt = '';
        this.sync();
    }

    public sync(): void {
        console.log(this.todos);
        this.tabsSvc.addChangedTabKey(this.todosKey);
        if (this.todosKey) {
            if ((!this.syncTodos || !this.syncEvent) && this.fireStore.authFire.auth.currentUser) {
                this.syncTodos = true;
                this.sharedSvc.syncKey(this.todosKey)
                    .then(() => this.syncCalendar());
            }
            this.storage.set(this.todosKey, {todoList: JSON.parse(JSON.stringify(this.todos))});
            this.saveToCalendar();
        }
    }

    public removeTodo(id: number): void {
        this.todos.splice(this.todos.findIndex(x => x.id === id), 1);
        this.sync();
    }

    public getAllTodos(): Promise<TodoItem[]> {
        return new Promise(resolve => {
            if (this.todosKey) {
                this.storage.get(this.todosKey)
                    .pipe(take(1))
                    .subscribe((data: TodoListWrapper) => {
                        if (data && data.todoList) {
                            this.todos = data.todoList;
                            this.todos.forEach((todo, index) => {
                                todo.id = index + 1;
                            });
                            resolve(this.todos);
                        } else {
                            resolve([]);
                        }
                    });
            } else resolve([]);
        });
    }

    private syncCalendar() {
        if (!this.syncEvent) {
            this.syncEvent = true;
            this.sharedSvc.syncKey(base_storage_keys.calendar);
        }
    }

    private saveToCalendar() {
        this.storage.get(base_storage_keys.calendar)
            .pipe(take(1))
            .subscribe((calendar: EventsWrapper) => {
                if (!calendar || !calendar.events) {
                    calendar = {events: []}
                    if (this.todos) {
                        this.todos.forEach(t => {
                            if (t.createdAt) {
                                calendar.events.push({
                                    title: 'New event',
                                    id: 'todo-'+t.id,
                                    start: startOfDay(new Date(t.createdAt)),
                                    end: endOfDay(new Date(t.createdAt)),
                                    color: colors.blue,
                                    draggable: false,
                                    resizable: {
                                        beforeStart: false,
                                        afterEnd: false
                                    },
                                    meta: this.todosKey
                                });
                            }
                        });
                    }
                    this.storage.set(base_storage_keys.calendar, {events: JSON.parse(JSON.stringify(calendar.events))});
                    this.tabsSvc.addChangedTabKey(base_storage_keys.calendar);
                } else {
                    let otherEvents = calendar.events.filter((e: any) => {
                        return e.meta != this.todosKey;
                    });

                    let todoEvents = [];
                    this.todos.forEach(t => {
                        if (t.createdAt) {
                            let event: any = {
                                title: 'New event',
                                id: 'todo-'+t.id,
                                start: new Date(t.createdAt),
                                color: colors.blue,
                                draggable: false,
                                resizable: {
                                    beforeStart: false,
                                    afterEnd: false
                                },
                                meta: this.todosKey
                            }
                            if (t.finishedAt) event.end = new Date(t.finishedAt);
                            else event.end = startOfDay(new Date(t.createdAt));
                            todoEvents.push(event);
                        }
                    });

                    let allEvents = [...otherEvents, ...todoEvents];
                    this.storage.set(base_storage_keys.calendar, {events: JSON.parse(JSON.stringify(allEvents))});
                    this.tabsSvc.addChangedTabKey(base_storage_keys.calendar);
                }
            });
    }
}
