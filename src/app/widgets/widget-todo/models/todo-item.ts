export class TodoItem {
    public id: number;
    public content: string;
    public isCompleted: boolean;
    public hasFlag: boolean;
    public createdAt: string;
    public finishedAt?: string;
}
