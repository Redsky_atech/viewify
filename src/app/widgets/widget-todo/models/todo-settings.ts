import { WidgetSettings } from 'src/app/shared/base-structures/widget-settings';

export class TodoSettings extends WidgetSettings {
    openInStart: boolean;
}
