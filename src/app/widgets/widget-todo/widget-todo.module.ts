import { DatePickerComponent } from 'src/app/shared/components/date-picker/date-picker.component';

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {
    MatButtonModule,
    MatCheckboxModule,
    MatDatepickerIntl,
    MatDatepickerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatNativeDateModule,
    MatRippleModule
} from '@angular/material';

import { BaseStructuresModule } from '../../shared/base-structures/base-structures.module';
import { WidgetModule } from '../widget/widget.module';
import { WidgetTodoComponent } from './component/widget-todo.component';
import {SortablejsModule} from "angular-sortablejs";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatButtonModule,
        MatNativeDateModule,
        MatDatepickerModule,
        MatRippleModule,

        BaseStructuresModule,
        WidgetModule,
        SortablejsModule
    ],
    providers: [
        MatDatepickerIntl
    ],
    declarations: [
        DatePickerComponent,
        WidgetTodoComponent,
    ],
    exports: [
        WidgetTodoComponent,
    ],
})
export class WidgetTodoModule {
}
