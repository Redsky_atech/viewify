import { Component } from '@angular/core';

import {
    WidgetBaseComponent
} from '../../../shared/base-structures/widget-base.component';
import { AppStorageService } from '../../../shared/storage/services/app-storage.service';
import { WeatherSettings } from '../models/weather-settings';
import { WeatherApisService } from '../weather-apis.service';
import { WeatherService } from '../weather.service';
import {FireStoreService} from "../../../firebase/fire-store.service";
import {Subject} from "rxjs/Rx";
import {widget_storage_keys} from "../../../shared/model/storage-keys";
import {debounceTime, distinctUntilChanged, take} from "rxjs/internal/operators";
import {ChromeStorageService} from "../../../shared/storage/services/chrome-storage.service";
import {TabsService} from "../../../shared/storage/services/tabs.service";
import {LocationModel} from "../../../shared/geo/location.model";
import {FormControl} from "@angular/forms";
import {countries} from "../../../shared/geo/countries";
import {LocationService} from "../../../shared/geo/location.service";

@Component({
    selector: 'app-widget-weather',
    templateUrl: './widget-weather.component.html',
    styleUrls: ['./widget-weather.component.scss']
})
export class WidgetWeatherComponent extends WidgetBaseComponent<WeatherSettings> {
    settings = new WeatherSettings();
    FACountryCodes = ['USA', 'US', 'United States', 'CA', 'Canada'];
    public isEditingLocation = false;
    public tempType = "";
    private getWeatherForecastSub = new Subject<boolean>();
    public getWeatherForecast$ = this.getWeatherForecastSub.asObservable();
    public geolocation: LocationModel = new LocationModel();
    widgetGeo: LocationModel = new LocationModel();
    public showInAutoComplete: any[];
    locationInputData: string = "";
    foundLocation: any;
    isFahrenheit = true;
    locationChanged: boolean = false;
    state: string = "";
    locationInfo: string = "";
    private interval;
    searchField: FormControl = new FormControl();
    geoTypes: any = {
        current: "current",
        custom: "custom"
    }

    constructor(
        protected appStorage: AppStorageService,
        private storage: ChromeStorageService,
        public weatherSvc: WeatherService,
        protected fireStore: FireStoreService,
        private weatherApiSvc: WeatherApisService,
        protected tabsSvc: TabsService,
        public geoSvc: LocationService) {
        super(appStorage, fireStore, tabsSvc);
        this.searchField.disable();
        this.maxHeaderLength = 16;
    }


    ngOnDestroy(): void {
        clearInterval(this.interval);
        super.ngOnDestroy();
    }

    widgetInit(): void {
        if(!this.initialized) {
            this.settings.geoType = this.geoTypes.current;
            this.interval = setInterval(() => {
                this.reload();
            },3600000);

            this.rootSubs.add(this.getWeatherForecast$.subscribe(isFahrenheit => {
                this.isFahrenheit = isFahrenheit;
                if (this.settings.geoType == this.geoTypes.current) this.getWeatherForecast(this.geolocation);
                else if (this.settings.geoType == this.geoTypes.custom) this.getWeatherForecast(this.widgetGeo);
            }));

            this.startWork()
                .then(result => {
                    this.geolocation = this.geoSvc.location;
                    if (this.settings.location) this.widgetGeo = this.settings.location;

                    this.changeLocationInfo();
                    this.searchField.setValue(this.widgetGeo);

                    if (this.settings.needUpdateWeather) {
                        this.reload();
                    } else {
                        this.CheckStoredTempType();
                    }

                    this.listenSync();

                    this.searchField.valueChanges
                        .pipe(debounceTime(400), distinctUntilChanged())
                        .subscribe((input: any) => {
                            if (input) {
                                this.locationInputData = input;
                                this.keyUpLocation();
                            }
                        });
                });
            this.initialized = true;
        }
    }

    public saveSettings(): void {
        if (this.widgetGeo) this.settings.location = this.widgetGeo;

        this.settings.WeatherUpdatedAt = new Date().toString();

        if (this.locationChanged) {
            this.checkTempType();
            this.saveCheckedLocation();
            this.locationChanged = false;
        } else super.saveSettings();
    }

    private checkTempType() {
        if (this.settings.geoType == this.geoTypes.current) {
            if (this.FACountryCodes.includes(this.geolocation.countryCode)) this.settings.TempType = 'f';
            else this.settings.TempType = 'c';
        } else {
            if (this.FACountryCodes.includes(this.settings.location.countryCode)) this.settings.TempType = 'f';
            else this.settings.TempType = 'c';
        }
    }

    private saveCheckedLocation() {
        this.isEditingLocation = false;
        this.showInAutoComplete = [];
        this.searchField.disable();
        this.foundLocation = null;
        this.locationInputData = "";
        super.saveSettings();
        this.changeLocationInfo();
        this.reload();
    }

    public reload(): void {
        this.CheckStoredTempType();
        if (this.settings.geoType == this.geoTypes.current) this.getWeatherForecast(this.geolocation);
        else if (this.settings.geoType == this.geoTypes.custom) this.getWeatherForecast(this.widgetGeo);

        /*this.getTempType().then(data => {
            this.settings.TempType = data;
            this.CheckStoredTempType();
            if (this.settings.geoType == this.geoTypes.current) this.getWeatherForecast(this.geolocation);
            else if (this.settings.geoType == this.geoTypes.custom) this.getWeatherForecast(this.widgetGeo);
        });*/
    }

    onLocationSettingsChange(value) {
        if (value.checked === false) {
            this.settings.FindAutoAddress = false;
        } else {
            this.settings.FindAutoAddress = true;
        }
        this.saveSettings();
    }

    onChangeTempUnit(value) {
        if (value.checked === true) {
            this.isFahrenheit = true;
            //this.addOrUpdateTempType('f');
        } else {
            this.isFahrenheit = false;
            //this.addOrUpdateTempType('c');
        }

        this.getWeatherForecastFahrenheit(this.isFahrenheit);
    }

    changeUsedLocation() {
        this.locationChanged = true;
    }

    editLocation() {
        this.foundLocation = null;
        this.showInAutoComplete = [];
        this.searchField.enable();
        this.isEditingLocation = true;
    }

    changeLocation(loc: any) {
        this.showInAutoComplete = [];
        this.foundLocation = loc;
    }

    saveCustomLocation() {
        if (this.foundLocation) {
            if (this.FACountryCodes.includes(this.foundLocation.countryCode)) {
                this.settings.TempType = 'f';
            } else this.settings.TempType = 'c';

            this.showInAutoComplete = [];
            this.searchField.disable();
            this.isEditingLocation = false;

            this.widgetGeo = this.foundLocation;
            //this.syncable = true;
            this.saveSettings();
            //this.syncable = false;
            this.reload();
            this.locationInputData = "";
            this.searchField.setValue(this.widgetGeo);
            this.foundLocation = null;
        }
    }

    CheckStoredTempType() {
        if (this.settings.TempType != '') {
            if (this.settings.TempType == 'f') {
                this.isFahrenheit = true;
            } else {
                this.isFahrenheit = false;
            }
        } else {
            // if user comes fr the first time
            // if (this.woeid && this.woeid.toString() === '23424977') { // for usa only, defalut is Fahrenheit
            if (this.settings.location && (this.settings.location.countryCode == countries.usa || this.settings.location.countryCode == "US")) { // for usa only, defalut is Fahrenheit
                this.isFahrenheit = true;
            } else {
                this.isFahrenheit = false;
            }
        }
    }

    getMyLocation() {
        this.foundLocation = this.geolocation;
        this.searchField.setValue(this.geolocation);
    }

    displayFn(value: any):string | undefined {
        return value? value.city: "";
    }

    keyUpLocation() {
        this.showInAutoComplete = [];
        if (this.locationInputData.length > 0) {
            if (this.geolocation.countryCode == countries.usa || this.geolocation.countryCode == "US") {
                this.geoSvc.findByCityName(this.locationInputData, countries.usa)
                    .pipe(take(1))
                    .subscribe((loc: any[]) => {
                        if (loc && loc.length > 0) this.showInAutoComplete = loc;
                        else this.findWeatherByYahoo();
                    });
            } else if (this.geolocation.countryCode == countries.canada || this.geolocation.countryCode == "Canada") {
                this.geoSvc.findByCityName(this.locationInputData, countries.canada)
                    .pipe(take(1))
                    .subscribe((loc: any[]) => {
                        if (loc && loc.length > 0) this.showInAutoComplete = loc;
                        else this.findWeatherByYahoo();
                    });
            } else this.findWeatherByYahoo();
        }
    }

    private findWeatherByYahoo(): Promise<any> {
        return new Promise<any>(resolve => {
            this.weatherApiSvc.getSuggestedLocations(this.locationInputData).then((locations: LocationModel[]) => {
                if (locations) {
                    let foundLocations = [];
                    let redundantLocations = [];
                    locations.forEach(l => {
                        if (foundLocations.length < 10) {
                            if (l.city.toLowerCase().startsWith(this.locationInputData.toLowerCase())) {
                                foundLocations.push(l);
                            } else if (l.countryCode.toLowerCase().startsWith(this.locationInputData.toLowerCase())) {
                                foundLocations.push(l);
                            } else redundantLocations.push(l);
                        }
                    });

                    this.showInAutoComplete = foundLocations.concat(redundantLocations);
                    resolve()
                } else resolve();
            });
        });
    }

    clearInput() {
        this.locationInputData = "";
        this.showInAutoComplete = [];
        this.searchField.setValue(this.widgetGeo);
        this.searchField.disable();
        this.isEditingLocation = false;
    }

    getWeatherForecastFahrenheit(isFahrenheit: boolean): void {
        this.getWeatherForecastSub.next(isFahrenheit);
    }

    getWeatherForecast(location: any): void {
        if (location) {
            if (this.isFahrenheit) {
                this.settings.TempType = 'f';
            } else {
                this.settings.TempType = 'c';
            }

            this.weatherApiSvc.getWeatherForecast(location.city, this.settings.TempType).then(data => {
                this.settings.WeatherDatas = data;
                //this.syncable = true;
                this.saveSettings();
                //this.syncable = false;
            });
        }
    }

    addOrUpdateTempType(newContent: string): void {
        this.settings.TempType = newContent;
        this.tempType = newContent;
        //this.sync();
    }

    getTempType(): any {
        return new Promise(resolve => {
            this.storage.get(widget_storage_keys.weather_temp_type)
                .pipe(take(1))
                .subscribe(data => {
                    if (data != null) {
                        this.tempType = data;
                        resolve(this.tempType);
                    } else {
                        resolve('');
                    }
                });
        });
    }

    public sync(): void {
        super.saveSettings();
        this.storage.set(widget_storage_keys.weather_temp_type, this.tempType);
    }

    changeLocationInfo() {
        if ((this.settings.geoType == this.geoTypes.custom) && this.widgetGeo.city) this.locationInfo = `${this.widgetGeo.city}, ${this.widgetGeo.countryCode}`;
        else if (this.geolocation.city && (this.settings.geoType == this.geoTypes.current)) {
            this.locationInfo = `${this.geolocation.city}, ${this.geolocation.countryCode}`;
        } else this.locationInfo = "";
    }
}
