import {Injectable} from '@angular/core';
import { WeatherSettings } from './models/weather-settings';

@Injectable()
export class WeatherService {
    public geoUrl = "http://api.geonames.org/findNearbyJSON?lat=";
    public geoUrl2 = 'http://api.ipstack.com/check?access_key=3589f68db60f0f3e4898489508833085&format=1';

    constructor () {}

    public weatherIcon(settings: WeatherSettings, conditionCode?: string): string {
        if (!conditionCode && !settings.WeatherDatas[0]) {
            return '<i class="wi wi-cloud"></i>';
        }
        if (settings.WeatherDatas[0].conditionCode !== undefined) {
            switch (conditionCode || settings.WeatherDatas[0].conditionCode.toString()) {
                case '0': return '<i class="wi wi-tornado"></i>';
                case '1': return '<i class="wi wi-storm-showers"></i>';
                case '2': return '<i class="wi wi-tornado"></i>';
                case '3': return '<i class="wi wi-thunderstorm"></i>';
                case '4': return '<i class="wi wi-thunderstorm"></i>';
                case '5': return '<i class="wi wi-snow"></i>';
                case '6': return '<i class="wi wi-rain-mix"></i>';
                case '7': return '<i class="wi wi-rain-mix"></i>';
                case '8': return '<i class="wi wi-sprinkle"></i>';
                case '9': return '<i class="wi wi-sprinkle"></i>';
                case '10': return '<i class="wi wi-hail"></i>';
                case '11': return '<i class="wi wi-showers"></i>';
                case '12': return '<i class="wi wi-showers"></i>';
                case '13': return '<i class="wi wi-snow"></i>';
                case '14': return '<i class="wi wi-storm-showers"></i>';
                case '15': return '<i class="wi wi-snow"></i>';
                case '16': return '<i class="wi wi-snow"></i>';
                case '17': return '<i class="wi wi-hail"></i>';
                case '18': return '<i class="wi wi-hail"></i>';
                case '19': return '<i class="wi wi-cloudy-gusts"></i>';
                case '20': return '<i class="wi wi-fog"></i>';
                case '21': return '<i class="wi wi-fog"></i>';
                case '22': return '<i class="wi wi-fog"></i>';
                case '23': return '<i class="wi wi-cloudy-gusts"></i>';
                case '24': return '<i class="wi wi-cloudy-windy"></i>';
                case '25': return '<i class="wi wi-thermometer"></i>';
                case '26': return '<i class="wi wi-cloudy"></i>';
                case '27': return '<i class="wi wi-night-cloudy"></i>';
                case '28': return '<i class="wi wi-day-cloudy"></i>';
                case '29': return '<i class="wi wi-night-cloudy"></i>';
                case '30': return '<i class="wi wi-day-cloudy"></i>';
                case '31': return '<i class="wi wi-night-clear"></i>';
                case '32': return '<i class="wi wi-day-sunny"></i>';
                case '33': return '<i class="wi wi-night-clear"></i>';
                case '34': return '<i class="wi wi-day-sunny-overcast"></i>';
                case '35': return '<i class="wi wi-hail"></i>';
                case '36': return '<i class="wi wi-day-sunny"></i>';
                case '37': return '<i class="wi wi-thunderstorm"></i>';
                case '38': return '<i class="wi wi-thunderstorm"></i>';
                case '39': return '<i class="wi wi-thunderstorm"></i>';
                case '40': return '<i class="wi wi-storm-showers"></i>';
                case '41': return '<i class="wi wi-snow"></i>';
                case '42': return '<i class="wi wi-snow"></i>';
                case '43': return '<i class="wi wi-snow"></i>';
                case '44': return '<i class="wi wi-cloudy"></i>';
                case '45': return '<i class="wi wi-lightning"></i>';
                case '46': return '<i class="wi wi-snow"></i>';
                case '47': return '<i class="wi wi-thunderstorm"></i>';
                case '3200': return '<i class="wi wi-cloud"></i>';
                default: return '<i class="wi wi-cloud"></i>';
            }
        } else {
            return '<i class="wi wi-cloud"></i>';
        }
    }
}
