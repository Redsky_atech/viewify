//import {OAuth} from "oauth";
import * as OAuth from 'oauth-1.0a';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Location } from './models/location';
import { LocationSuggest } from './models/location-suggest';
import { WeatherData } from './models/weather-data';
import {LocationModel} from "../../shared/geo/location.model";

@Injectable()
export class WeatherApisService {
    private apiUrl = 'https://weather-ydn-yql.media.yahoo.com/forecastrss';

    private clientOAuthOptions: OAuth.Options = {
        consumer: {
            key: 'dj0yJmk9ZmFVb2cya3E1Ym03JmQ9WVdrOVZsUlBjWHBQTldVbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD02Yw--',
            secret: 'fb6a2ac83f818dd2cf47bca7c836b40039f9f5cd'
        },
        signature_method: 'PLAINTEXT',
        realm: ''
    };

    constructor(private http: HttpClient) { }

    public getWeatherForecast(city: string, tempType: string): Promise<WeatherData[]> {
        return new Promise(resolve => {
            const oauth = new OAuth(this.clientOAuthOptions);
            $.ajax({
                url: this.apiUrl,
                type: 'POST',
                data: oauth.authorize({
                    url: this.apiUrl,
                    method: 'POST',
                    data: { location: city, u: tempType }
                })
            }).done(function (data) {
                const weatherDatas: WeatherData[] = [];
                const condition = $(data).find('item>yweather\\:condition');
                const wind = $(data).find('channel>yweather\\:wind');
                const atmosphere = $(data).find('channel>yweather\\:atmosphere');
                const astronomy = $(data).find('channel>yweather\\:astronomy');
                const units = $(data).find('channel>yweather\\:units');
                weatherDatas.push({
                    day: 'Today',
                    temp: parseInt(condition.attr('temp'), 10),
                    conditionCode: parseInt(condition.attr('code'), 10),
                    condition: condition.attr('text'),
                    wind: { chill: +wind.attr('chill'), direction: +wind.attr('direction'), speed: +wind.attr('speed') },
                    atmosphere: { humidity: +atmosphere.attr('humidity'), pressure: +atmosphere.attr('pressure') },
                    astronomy: { sunrise: astronomy.attr('sunrise'), sunset: astronomy.attr('sunset') },
                    unit: {
                        distance: units.attr('distance'), pressure: units.attr('pressure'),
                        speed: units.attr('speed'), temperature: units.attr('temperature')
                    }
                });

                $(data).find('item>yweather\\:forecast').each((index, forecast) => {
                    weatherDatas.push({
                        day: $(forecast).attr('day'),
                        high: parseInt($(forecast).attr('high'), 10),
                        low: parseInt($(forecast).attr('low'), 10),
                        conditionCode: parseInt($(forecast).attr('code'), 10),
                        condition: $(forecast).attr('text')
                    });
                });

                resolve(weatherDatas);
            });
        });
    }

    public getSuggestedLocations(searchText: string): Promise<LocationModel[]> {
        return new Promise(resolve => {
            this.http.get('https://www.yahoo.com/news/_tdnews/api/resource/WeatherSearch;text=' + searchText)
                .toPromise().then(res => {
                    let result: LocationModel[] = [];
                    if (res instanceof Array) {
                        res.forEach(item => {
                            let localitem: LocationModel = new LocationModel();
                            localitem.postalCode = "";
                            localitem.countryCode = item.country;
                            localitem.city = item.city;
                            localitem.latitude = item.lat;
                            localitem.longitude = item.lon;
                            localitem.woeid = item.woeid;

                            result.push(localitem);
                        });
                    }
                    resolve(result);
                });
        });
    }
}
