import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
    MatAutocompleteModule,
    MatTooltipModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatRadioModule,
    MatInputModule,
    MatSlideToggleModule
} from '@angular/material';

import { BaseStructuresModule } from '../../shared/base-structures/base-structures.module';
import { UtilsModule } from '../../shared/utils/utils.module';
import { WidgetModule } from '../widget/widget.module';
import { WidgetWeatherComponent } from './component/widget-weather.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,

        MatFormFieldModule,
        MatRadioModule,
        MatInputModule,
        MatButtonModule,
        UtilsModule,
        BaseStructuresModule,
        WidgetModule,
        MatSlideToggleModule,
        MatIconModule,
        MatTooltipModule,
        MatAutocompleteModule
    ],
    declarations: [WidgetWeatherComponent],
    exports: [
        WidgetWeatherComponent,
    ]
})
export class WidgetWeatherModule {
}
