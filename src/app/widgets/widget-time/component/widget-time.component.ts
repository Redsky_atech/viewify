import { AppStorageService } from 'src/app/shared/storage/services/app-storage.service';

import {AfterViewInit, Component, ElementRef, OnDestroy} from '@angular/core';

import { WidgetBaseComponent } from '../../../shared/base-structures/widget-base.component';
import { TimeSettings } from '../models/time-settings';
import {WidgetType} from "../../../shared/base-structures/widget-type";
import {FireStoreService} from "../../../firebase/fire-store.service";
import {TabsService} from "../../../shared/storage/services/tabs.service";

@Component({
    selector: 'app-widget-time',
    templateUrl: './widget-time.component.html',
    styleUrls: ['./widget-time.component.scss'],
})
export class WidgetTimeComponent extends WidgetBaseComponent<TimeSettings> implements OnDestroy, AfterViewInit{
    public currentTime: number;
    private interval: number;
    constructor(protected appStorage: AppStorageService,
                protected fireStore: FireStoreService,
                protected tabsSvc: TabsService) {
        super(appStorage, fireStore, tabsSvc);
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        clearInterval(this.interval);
    }

    ngAfterViewInit(): void {
        /*if (this.position == 'bottom') {
            $(this.element.nativeElement).closest('.app-widget-full-content-body').addClass('transparent-widget');
        }*/
    }

    widgetInit(): void {
        if(!this.initialized) {
            if (!this.defaultSettings) this.defaultSettings = {types: [WidgetType.icon], showHeaderByDefault: false, headerNameByDefault: ""}

            this.startWork()
                .then(() => {
                    this.currentTime = Date.now();
                    this.interval = setInterval(() => this.currentTime = Date.now(), 1000);
                    this.listenSync();
                });
            this.initialized = true;
        }
    }

    public reload(): void {
        // Todo
    }
}

