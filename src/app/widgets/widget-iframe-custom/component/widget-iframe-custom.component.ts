import { AppStorageService } from 'src/app/shared/storage/services/app-storage.service';

import {
     Component, ElementRef
} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { WidgetIframeComponent } from '../../widget-iframe/component/widget-iframe.component';
import {HttpClient} from "@angular/common/http";
import {MatSnackBar} from "@angular/material";
import {FireStoreService} from "../../../firebase/fire-store.service";
import {TabsService} from "../../../shared/storage/services/tabs.service";

@Component({
    selector: 'app-widget-iframe-custom',
    templateUrl: './widget-iframe-custom.component.html',
    styleUrls: ['./widget-iframe-custom.component.scss']
})
export class WidgetIframeCustomComponent extends WidgetIframeComponent {
    private readonly mobileWebs: string[] = [
        'https://facebook.com', 'https://www.facebook.com', 'facebook.com', 'www.facebook.com',
        'http://facebook.com', 'http://www.facebook.com',
        'https://reddit.com', 'https://www.reddit.com', 'reddit.com', 'www.reddit.com',
        'http://reddit.com', 'http://www.reddit.com',
        'https://youtube.com', 'https://www.youtube.com', 'youtube.com', 'www.youtube.com',
        'http://youtube.com', 'http://www.youtube.com',
        'https://mobile.twitter.com', 'https://www.mobile.twitter.com', 'mobile.twitter.com', 'www.mobile.twitter.com',
        'http://mobile.twitter.com', 'http://www.mobile.twitter.com',
        'https://music.youtube.com', 'https://www.music.youtube.com', 'music.youtube.com', 'www.music.youtube.com',
        'http://music.youtube.com', 'http://www.music.youtube.com',
        'https://web.whatsapp.com', 'https://www.web.whatsapp.com', 'web.whatsapp.com', 'www.web.whatsapp.com',
        'http://web.whatsapp.com', 'http://www.web.whatsapp.com'
    ];
    public inputUrl: string;

    constructor(
        private http: HttpClient,
        protected sanitizer: DomSanitizer,
        protected appStorage: AppStorageService,
        protected element: ElementRef,
        public snackbar: MatSnackBar,
        protected fireStore: FireStoreService,
        protected tabsSvc: TabsService
    ) {
        super(sanitizer, appStorage, element, fireStore, tabsSvc);
    }

    widgetInit(): any {
        if(!this.initialized) {
            this.startWork()
                .then(() => {
                    this.showIframe(this.settings, this.sanitizer);
                    this.listenSync();
                });
            this.initialized = true;
        }
    }

    public submitUrl(): void {
        if (this.mobileWebs.includes(this.inputUrl)) {
            if (this.inputUrl.startsWith('http://') || this.inputUrl.startsWith('https://')) {
                let url = '';
                if (this.inputUrl.startsWith('https://')) {
                    url = this.inputUrl.substring(8);
                } else {
                    url = this.inputUrl.substring(7);
                }
                if (url.startsWith('www.')) url = url.substring(4);
                this.useValidUrl('https://m.'+url);
            } else {
                let url = this.inputUrl;
                if (url.startsWith('www.')) url = url.substring(4);
                this.useValidUrl('https://m.'+url);
            }
        } else if (this.inputUrl.startsWith('https://') || this.inputUrl.startsWith('http://')) {
            this.http.get(this.inputUrl, {observe: 'response'})
                .subscribe(res => {
                    if (res.status == 200) {
                        this.useValidUrl(this.inputUrl);
                    } else {
                        this.openSnack('Website not found!', 'error', 'snack_error');
                    }
                }, error => {
                    if (error.status == 200) {
                        this.useValidUrl(this.inputUrl);
                    } else this.openSnack('Website not found!', 'error', 'snack_error');
                });
        } else if (this.inputUrl.length > 4) {
            let httpsUrl = "https://"+this.inputUrl;
            let httpUrl = "http://"+this.inputUrl;
            this.checkUrl(httpUrl, httpsUrl);
        } else this.openSnack('Website not found!', 'error', 'snack_error');
    }

    private checkUrl(httpUrl: string, httpsUrl: string) {
        this.http.get(httpsUrl, {observe: 'response'})
            .subscribe(res => {
                if (res.status == 200) {
                    this.useValidUrl(httpsUrl);
                } else {
                    this.http.get(httpUrl, {observe: 'response'})
                        .subscribe(res => {
                            if (res.status == 200) {
                                this.useValidUrl(httpUrl);
                            } else {
                                this.openSnack('Website not found!', 'error', 'snack_error');
                            }
                        }, error => {
                            if (error.status == 200) {
                                this.useValidUrl(httpUrl);
                            } else this.openSnack('Website not found!', 'error', 'snack_error');
                        });
                }

            }, error => {
                if (error.status == 200) {
                    this.useValidUrl(httpsUrl);
                } else this.openSnack('Website not found!', 'error', 'snack_error');
            });
    }

    private useValidUrl(url: string) {
        if (url.startsWith('https://')) this.settings.headerName,
                                    this.settingHeaderName = url.length > 22?url.substring(8, 22): url.substring(8);
        else if (url.startsWith('http://')) this.settings.headerName,
                                    this.settingHeaderName = url.length > 21?url.substring(7, 21): url.substring(7);
        else this.settingHeaderName = url.length > 14?url.substring(0, 14): url.substring(0);

        this.inputUrl = url;
        this.url = this.sanitizer.bypassSecurityTrustResourceUrl(url);
        this.settings.url = url;
        this.saveSettings();
    }

    private openSnack(message: string, action: string, styleClass: string) {
        this.snackbar.open(message, action, {
            duration: 3500,
            panelClass: ['snack-bar_base', styleClass]
        });
    }
}
