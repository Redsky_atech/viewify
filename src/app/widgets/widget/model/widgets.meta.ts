export const widgetsList = {
    "w-todo": {
        key: "w-todo",
        countries: ["World"],
        type: "native",
        categories: ["Utility", "Productivity"]
    },
    "w-quotes": {
        key: "w-quotes",
        countries: ["World"],
        type: "native",
        categories: ["Productivity"]
    },
    "w-calculator": {
        key: "w-calculator",
        countries: ["World"],
        type: "native",
        categories: ["Utility"]
    },
    "w-events": {
        key: "w-events",
        countries: ["US", "CA"],
        type: "native",
        categories: ["Entertainment"]
    },
    "w-movies": {
        key: "w-movies",
        countries: ["US"],
        type: "native",
        categories: ["Entertainment"]
    },
    "w-weather": {
        key: "w-weather",
        countries: ["World"],
        type: "native",
        categories: ["Utility"]
    },
    "w-time": {
        key: "w-time",
        countries: ["World"],
        type: "native",
        categories: ["Utility"]
    },
    "w-stock": {
        key: "w-stock",
        countries: ["World"],
        type: "native",
        categories: ["News"]
    },
    "w-translate": {
        key: "w-translate",
        countries: ["World"],
        type: "native",
        categories: ["Utility"]
    },
    "w-currency": {
        key: "w-currency",
        countries: ["World"],
        type: "native",
        categories: ["Utility"]
    },
    "w-calendar": {
        key: "w-calendar",
        countries: ["World"],
        type: "native",
        categories: ["Utility", "Productivity"]
    },
    "w-news": {
        key: "w-news",
        countries: ["World"],
        type: "native",
        categories: ["News"]
    },
    "w-youtube-music": {
        key: "w-youtube-music",
        countries: ["World"],
        type: "web",
        categories: ["Entertainment"]
    },
    "w-whatsapp": {
        key: "w-whatsapp",
        countries: ["World"],
        type: "web",
        categories: ["Social"]
    },
    "w-g-mail": {
        key: "w-g-mail",
        countries: ["World"],
        type: "web",
        categories: ["Utility"]
    },
    "w-facebook": {
        key: "w-facebook",
        countries: ["World"],
        type: "web",
        categories: ["Social"]
    },
    "w-youtube": {
        key: "w-youtube",
        countries: ["World"],
        type: "web",
        categories: ["Entertainment"]
    },
    "w-instagram": {
        key: "w-instagram",
        countries: ["World"],
        type: "web",
        categories: ["Social"]
    },
    "w-reddit": {
        key: "w-reddit",
        countries: ["World"],
        type: "web",
        categories: ["Social"]
    },
    "w-g-keep": {
        key: "w-g-keep",
        countries: ["World"],
        type: "web",
        categories: ["Utility", "Productivity"]
    },
    "w-custom": {
        key: "w-custom",
        countries: ["World"],
        type: "web",
        categories: ["All"]
    },
    "w-drive": {
        key: "w-drive",
        countries: ["World"],
        type: "web",
        categories: ["Utility"]
    },
    "w-dictionary": {
        key: "w-dictionary",
        countries: ["World"],
        type: "web",
        categories: ["Utility"]
    },
    "w-radio": {
        key: "w-radio",
        countries: ["World"],
        type: "web",
        categories: ["Utility"]
    },
    "w-unit-converter": {
        key: "w-unit-converter",
        countries: ["World"],
        type: "web",
        categories: ["Utility"]
    }
}
