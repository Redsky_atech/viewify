import { Observable, Subscription } from 'rxjs';
import { WidgetSettings } from 'src/app/shared/base-structures/widget-settings';

import {
    AfterViewInit,
    ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, TemplateRef,
    ViewChild, ViewEncapsulation
} from '@angular/core';
import { DialogPosition, MatDialog, MatDialogRef } from '@angular/material';

import { WidgetDefaultSettings } from '../../../shared/base-structures/widget-default-settings';
import { WidgetType } from '../../../shared/base-structures/widget-type';

import * as $ from "jquery";

@Component({
    selector: "app-widget-info",
    template: "<div class='widget-info_wrapper'><ng-content></ng-content></div>"
})
export class WidgetInfoComponent implements OnInit {
    constructor(){}

    ngOnInit() {
    }
}

@Component({
    selector: 'app-widget-full',
    template: '<ng-content></ng-content>',
})
export class WidgetFullComponent implements OnInit {

    constructor() { }

    ngOnInit() {
    }
}

@Component({
    selector: 'app-widget-icon',
    template: '<div class="app-widget-icon-wrapper"><ng-content></ng-content></div>',
    styles: [
        `.app-widget-icon-wrapper, .app-widget-icon-wrapper > * {
            width: 100%;
            height: 100%;
        }`,
    ],
    encapsulation: ViewEncapsulation.None,
})
export class WidgetIconComponent implements OnInit {

    constructor() { }

    ngOnInit() {
    }
}

@Component({
    selector: 'app-widget-settings-on-adding',
    template: `
        <div class="setting-item">
            <span class="setting-item-label">Widget Header</span>
            <mat-slide-toggle (change)="onHeaderToggle($event)" [checked]="settings.showHeader"></mat-slide-toggle>
        </div>
        <div class="setting-item">
            <span class="setting-item-label">Widget Name</span>
            <mat-form-field>
                <input matInput [(ngModel)]="settings.headerName" (keyup)="sync()"
                    autocomplete="off" spellcheck="false">
            </mat-form-field>
        </div>
        <ng-content></ng-content>
    `,
})
export class WidgetSettingsOnAddingComponent implements OnInit {
    @Input() settings: WidgetSettings;
    @Output() syncSettings: EventEmitter<WidgetSettings> = new EventEmitter<WidgetSettings>();

    constructor() { }

    ngOnInit() {
    }

    public onHeaderToggle(value): void {
        if (value.checked === false) {
            this.settings.showHeader = false;
        } else {
            this.settings.showHeader = true;
        }
        this.sync();
    }

    sync(): void {
        this.syncSettings.emit(this.settings);
    }
}

@Component({
    selector: 'app-widget-settings',
    template: '<ng-content></ng-content>',
})
export class WidgetSettingsComponent implements OnInit {

    constructor() { }

    ngOnInit() {
    }
}

@Component({
    selector: 'app-widget',
    templateUrl: './widget.component.html',
    styleUrls: ['./widget.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class WidgetComponent implements OnInit, OnDestroy, AfterViewInit {
    @Input() headerName: string;
    @Input() showHeader: boolean;
    @Input() frozen = false;
    @Input() open: Observable<any>;
    @Input() hide: Observable<any>;
    @Input() remove: Observable<any>;
    @Input() opened: () => any;
    @Input() type: WidgetType;
    @Input() subType: string;
    @Input() key: string;
    @Input() settings: WidgetDefaultSettings;
    @Input() minimizedSubj: Observable<any>;
    @Input() userData: any;
    @Input() position: string;
    @Input() reloadable: boolean;
    @Input() poweredImg: string;
    @Input() poweredBy: boolean;

    @Output() reload: EventEmitter<any> = new EventEmitter();
    @Output() hideWidget: EventEmitter<any> = new EventEmitter();
    @Output() minimizedEmiter: EventEmitter<any> = new EventEmitter();
    @Output() save: EventEmitter<any> = new EventEmitter();
    @Output() close: EventEmitter<any> = new EventEmitter();
    @Output() init: EventEmitter<any> = new EventEmitter();

    @ViewChild('full') fullTpl: TemplateRef<any>;
    //private settingsDialogRef: MatDialogRef<any, any>;
    //@ViewChild('settings') settingsTpl: TemplateRef<any>;
    private matDialogRef: MatDialogRef<any, any>;
    private sub: Subscription = new Subscription();
    protected _ignoreHideMenu = false;
    openFullWindow: boolean = true;
    openSettings: boolean = false;
    openDeleteWindow: boolean = false;
    minimized: boolean = false;
    ymusicHidden: boolean = false;
    showInfo: boolean = false;
    minimizedOnStart: boolean = false;

    constructor(
        public dialog: MatDialog,
        private cdr: ChangeDetectorRef,
        public element: ElementRef
    ) {
    }

    ngOnInit() {
        if (this.type === WidgetType.icon && !this.frozen) {
            if (this.hide) {
                this.sub.add(this.hide.subscribe(() => {
                    if (this._ignoreHideMenu) {
                        this._ignoreHideMenu = false;
                        return;
                    } else {
                        if (this.matDialogRef) {
                            this.matDialogRef.close();
                            this.matDialogRef = null;
                            this.cdr.markForCheck();
                        }
                    }
                }));
            }
        }

        if (this.open) {
            this.sub.add(this.open.subscribe(() => this.iconClick()));
        }

        if (this.remove) {
            this.sub.add(this.remove.subscribe(() => {
                this.openDeleteWindow = true;
                this.iconClick();
            }));
        }

        if (this.minimizedSubj && this.type != 'on-adding') {
            this.sub.add(this.minimizedSubj.subscribe((result) => {
                this.minimized = !result;
                this.minimizeWidget(this.element.nativeElement);
            }));
        }
    }


    ngAfterViewInit(): void {
        /*if (!this.reloadable) {
            $(this.element.nativeElement).closest('.widget-content').children('.drag-icon').addClass('drag-icon_right');
        }*/
        /*if (this.poweredBy) {
            $(this.element.nativeElement).closest('.widget-content').children('.drag-icon').addClass('powered-widget_grag');
            $(this.element.nativeElement).find('.actions-icons').addClass('powered-widget_icons');
        }*/
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }

    public iconClick(event?): void {
        if (event) event.stopPropagation();

        if (!this.frozen) {
            if (!this.matDialogRef) {
                let config = {
                    hasBackdrop: true,
                    backdropClass: "modal-overlay_transparent-backdrop",
                    panelClass: ['app-widget-modal-overlay'],
                    data: {
                        widgets: []
                    },
                    position: this.dialogPosition
                }
                this.opened();
                this.init.emit();
                this.matDialogRef = this.dialog.open(this.fullTpl, config);
                this.sub.add(this.matDialogRef.afterOpen().subscribe(() => {
                    let el = $('.app-widget-modal-overlay');
                    el.css('overflow-y', 'auto');
                    /*if (this.poweredBy) {
                        el.find('.actions-icons').addClass('powered-widget_icons');
                    }*/
                }));
                this.sub.add(this.matDialogRef.beforeClose().subscribe(() => {
                    $('.app-widget-modal-overlay').css('overflow-y', 'hidden');
                }));
                this.sub.add(this.matDialogRef.backdropClick().subscribe(() => {
                    this.closeWidget();
                }));
            } else {
                this.closeWidget();
            }
        }
    }

    public onOpenSettings(): void {
        if (!this.minimized) {
            this.openDeleteWindow = false;
            this.showInfo = false;
            this.openSettings = !this.openSettings;
        }
    }

    public onHideWidget(): void {
        if (this.matDialogRef) {
            this.matDialogRef.close();
            this.matDialogRef = null;
        }
        this.hideWidget.emit();
    }

    public onReload() {
        if (!this.minimized) {
            this.reload.emit();
        }
    }

    public openInfo() {
        if (!this.minimized) {
            this.showInfo = !this.showInfo;
            this.openDeleteWindow = false;
            this.openSettings = false;
        }
    }

    public closeWidget(): void {
        this.openDeleteWindow = false;
        if (this.matDialogRef) {
            this.matDialogRef.close();
            this.matDialogRef = null;
        }
    }

    public saveSettings() {
        if (!this.minimized) {
            this.openSettings = !this.openSettings;
            this.save.emit();
        }
    }

    public minimizeWidget(target) {
        if (this.minimized) {
            if (this.subType == 'iframe') this.showHeader = false;
            this.minimized = false;
            this.openFullWindow = true;
            $(target).closest('.column-layer').removeClass('widget-minimize');
            //this.minimizedEmiter.emit('exclude');
            if (this.minimizedOnStart) {
                this.minimizedEmiter.emit(false);
            } else {
                this.minimizedOnStart = true;
            }
        } else {
            if (this.subType == 'iframe') this.showHeader = true;
            this.minimized = true;
            this.openSettings = false;
            this.openDeleteWindow = false;
            this.openFullWindow = false;
            $(target).closest('.column-layer').addClass('widget-minimize');
            //this.minimizedEmiter.emit('exclude');
            if (this.minimizedOnStart) {
                this.minimizedEmiter.emit(true);
            } else {
                this.minimizedOnStart = true;
            }
        }
        /*setTimeout(() => {
            this.cdr.detectChanges();
        }, 1000);*/
    }

    public declineDelete() {
        this.openDeleteWindow = false;
    }

    public askForDelete() {
        if (!this.minimized) {
            this.openDeleteWindow = !this.openDeleteWindow;
            this.showInfo = false;
            this.openSettings = false;
        }
    }

    private get dialogPosition(): DialogPosition {
        if (this.settings && this.settings.widgetIconSettings &&
            this.settings.widgetIconSettings.dialogMarginBottom && this.settings.widgetIconSettings.dialogMarginLeft) {
            return {
                bottom: this.settings.widgetIconSettings.dialogMarginBottom + 'px',
                left: this.settings.widgetIconSettings.dialogMarginLeft + 'px'
            };
        }
        return {
            top: '70px',
            left: '70px'
        };
    }

}
