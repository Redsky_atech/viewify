import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatIconModule, MatTooltipModule } from '@angular/material';

import { BaseStructuresModule } from '../../shared/base-structures/base-structures.module';
import {
    WidgetComponent,
    WidgetFullComponent,
    WidgetIconComponent,
    WidgetInfoComponent,
    WidgetSettingsComponent,
    WidgetSettingsOnAddingComponent
} from './component/widget.component';

@NgModule({
    imports: [
        CommonModule,
        BaseStructuresModule,
        MatIconModule,
        MatTooltipModule
    ],
    declarations: [
        WidgetComponent,
        WidgetIconComponent,
        WidgetInfoComponent,
        WidgetFullComponent,
        WidgetSettingsComponent,
        WidgetSettingsOnAddingComponent
    ],
    exports: [
        WidgetComponent,
        WidgetIconComponent,
        WidgetInfoComponent,
        WidgetFullComponent,
        WidgetSettingsComponent,
        WidgetSettingsOnAddingComponent
    ]
})
export class WidgetModule {
}
