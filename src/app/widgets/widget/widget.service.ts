import {WidgetSettings} from "../../shared/base-structures/widget-settings";
import {WidgetDefaultSettings} from "../../shared/base-structures/widget-default-settings";
import {AppStorageService} from "../../shared/storage/services/app-storage.service";
import {OnDestroy} from "@angular/core";
import {Subscription} from "rxjs/Rx";
import {ChromeStorageService} from "../../shared/storage/services/chrome-storage.service";
import {base_storage_keys} from "../../shared/model/storage-keys";
import {take} from "rxjs/internal/operators";
import {FireStoreService} from "../../firebase/fire-store.service";
import {WidgetType} from "../../shared/base-structures/widget-type";
import {TabsService} from "../../shared/storage/services/tabs.service";

export abstract class WidgetService<Settings extends WidgetSettings> implements OnDestroy {
    isWorking: boolean = false;
    public settings: Settings = <Settings> {headerName: '', showHeader: false, minimized: false};
    public settingShowHeader: boolean;
    public settingHeaderName: string = '';
    public syncable: boolean = false;
    private syncTab: boolean = false;
    maxHeaderLength: number = 19;
    protected storageKey: any;
    public defaultSettings: WidgetDefaultSettings;
    private rootSubs: Subscription = new Subscription();
    constructor (protected appStorage: AppStorageService,
                 protected storage: ChromeStorageService,
                 protected fireStore: FireStoreService,
                 protected tabsSvc: TabsService) {
    }

    ngOnDestroy(): void {
        this.rootSubs.unsubscribe();
    }

    startWork(settings: WidgetDefaultSettings, key: any): Promise<any> {
        return new Promise<any>(resolve => {
            this.defaultSettings = settings;
            this.storageKey = key;

            this.appStorage.get(this.storageKey)
                .pipe(take(1))
                .subscribe(storageSettings => {
                    if (this.defaultSettings) {
                        this.settings.showHeader = this.defaultSettings.showHeaderByDefault;
                        this.settings.headerName = this.defaultSettings.headerNameByDefault;
                    } else {
                        this.settings.showHeader = false;
                        this.settings.headerName = "";
                    }

                    if (storageSettings) {
                        this.settings = Object.assign(this.settings, storageSettings);
                    }

                    this.settingShowHeader = this.settings.showHeader;
                    this.settingHeaderName = this.settings.headerName;

                    resolve();
            });
        });
    }

    public lengthLimit() {
        if (this.settingHeaderName.length > this.maxHeaderLength) {
            this.settingHeaderName = this.settingHeaderName.substring(0, this.maxHeaderLength);
        }
    }

    public abstract afterTabSync();

    public listenSync () {
        if (this.fireStore.authFire.auth.currentUser) {
            this.rootSubs.add(this.fireStore.syncWithFire.subscribe((reset) => {
                if (reset.type == "marker") {
                    this.syncable = false;
                } else if (reset.type == "data" && (!reset.keys || reset.keys.includes(this.storageKey.keyName))) {
                    this.startWork(this.defaultSettings, this.storageKey);
                }
            }));
        }
    }

    saveSettings() {
        if (this.storageKey) this.tabsSvc.addChangedTabKey(this.storageKey.keyName);
        if (this.settingHeaderName) this.settings.headerName = this.settingHeaderName;
        this.settings.showHeader = this.settingShowHeader;
        this.appStorage.set<Settings>(this.storageKey, JSON.parse(JSON.stringify(this.settings)));
        if (!this.syncable && this.fireStore.authFire.auth.currentUser) {
            this.syncable = true;
            this.storage.get(base_storage_keys.sync_keys)
                .pipe(take(1))
                .subscribe((k: any) => {
                    let keys = k;
                    if (!keys) keys = {};
                    keys[this.storageKey.keyName] = 'save';
                    this.storage.set(base_storage_keys.sync_keys, keys);
                });
        }
    }
    public listenTabSync(settings: WidgetDefaultSettings, key: any) {
        if (settings && key) {
            this.rootSubs.add(this.tabsSvc.addTabObserver(key.keyName)
                .subscribe(() => {
                    //this.syncTab = true;
                    this.startWork(settings, key)
                        .then(() => {
                            this.afterTabSync();
                        })
                }));
        }
    }

}
