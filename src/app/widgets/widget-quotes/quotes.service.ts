import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Rx";

@Injectable()
export class QuoteService {
    urlList = {
        forismatic: 'https://api.forismatic.com/api/1.0/?method=getQuote&format=json&lang=en',
        forbes: 'https://www.forbes.com/forbesapi/thought/get.json?limit=1&random=true&stream=true'
    }
    quotesSourceUrl = this.urlList.forismatic;

    constructor (private http: HttpClient) {}

    getQuote(quoteSource): Observable<any> {
        if(quoteSource == "" || quoteSource == "forismatic") {
            this.quotesSourceUrl = this.urlList.forismatic;
        } else if(quoteSource == "forbes") {
            this.quotesSourceUrl = this.urlList.forbes;
        }
        return this.http.get<string>(this.quotesSourceUrl, {responseType: 'text' as 'json'});
    }
}
