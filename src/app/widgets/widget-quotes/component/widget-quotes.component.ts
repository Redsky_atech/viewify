import {Component} from '@angular/core';
import {QuoteSettings} from "../model/quotes-settings";
import {WidgetBaseComponent} from "../../../shared/base-structures/widget-base.component";
import {AppStorageService} from "../../../shared/storage/services/app-storage.service";
import {QuoteService} from "../quotes.service";
import {Quote} from "../../../quote/quote";
import {take} from "rxjs/internal/operators";
import {FireStoreService} from "../../../firebase/fire-store.service";
import {TabsService} from "../../../shared/storage/services/tabs.service";
import { ChromeStorageService } from "../../../shared/storage/services/chrome-storage.service";

@Component({
  selector: 'app-widget-quote',
  templateUrl: './widget-quotes.component.html',
  styleUrls: ['./widget-quotes.component.scss'],
  //encapsulation: ViewEncapsulation.None
})
export class WidgetQuoteComponent extends WidgetBaseComponent<QuoteSettings> {
    private tweetLink = "https://twitter.com/intent/tweet?text=";
    quote = new Quote();
    quoteSource: string = "";

    constructor(
        protected appStorage: AppStorageService,
        public quoteSvc: QuoteService,
        protected fireStore: FireStoreService,
        protected tabsSvc: TabsService,
        private _storage: ChromeStorageService
    ) {
        super(appStorage, fireStore, tabsSvc);
    }

    toQuoteSource() {
        window.open(this.quote.quoteLink);
    }

    getNewQuote(quoteSource: string) {
        this.quote = new Quote();
        this.quoteSvc.getQuote(quoteSource)
            .pipe(take(1))
            .subscribe((q: any) => {
                if (q) {
                    let quoteObj = new Quote();
                    if(quoteSource == "" || quoteSource == "forismatic"){
                        quoteObj = this.parseToQuote(q);
                    } else {
                        let quoteData = JSON.parse(q);
                        quoteObj.quoteAuthor = quoteData.thoughtStream.thoughtAuthor.name;
                        quoteObj.quoteText = quoteData.thoughtStream.thoughts[0].quote;
                    }
                    if (quoteObj.quoteText) {
                        if(quoteObj.quoteText.length && quoteObj.quoteText.length>160){
                            this.getNewQuote(quoteSource);
                            return;
                        }
                        this.quote = quoteObj;
                        this.quote.quoteText = `"${this.quote.quoteText}"`;
                        this.quote.quoteAuthor = "— "+this.quote.quoteAuthor;
                    }
                }
            });
    }

    tweetQuote() {
        window.open(this.tweetLink+this.quote.quoteText);
    }

    public widgetInit() {
        if(!this.initialized) {
            this.startWork()
                .then(() => {
                    if(!this.isFromAddPopUp) {
                        this.getQuoteSource();
                    }
                    this.listenSync();
                });
            this.initialized = true;
        }
    }

    public reload() {
        this.getQuoteSource();
    }

    private parseToQuote(quoteStr: string){
        let quoteObj = new Quote();
        if(quoteStr && quoteStr.indexOf("\'")>-1){
            let woQuoteStr = quoteStr.replace(/\\/g,"");
            quoteObj = JSON.parse(woQuoteStr);
        }else{
            quoteObj = JSON.parse(quoteStr);
        }
        return quoteObj;
    }

    getQuote() {
        console.log("iss  "+this.isFromAddPopUp);
        this.getQuoteSource(true);
        this.isFromAddPopUp = false;
    }

    getQuoteSource(haveQuoteSource: boolean = false) {
        let source = ""
        if(haveQuoteSource) {
            source = this.quoteSource;
            this._storage.set("quoteSource", this.quoteSource);
            this.getNewQuote(source);
        } else {
            this._storage.get("quoteSource").pipe(take(1))
            .subscribe(data => {
                if(data) {
                    source = data;
                } else {
                    source = "";
                }
                this.getNewQuote(source);
            })
        }
    }
}
