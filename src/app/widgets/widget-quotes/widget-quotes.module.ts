import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WidgetQuoteComponent } from './component/widget-quotes.component';
import {WidgetModule} from "../widget/widget.module";
import {BaseStructuresModule} from "../../shared/base-structures/base-structures.module";

@NgModule({
  imports: [
    CommonModule,
    BaseStructuresModule,
    WidgetModule,
  ],
  exports: [
    WidgetQuoteComponent
  ],
  declarations: [WidgetQuoteComponent]
})
export class WidgetQuoteModule { }
