import {
    AfterViewInit,
    ChangeDetectorRef, Component, EventEmitter, OnDestroy, OnInit, Output, TemplateRef,
    ViewChild
} from '@angular/core';
import {Subject} from 'rxjs';
import {Widget} from '../../shared/base-structures/widget';
import {WidgetType} from '../../shared/base-structures/widget-type';
import {ChromeStorageService} from '../../shared/storage/services/chrome-storage.service';
import {WidgetsDynamicService} from '../../widgets/widgets-dynamic/widgets-dynamic.service';
import {MatDialog, MatDialogRef} from "@angular/material";
import {FullUnitStorageable} from "../../full-view/models/full-unit-storageable";
import {IconUnitStorageable} from "../../icon-view/models/icon-unit-storageable";
import {PageInfo} from "../model/page-info";
import {base_storage_keys} from "../../shared/model/storage-keys";
import {SortablejsOptions} from "angular-sortablejs";
import {AuthService} from "../../firebase/auth.service";
import {WeatherService} from "../../widgets/widget-weather/weather.service";
import {WeatherApisService} from "../../widgets/widget-weather/weather-apis.service";
import {Subscription} from "rxjs/Rx";
import * as $ from 'jquery';
import {take} from "rxjs/internal/operators";
import {FireStoreService} from "../../firebase/fire-store.service";
import {QuoteService} from "../../widgets/widget-quotes/quotes.service";
import {DeskService} from "../desk.service";
import {TabsService} from "../../shared/storage/services/tabs.service";
//import {RssService} from "../../widgets/widget-rss/rss.service";
//import {RSSParserService} from "../../widgets/widget-rss/rss-parser.service";
import {SharedService} from "../../shared/utils/shared.service";
import {SnackService} from "../../shared/utils/snack.service";
declare let ga: any;
//import {} from '@types/google.analytics';

enum ViewType {
    'icon' = 0,
    'full' = 1,
}

export class PagesWrapper {
    allPages: PageInfo[]
}

export class IconWrapper {
    iconWidgets: IconUnitStorageable[]
}

export class FullWrapper {
    pageSettings: {
        col_count: number
    };
    fullWidgets: FullUnitStorageable[]
}

@Component({
    selector: 'app-desk',
    templateUrl: './desk.component.html',
    styleUrls: ['./desk.component.scss'],
    //changeDetection: ChangeDetectionStrategy.OnPush,
    viewProviders: [
        WeatherService,
        WeatherApisService,
        QuoteService,
        /*RssService,
        RSSParserService*/
    ]
})
export class DeskComponent implements OnInit, OnDestroy, AfterViewInit {
    allWidgets: Widget[];
    fullWidgets: Widget[];
    iconWidgets: Widget[];
    currentView: string;
    defaultView: string;
    currentViewIndex: number;
    defaultViewIndex: number;
    addFullUnit: Subject<any> = new Subject();
    addIconUnit: Subject<any> = new Subject();
    hideOpenedW: Subject<void> = new Subject<void>();
    viewChange: Subject<any> = new Subject<number>();
    ymusicHidden: boolean = false;
    syncPages: boolean = false;
    syncDesk: boolean = false;
    timeOutId: any;

    searchMenus = [
        {id: 'search', name: 'Search', icon: 'icon ion-ios-search'},
        {id: 'shop', name: 'Shop', icon: 'icon ion-ios-cart'},
        {id: 'travel', name: 'Travel', icon: 'icon ion-ios-briefcase'},
        {id: 'eat', name: 'Eat', icon: 'icon ion-ios-restaurant'},
        {id: 'knowledge', name: 'Knowledge', icon: 'icon ion-md-school'}
    ];
    selectedMenu: any;
    @ViewChild("imgChanger") imgChangerTemplate: TemplateRef<any>;
    @ViewChild("pageMenu") pageMenuTemplate: TemplateRef<any>;
    @ViewChild("searchMenu") searchMenuTemplate: TemplateRef<any>;
    @ViewChild('bookmarksModal') bookmarksTemplate: TemplateRef<any>;
    @ViewChild('historyModal') historyTemplate: TemplateRef<any>;
    @ViewChild('youMusic') yMusicTemplate: TemplateRef<any>;
    @ViewChild('profileMenu') settingsTemplate: TemplateRef<any>;

    @Output() authenticated: EventEmitter<any> = new EventEmitter<any>();

    firstLoad: boolean = true;
    fullViews: PageInfo[] = [];
    homePage: PageInfo = new PageInfo();
    newPage: PageInfo = new PageInfo();
    options: SortablejsOptions;
    sub: Subscription = new Subscription();

    dialogRef: MatDialogRef<any, any>;
    musicDialogRef: MatDialogRef<any, any>;

    get viewTypeIndex() {
        return ViewType[this.currentView];
    }

    private defaults: {
        currentView: ViewType,
        defaultView: ViewType,
    } = {
        currentView: ViewType.icon,
        defaultView: ViewType.icon,
    };

    constructor(
        private widgetsService: WidgetsDynamicService,
        private storage: ChromeStorageService,
        private dialog: MatDialog,
        private deskSvc: DeskService,
        public authSvc: AuthService,
        private tabsSvc: TabsService,
        private detector: ChangeDetectorRef,
        private fireStorage: FireStoreService,
        private snackSvc: SnackService,
        private sharedSvc: SharedService
    ) {
        this.options = {
            handle: '.page-menu_handle',
            onUpdate: this.postChangesToStorage.bind(this)
        };
        if (this.authSvc.currentUser) {
            this.sub.add(fireStorage.syncWithFire
                .subscribe((reset) => {
                    if(reset.type == "marker") {
                        this.syncDesk = false;
                        this.syncPages = false;
                    } else if (reset.type == "data" && (!reset.keys || reset.keys.includes(base_storage_keys.pages))) {
                        this.getPages();
                    }
                }));
        }

        this.sub.add(tabsSvc.addTabObserver(base_storage_keys.pages)
            .subscribe(() => {
                this.getPages();
            }));

        this.sub.add(this.tabsSvc.addTabObserver(base_storage_keys.isAnonymous)
            .subscribe(() => {
                this.storage.get(base_storage_keys.isAnonymous)
                    .pipe(take(1))
                    .subscribe((a: boolean) => {
                        if(!a) this.logOut();
                    });
            }));
    }

    ngAfterViewInit(): void {
        setTimeout(() => {
            this.detector.detectChanges();
        }, 500)
    }

    onAddUnit(event) {
        if (this.currentView === ViewType[ViewType.full]) {
            this.addFullUnit.next(event);
        } else {
            //this.viewChange.next(ViewType.icon);  // close opened widgets
            this.addIconUnit.next(event);
        }
    }

    postChangesToStorage() {

    }

    ngOnInit() {
        this.sub.add(this.authSvc.authenticatedSubject
            .subscribe(result => {
                this.authenticated.emit(result);
                this.timeOutId = setTimeout(() => {
                    this.detector.detectChanges();
                }, 200);
            }));

        this.storage.get(base_storage_keys.pages)
            .pipe(take(1))
            .subscribe((pages: PagesWrapper) => {
                if (pages && pages.allPages) {
                    this.homePage = pages.allPages.shift();
                    this.fullViews = pages.allPages;
                } else {
                    if (this.authSvc.currentUser) {

                    } else {
                        this.deskSvc.loadDefaultStorageData()
                            .then(() => {
                                this.storage.get(base_storage_keys.pages)
                                    .pipe(take(1))
                                    .subscribe((pages: PagesWrapper) => {
                                        if (pages && pages.allPages) {
                                            this.homePage = pages.allPages.shift();
                                            this.fullViews = pages.allPages;
                                            this.syncPages = true;
                                        }
                                    });
                            });
                    }
                }

                this.widgetsService.widgets.pipe(take(1)).subscribe(widgets => {
                    this.storage.get(base_storage_keys.desk)
                        .pipe(take(1))
                        .subscribe((data: { v: number, i: number }) => {
                        this.allWidgets = widgets;
                        this.fullWidgets = widgets.filter(widget => widget.settings.types.includes(WidgetType.full));
                        this.iconWidgets = widgets.filter(widget => widget.settings.types.includes(WidgetType.icon));
                        if (data) {
                            this.currentView = ViewType[data.v];
                            this.defaultView = ViewType[data.v];
                            this.currentViewIndex = data.i ? data.i : 0;
                            this.defaultViewIndex = this.currentViewIndex;
                        } else {
                            this.currentView = ViewType[this.defaults.currentView];
                            this.defaultView = ViewType[this.defaults.defaultView];
                            this.defaultViewIndex = 0;
                            this.currentViewIndex = 0;
                        }
                        this.firstLoad = false;
                        this.viewChange.next(ViewType[this.currentView]);
                    });
                });
            });

        window.document.onkeydown = ((e) => {
            if (e.keyCode == 37) {
                if (this.currentView != 'icon') {
                    this.goLeft(0);
                }
            }
            else if (e.keyCode == 39) {
                if (this.currentView == 'icon') {
                    this.goFullView();
                } else if (this.currentView == 'full' && this.currentViewIndex != this.fullViews.length-1) {
                    this.goRight(0);
                }
            }
        });
    }

    ngOnDestroy(): void {
        clearTimeout(this.timeOutId);
        this.authSvc.clearInterval();
        this.sub.unsubscribe();
    }

    widgetOpened() {
        return () => this.hideOpenedW.next();
    }

    searchBarOpen(event) {
        this.viewChange.next(ViewType[this.currentView]);  // close opened widgets
    }

    openSearchMenu(event) {
        event.stopPropagation();
        this.dialogRef = this.dialog.open(this.searchMenuTemplate, {
            panelClass: "search-menu_modal-overlay",
            hasBackdrop: true,
            backdropClass: "modal-overlay_back-drop"
        });
        this.bugfixMultiCdkOverlay();
        this.addActiveIconListener(event);
    }

    goLeft(index: number) {
        if (this.currentViewIndex == 0) {
            this.goIconView();
        } else {
            this.currentViewIndex--;
        }
    }

    goRight(index: number) {
        this.currentViewIndex++;
    }

    goFullView() {
        this.viewChange.next(ViewType.full);
        this.currentView = ViewType[ViewType.full];
    }

    goIconView() {
        this.viewChange.next(ViewType.icon);
        this.currentView = ViewType[ViewType.icon];
    }

    selectMenu(menu,event) {
        event.stopPropagation();
        this.selectedMenu = menu;
        if (this.dialogRef) {
            this.dialogRef.close();
            this.dialogRef = null;
        }
    }

    toHomePage() {

    }

    closeSearch() {
        this.selectedMenu = null;
    }

    openSettingsMenu(event) {
        this.dialogRef = this.dialog.open(this.settingsTemplate, {
            panelClass: "settings-menu_overlay",
            hasBackdrop: true,
            backdropClass: "modal-overlay_back-drop",
            position: {
                top: "50px",
                right: "50px"
            }
        });

        this.bugfixMultiCdkOverlay();
        this.addActiveIconListener(event);
    }

    private logOut() {
        if (this.authSvc.currentUser) this.authSvc.signOut();
        else this.authenticated.emit(false);
        if (this.musicDialogRef) {
            this.musicDialogRef.close();
            this.musicDialogRef = null;
        }
        this.hideOpenedW.next();
    }

    private saveDesk(data: any) {
        if (!this.syncDesk && this.authSvc.currentUser) {
            this.syncDesk = true;
            this.sharedSvc.syncKey(base_storage_keys.desk);
        }
        this.storage.set(base_storage_keys.desk, data);
    }

    private bugfixMultiCdkOverlay() {
        if (this.currentView == 'icon') {
            this.dialogRef.afterOpen().pipe(take(1)).subscribe(() => {
                $('.app-widget-modal-overlay').closest('.cdk-global-overlay-wrapper').css('z-index', -1);
            });
            this.dialogRef.beforeClose().pipe(take(1)).subscribe(() => {
                $('.app-widget-modal-overlay').closest('.cdk-global-overlay-wrapper').css('z-index', 1000);
            });
        }
    }

    private getPages() {
        this.storage.get(base_storage_keys.pages)
            .pipe(take(1))
            .subscribe((pages: PagesWrapper) => {
                if (pages && pages.allPages) {
                    this.homePage = pages.allPages.shift();
                    this.fullViews = pages.allPages;
                }
            });
    }

    private addActiveIconListener(event) {
        $(event.srcElement).addClass('active-icon');
        this.dialogRef.beforeClose()
            .pipe(take(1))
            .subscribe(() => {
                $(event.srcElement).removeClass('active-icon');
            });
    }
}
