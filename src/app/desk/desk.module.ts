import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDialogModule,
    MatDividerModule,
    MatIconModule,
    MatMenuModule,
    MatSidenavModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatSnackBarModule
} from '@angular/material';
import {IconViewModule} from '../icon-view/icon-view.module';
import {DeskComponent} from './component/desk.component';
//import {SearchBarModule} from '../search-bar/search-bar.module';
import {BaseStructuresModule} from '../shared/base-structures/base-structures.module';
import {FullViewModule} from '../full-view/full-view.module';
import {WidgetsDynamicModule} from '../widgets/widgets-dynamic/widgets-dynamic.module';
import {FormsModule} from "@angular/forms";
import {SortablejsModule} from "angular-sortablejs";

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        SortablejsModule,
        MatCardModule,
        MatIconModule,
        MatDialogModule,
        MatMenuModule,
        MatButtonModule,
        MatToolbarModule,
        MatSidenavModule,
        MatTabsModule,
        MatTooltipModule,
        MatCheckboxModule,
        MatDividerModule,
        MatSnackBarModule,

        BaseStructuresModule,
        WidgetsDynamicModule,

        FullViewModule,
        IconViewModule,
    ],
    declarations: [
        DeskComponent,
    ],
    exports: [
        DeskComponent,
    ],
    providers: [
    ]
})
export class DeskModule {
}
