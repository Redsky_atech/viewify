export class PageInfo {
    index: number;
    name: string;
    defaultView: boolean;
    viewType: string;
    edit: boolean;
    showDelete: boolean;
}
