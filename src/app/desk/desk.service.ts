import {Injectable} from "@angular/core";
import {base_storage_keys} from "../shared/model/storage-keys";
import {take} from "rxjs/internal/operators";
import {ChromeStorageService} from "../shared/storage/services/chrome-storage.service";
import {PageInfo} from "./model/page-info";
import {AppStorageService} from "../shared/storage/services/app-storage.service";
import {AngularFireAuth} from "@angular/fire/auth";
import {LocationService} from "../shared/geo/location.service";

@Injectable()
export class DeskService {
    constructor(private storage: ChromeStorageService,
                private authFire: AngularFireAuth) {}

    public loadDefaultStorageData(): Promise<any> {
        return new Promise<any>(resolve => {
            this.loadDefaultWidgets()
                .then(() => {
                    this.loadDefaultPages()
                        .then(() => resolve()).catch(error => resolve());
                }).catch(error => resolve());
        });
    }

    private uniqueId() {
        return Math.random().toString(36).substr(2, 6);
    }

    private loadDefaultWidgets(): Promise<any> {
        return new Promise<any>(resolve => {
            resolve();
        });
    }

    private loadDefaultPages(): Promise<any> {
        return new Promise<any>(resolve => {
            let homePage = {
                index: 0,
                name: "Home",
                defaultView: true,
                viewType: "icon",
                edit: false,
                showDelete: false
            }
            let fullViews = [{
                index: 0,
                name: "My Widgets",
                defaultView: false,
                viewType: "full",
                edit: false,
                showDelete: false
            }, {
                index: 1,
                name: "Social",
                defaultView: false,
                viewType: "full",
                edit: false,
                showDelete: false
            }, {
                index: 2,
                name: "Entertainment",
                defaultView: false,
                viewType: "full",
                edit: false,
                showDelete: false
            }, {
                index: 3,
                name: "Gaming",
                defaultView: false,
                viewType: "full",
                edit: false,
                showDelete: false
            }];
            let allPages: PageInfo[] = [homePage, ...fullViews];

            this.savePages(allPages)
                .then(() => resolve()).catch(error => resolve());
        });
    }

    private savePages(allPages: PageInfo[]): Promise<any> {
        return new Promise<any>(resolve => {
            if (this.authFire.auth.currentUser) {
                this.storage.get(base_storage_keys.sync_keys)
                    .pipe(take(1))
                    .subscribe((k: any) => {
                        let keys = k;
                        if (!keys) keys = {};
                        keys[base_storage_keys.pages] = 'save';
                        this.storage.remove(base_storage_keys.sync_keys)
                            .pipe(take(1))
                            .subscribe(() => {
                                this.storage.set(base_storage_keys.sync_keys, keys);
                                this.storage.set(base_storage_keys.pages, {allPages: JSON.parse(JSON.stringify(allPages))});
                                resolve();
                            }, error => {
                                resolve()
                            });
                    }, error => {
                        resolve();
                    });
            } else {
                this.storage.set(base_storage_keys.pages, {allPages: JSON.parse(JSON.stringify(allPages))});
                resolve();
            }
        });
    }
}
