import { Observable, Subject } from 'rxjs';

import { DOCUMENT } from '@angular/common';
import {
    AfterViewInit, ChangeDetectorRef,
    Component, ElementRef, Inject, Input, OnDestroy, OnInit, TemplateRef, ViewChild
} from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';

import {
    WidgetsDynamicService
} from '../../../app/widgets/widgets-dynamic/widgets-dynamic.service';
import { DeskViewBaseComponent } from '../../shared/base-structures/desk-view-base.component';
import { Widget } from '../../shared/base-structures/widget';
import { WidgetDefaultSettings } from '../../shared/base-structures/widget-default-settings';
import { WidgetDragging } from '../../shared/base-structures/widget-dragging';
import {Subscription} from "rxjs/Rx";
import { take } from 'rxjs/operators';
import {AppStorageService} from "../../shared/storage/services/app-storage.service";
import {AppStorageKey} from "../../shared/storage/models/app-storage-key";
import {TodoSettings} from "../../widgets/widget-todo/models/todo-settings";
@Component({
    selector: 'app-icon-view',
    templateUrl: './icon-view.component.html',
    styleUrls: ['./icon-view.component.scss']
})
export class IconViewComponent extends DeskViewBaseComponent implements OnInit, OnDestroy, AfterViewInit {

    @Input() widgets: Widget[];
    @Input() addUnit: Observable<any>;
    @Input() viewChangeEvent: Observable<any>;
    @Input() currentView: string;

    @ViewChild('addWidgetModal') addWidgetModalTpl: TemplateRef<any>;
    @ViewChild('widgetsBar') widgetsBar: ElementRef;

    todoSettings: WidgetDefaultSettings = new WidgetDefaultSettings();
    @Input() hideOpenedWidgets: Subject<void>;
    openTodo: Subject<void> = new Subject<void>();
    widgetAddingDragStart: Subject<WidgetDragging> = new Subject<WidgetDragging>();
    widgetAddingDragEnd: Subject<WidgetDragging> = new Subject<WidgetDragging>();

    matDialogRef: MatDialogRef<any, any>;
    availableWidgets: Widget[];
    private subs: Subscription = new Subscription();

    constructor(
        public dialog: MatDialog,
        @Inject(DOCUMENT) public document: Document,
        private widgetsService: WidgetsDynamicService,
        private appStorage: AppStorageService,
        private detector: ChangeDetectorRef,
    ) {
        super();
    }

    ngOnInit() {
        this.availableWidgets = this.widgets;
        this.subs.add(this.addUnit.subscribe((event) => {
            this.openAddWidgetModal(event);
        }));
        this.subs.add(this.viewChangeEvent.subscribe(event => {
            this.hideOpenedWidgets.next();
        }));

        this.widgetsService.widgets
            .pipe(take(1))
            .subscribe(widgets => {
                this.todoSettings = widgets.find(x => x.key === 'w-todo').settings;
                this.todoSettings.widgetIconSettings = {
                    dialogMarginBottom: 12,
                    dialogMarginLeft: 40
                };
        });

        let todoKey: AppStorageKey<TodoSettings> = new AppStorageKey<TodoSettings>("fixed-w-todo-settings");
        this.appStorage.get(todoKey)
            .pipe(take(1))
            .subscribe((todoSettings: TodoSettings) => {
                if (todoSettings && todoSettings.openInStart) this.openTodo.next();
            });
    }

    ngOnDestroy(): void {
        this.subs.unsubscribe();
    }

    ngAfterViewInit(): void {
        setTimeout(() => {
            this.detector.detectChanges();
        }, 500)
    }

    widgetOpened() {
        return () => this.hideOpenedWidgets.next();
    }

    openAddWidgetModal(event) {
        this.matDialogRef = this.dialog.open(this.addWidgetModalTpl, {
            panelClass: 'app-add-widget-modal-overlay',
            data: {
                widgets: this.widgets
            },
        });
        $(event.srcElement).addClass('active-icon');

        this.matDialogRef.afterOpen()
            .pipe(take(1))
            .subscribe(() => {
            $('.app-widget-modal-overlay').closest('.cdk-global-overlay-wrapper').css('z-index', -1);
        });
        this.matDialogRef.beforeClose()
            .pipe(take(1))
            .subscribe(() => {
                $(event.srcElement).removeClass('active-icon');
                $('.app-widget-modal-overlay').closest('.cdk-global-overlay-wrapper').css('z-index', 1000);
            });
    }

    onDragStart($event, type: string, widget: Widget) {
        this.widgetAddingDragStart.next({ $event, type, widget });
    }

    onDragEnd($event, type: string) {
        this.widgetAddingDragEnd.next({ $event, type });
    }

    closeDialog(): void {
        if (this.matDialogRef) {
            this.matDialogRef.close();
        }
    }
}
