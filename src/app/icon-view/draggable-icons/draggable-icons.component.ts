import { Observable, Subject, Subscription } from 'rxjs';
import { WidgetDragging } from 'src/app/shared/base-structures/widget-dragging';
import { WidgetType } from 'src/app/shared/base-structures/widget-type';

import {
    AfterViewInit, ChangeDetectorRef, Component, EventEmitter, Inject, Input, OnDestroy, OnInit,
    Output
} from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';

import { Widget } from '../../shared/base-structures/widget';
import { ChromeStorageService } from '../../shared/storage/services/chrome-storage.service';
import { IconUnit } from '../models/icon-unit';
import { IconUnitPosition } from '../models/icon-unit-position';
import { IconUnitStorageable } from '../models/icon-unit-storageable';
import { IconVerticalLayer } from '../models/icon-vertical-layer';
import {base_storage_keys, widget_storage_keys} from "../../shared/model/storage-keys";
import {IconWrapper} from "../../desk/component/desk.component";
import {take} from "rxjs/internal/operators";
import {FireStoreService} from "../../firebase/fire-store.service";
import {AuthService} from "../../firebase/auth.service";
import {AppStorageKey} from "../../shared/storage/models/app-storage-key";
import {AppStorageService} from "../../shared/storage/services/app-storage.service";
import {TabsService} from "../../shared/storage/services/tabs.service";
import {SharedService} from "../../shared/utils/shared.service";

@Component({
    selector: 'app-draggable-icons',
    templateUrl: './draggable-icons.component.html',
    styleUrls: ['./draggable-icons.component.scss']
})
export class DraggableIconsComponent implements OnInit, OnDestroy, AfterViewInit {
    @Input() position: 'left' | 'bottom' = 'left';
    @Input() widgets: Widget[];
    @Input() hide: Observable<any>;
    @Input() widgetAddingDragStart: Observable<WidgetDragging>;
    @Input() widgetAddingDragEnd: Observable<WidgetDragging>;

    @Output() widgetAddingCompleted = new EventEmitter<void>();

    canDrag = false;
    layers: IconVerticalLayer[] = [];
    draggingWidget: Widget;
    draggingLayer: IconVerticalLayer;
    lastEnteredArea: {
        layer: IconVerticalLayer,
        layerIdx: number,
    };
    @Input() hideOpenedWidgets: Subject<void>;

    private sub = new Subscription();
    storageKey: string;
    private syncIconView: boolean = false;
    private units: IconUnit[];
    private readonly canBeBottomWidgetKeys = ['w-time'];

    constructor(
        @Inject(DOCUMENT) public document: Document,
        private storage: ChromeStorageService,
        private appStorage: AppStorageService,
        private fireStore: FireStoreService,
        private detector: ChangeDetectorRef,
        private authSvc: AuthService,
        private tabsSvc: TabsService,
        private sharedSvc: SharedService) { }

    ngOnInit(): void {
        this.storageKey = base_storage_keys.iconView_key+this.position;

        this.getViewWidgets();

        this.sub.add(this.widgetAddingDragStart.subscribe(
            (dragData) => this.onDragStart(dragData.$event, dragData.type, dragData.widget)));
        this.sub.add(this.widgetAddingDragEnd.subscribe(
            (dragData) => this.onDragEnd(dragData.$event, dragData.type)));

        this.sub.add(this.fireStore.syncWithFire
            .subscribe((reset) => {
                if (reset.type == "marker") {
                    this.syncIconView = false;
                } else if (reset.type == "data" && (!reset.keys || reset.keys.includes(this.storageKey))) {
                    this.getViewWidgets();
                }
            }));

        this.sub.add(this.tabsSvc.addTabObserver(this.storageKey)
                .subscribe(() => {
                    this.getViewWidgets();
                }));
        this.sub.add(this.sharedSvc.listenViewChanges()
                .subscribe(key => {
                    if (key == this.storageKey) this.getViewWidgets();
                }));
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }

    ngAfterViewInit(): void {
        setTimeout(() => {
            this.detector.detectChanges();
        }, 500)
    }

    widgetOpened() {
        return () => this.hideOpenedWidgets.next();
    }

    onDragStart($event, type: string, widget: Widget, layer?: IconVerticalLayer) {
        if (!type) {
            type = $event.type;
            widget = {
                key: $event.widget.key,
                title: $event.widget.title,
                component: $event.widget.component,
                settings: $event.widget.settings,
                userData: $event.widget.userData
            }
        }
        if (!this.canAddWidget(widget.key)) {
            return;
        }
        if (type === 'available') {
            const modals = this.document.getElementsByClassName('cdk-overlay-container');
            Array.from(modals).forEach((el: any) => {
                el.style.zIndex = -1;
                el.style.opacity = 0;
            });
        }

        this.draggingWidget = widget;
        if (layer) {
            this.draggingLayer = layer;
            layer.isDragging = true;
        }
        setTimeout(() => {
            const el = this.document.querySelector('.drag-active') as HTMLElement;
            if (el) {
                el.style.display = 'none';
            }
        }, 20);
    }

    onDragEnd($event, type: string, layer?: IconVerticalLayer) {
        if (!type) {
            type = $event.type;
        }
        const el = this.document.querySelector('.drag-active') as HTMLElement;
        if (el) {
            el.style.display = 'block';
        }
        if (type === 'available') {
            this.widgetAddingCompleted.emit();
            const modals = this.document.getElementsByClassName('cdk-overlay-container');
            setTimeout(() => {
                Array.from(modals).forEach((_el: any) => {
                    _el.style.zIndex = null;
                    _el.style.opacity = null;
                });
            }, 500);
        }

        if (!this.lastEnteredArea || !this.draggingWidget) {
            this.draggingWidget = null;
            return false;
        }

        if (type === 'user') {
            const idx = this.layers.indexOf(layer);
            this.layers.splice(idx, 1);
            for (let i = idx; i < this.layers.length; i++) {
                this.layers[i].unit.position.layerIndex--;
                if (this.lastEnteredArea.layer === this.layers[i]) {
                    this.lastEnteredArea.layerIdx--;
                }
            }
        }

        const newUnit = new IconUnit(
            this.draggingWidget,
            {
                layerIndex: this.lastEnteredArea.layerIdx
            },
            this.draggingWidget.userData
        );
        const units = this.layers.reduce((_units: IconUnit[], _layer: IconVerticalLayer, layerIdx: number) => {
            if (_layer.isSpacer) {
                return _units;
            }
            if (layerIdx >= this.lastEnteredArea.layerIdx) {
                _layer.unit.position.layerIndex++;
            }
            _units.push(_layer.unit);
            return _units;
        }, [newUnit]);

        if (layer) {
            layer.isDragging = false;
        }
        this.draggingWidget = null;
        this.lastEnteredArea = null;

        this.saveUnits(units);
        this.setWidgetsOverflowScroll();
        this.canDrag = false;
    }

    onDragEnter($event, layer: IconVerticalLayer, layerIdx: number) {
        if (!this.canAddWidget($event.dropData.widget.key)) {
            return;
        }
        if (this.lastEnteredArea) {
            this.lastEnteredArea.layer.hasGhost = false;
        }
        this.lastEnteredArea = {
            layer,
            layerIdx,
        };
        layer.hasGhost = true;
    }

    removeLayer(layerIdx: number) {
        let removalKeys = [];
        const units = this.layers.reduce((_units: IconUnit[], _layer: IconVerticalLayer, _layerIdx: number) => {
            if (_layer.isSpacer) {
                return _units;
            }
            if (layerIdx == _layerIdx) {
                let widgetKey = _layer.unit.widget.userData? _layer.unit.widget.userData: _layer.unit.userData;
                removalKeys.push(widgetKey);
            }
            if (_layerIdx < layerIdx) {
                _units.push(_layer.unit);
            }
            if (_layerIdx > layerIdx) {
                _layer.unit.position.layerIndex--;
                _units.push(_layer.unit);
            }
            return _units;
        }, []);

        this.saveUnits(units)
            .then(() => {
                removalKeys.forEach(key => {
                    this.removeFromStorage(key);
                });
            });
    }

    onDragIconOver($event) {
        if (!this.draggingWidget) {
            this.canDrag = true;
        }
        this.setWidgetsOverflowHidden();
    }

    onDragIconOut($event) {
        if (!this.draggingWidget) {
            this.canDrag = false;
        }
        this.setWidgetsOverflowScroll();
    }

    public onUserDataChanged(iconUnit: IconUnit, userData: any): void {
        const unit = this.units.find(x => x === iconUnit);
        if (unit) {
            unit.userData = userData;
            if (unit.widget) unit.widget.userData = userData;
        }
        this.saveUnits(this.units);
    }

    setWidgetsOverflowHidden() {
        (<HTMLElement>this.document.getElementsByClassName('left-widgets').item(0)).style.overflowY = 'hidden';
    }

    setWidgetsOverflowScroll() {
        // TODO do something with scroll
        // (<HTMLElement>this.document.getElementsByClassName('left-widgets').item(0)).style.overflowY = 'scroll';
        (<HTMLElement>this.document.getElementsByClassName('left-widgets').item(0)).style.overflowY = 'hidden';
    }

    getValidateDragFn() {
        return coords => this.validateDrag(coords);
    }

    validateDrag(coords) {
        return this.canDrag;
    }

    get isForBottomWidgets(): boolean {
        return this.position === 'bottom';
    }

    private getViewWidgets() {
        this.storage.get(this.storageKey)
            .pipe(take(1))
            .subscribe((data: IconWrapper) => {
                if (data && data.iconWidgets) {
                    this.units = this.fromStorageToModel(data.iconWidgets, this.widgets);
                } else {
                    this.units = this.fromStorageToModel([], this.widgets);
                }
                this.initLayers();
            });
    }

    private removeFromStorage(widgetKey: string): Promise<any> {
        return new Promise<any>(resolve => {
            //remove todo items with todo settings
            let storageKey = new AppStorageKey<any>(`${widgetKey}-settings`);
            let todos = widget_storage_keys.todos + widgetKey;
            if (storageKey.keyName.startsWith('w-todo')) {
                this.storage.remove(todos);
            }

            this.appStorage.remove<any>(storageKey);
            if (this.authSvc.currentUser) {
                this.storage.get(base_storage_keys.sync_keys)
                    .pipe(take(1))
                    .subscribe((k: any) => {
                        let keys = k;
                        if (!keys) keys = {};
                        keys[storageKey.keyName] = 'remove';
                        if (storageKey.keyName.startsWith('w-todo')) {
                            keys[todos] = 'remove';
                        }
                        //this.storage.set(base_storage_keys.sync_keys, keys);
                        this.storage.remove(base_storage_keys.sync_keys)
                            .pipe(take(1))
                            .subscribe(() => {
                                this.storage.set(base_storage_keys.sync_keys, keys);
                                resolve();
                            }, error => {
                                resolve()
                            });
                    }, error => {
                        resolve()
                    });
            } else resolve();
        });
    }

    private canAddWidget(addingWidgetKey: string): boolean {
        return !this.isForBottomWidgets || (this.canBeBottomWidgetKeys.includes(addingWidgetKey) && this.layers.length < 2);
    }

    private saveUnits(units: IconUnit[]): Promise<any> {
        return new Promise<any>(resolve => {
            if (!this.syncIconView && this.authSvc.currentUser) {
                this.syncIconView = true;
                this.sharedSvc.syncKey(this.storageKey)
                    .then(() => resolve());
            } else resolve();
            this.storage.set(this.storageKey, {iconWidgets: JSON.parse(JSON.stringify(this.fromModelToStorage(units)))})
                .pipe(take(1))
                .subscribe(() => {
                    this.units = units;
                    this.initLayers();
                });
        });
    }

    private fromStorageToModel(data: IconUnitStorageable[], widgets: Widget[]): IconUnit[] {
        return data.map(record => {
            let widget = widgets.find(w => w.key === record.key);
            if (widget) {
                widget = {
                    key: widget.key,
                    title: widget.title,
                    component: widget.component,
                    settings: widget.settings,
                    userData: record.userData
                }
                const position = <IconUnitPosition>{
                    layerIndex: record.layerIndex
                };
                return new IconUnit(widget, position, record.userData);
            } else return null;
        }).filter(u => {
            return u != null;
        });
    }

    private fromModelToStorage(units: IconUnit[]): IconUnitStorageable[] {
        return units.map(unit => (<IconUnitStorageable>{
            key: unit.widget.key,
            layerIndex: unit.position.layerIndex,
            userData: unit.userData? unit.userData: unit.widget.userData
        }));
    }

    private initLayers() {
        // this._userUnitKeys = [];
        this.layers = this.units.sort((unit1, unit2) => {
            return unit1.position.layerIndex < unit2.position.layerIndex ? -1 : 1;
        }).map(unit => {
            // this._userUnitKeys.push(unit.widget.key);
            return <IconVerticalLayer>{
                unit,
            };
        });
        this.layers.push(<IconVerticalLayer>{
            unit: {
                widget: new Widget('shadow', null, null, { types: [WidgetType.icon] }),
                position: {
                    layerIndex: this.layers.length
                }
            },
            isSpacer: true,
        });
    }
}
