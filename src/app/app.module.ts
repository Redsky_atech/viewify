import { HotkeyModule } from 'angular2-hotkeys';
import { HttpClientModule } from '@angular/common/http';
import {APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA, ErrorHandler, NgModule} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import {DeskModule} from './desk/desk.module';
import {StorageModule} from './shared/storage/storage.module';

import {UtilsModule} from './shared/utils/utils.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DragulaModule} from "ng2-dragula";
import {SortablejsModule} from "angular-sortablejs";
import {FirebaseModule} from "./firebase/firebase.module";
import {AuthService} from "./firebase/auth.service";
import {AuthModule} from "./auth/auth.module";
import {FireStoreService} from "./firebase/fire-store.service";
import {ImageStorageService} from "./shared/storage/services/image-storage.service";
import {DeskService} from "./desk/desk.service";
import {MatSnackBarModule} from "@angular/material";
import {GlobalErrorHandler} from "./shared/log/GlobalErrorHandler";
import {LogService} from "./shared/log/log.service";
import {TabsService} from "./shared/storage/services/tabs.service";
import {LocationService} from "./shared/geo/location.service";
import {SharedService} from "./shared/utils/shared.service";

export function authenticateUser(provider: AuthService) {
    return () => provider.authOnStartpup();
}

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        MatSnackBarModule,

        StorageModule,
        DeskModule,
        AuthModule,
        UtilsModule,

        FirebaseModule,
        HotkeyModule.forRoot(),
        DragulaModule.forRoot(),
        SortablejsModule.forRoot({ animation: 50 })
    ],
    providers: [
        AuthService,
        DeskService,
        TabsService,
        LogService,
        SharedService,
        FireStoreService,
        ImageStorageService,
        LocationService,
        { provide: APP_INITIALIZER, useFactory: authenticateUser, deps: [AuthService], multi: true },
        { provide: ErrorHandler, useClass: GlobalErrorHandler}
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    bootstrap: [AppComponent]
})
export class AppModule { }
