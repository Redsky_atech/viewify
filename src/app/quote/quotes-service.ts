import { HttpClient } from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Quote} from './quote';


@Injectable()
export class QuoteService {
    private quotesSourceUrl = 'https://api.forismatic.com/api/1.0/?method=getQuote&format=json&lang=en';

    constructor(
        private http: HttpClient
    ) { }


    public getQuote(): Promise<string>{
        return this.http.get<string>(this.quotesSourceUrl, {responseType: 'text' as 'json'})
        .toPromise();
    }

}
