export class Quote {
    public quoteAuthor: string;
    public quoteText: string;
    public quoteLink: string;
}
