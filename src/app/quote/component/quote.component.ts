import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {QuoteService} from '../quotes-service';
import {Quote} from '../quote';

@Component({
    selector: 'app-quote',
    styleUrls: ['quote.component.scss'],
    encapsulation: ViewEncapsulation.None,
    templateUrl: './quote.component.html',
})
export class QuoteComponent implements OnInit {

    quote = new Quote();

    constructor(private quoteService: QuoteService) { }

    ngOnInit(): void {
                this.loadQuote();
    }

    private loadQuote() {
        this.quoteService.getQuote().then((quoteStr: any) => {
            if (quoteStr) {
                let quoteObj = this.formCorrectJSON(quoteStr);
                if (quoteObj.quoteText) {
                    if(quoteObj.quoteText.length && quoteObj.quoteText.length>160){
                        this.loadQuote();
                        return;
                    }
                    this.quote = quoteObj;
                    this.quote.quoteText = `"${this.quote.quoteText}"`;
                }
            }
        });
    }

    private formCorrectJSON(quoteStr: string){
         let quoteObj = new Quote();
         if(quoteStr && quoteStr.indexOf("\'")>-1){
                let woQuoteStr= quoteStr.replace(/\\/g,"");
                quoteObj = JSON.parse(woQuoteStr);
            }else{
                quoteObj = JSON.parse(quoteStr);
            }
         return quoteObj;
    }
}
