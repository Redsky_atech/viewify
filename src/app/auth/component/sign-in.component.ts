import {Component, EventEmitter, OnDestroy, Output, TemplateRef, ViewChild, ViewEncapsulation} from '@angular/core';
import {AuthService} from '../../firebase/auth.service';
import {UserModel} from "../model/user.model";
import {Subscription} from "rxjs/Rx";
import {base_storage_keys} from "../../shared/model/storage-keys";
import {ChromeStorageService} from "../../shared/storage/services/chrome-storage.service";
import {TabsService} from "../../shared/storage/services/tabs.service";
import {take} from "rxjs/internal/operators";
import {HttpClient} from "@angular/common/http";
import {SharedService} from "../../shared/utils/shared.service";
import {MatDialog, MatDialogRef} from "@angular/material";

const messages = {
    successReg: "New user Created!",
    failReg: "Registration failed!",
    failLogin: "Failed to login!",
    verifyMessage: "Verification email was sent!"
}

@Component({
    selector: 'app-sign-in',
    templateUrl: './sign-in.component.html',
    styleUrls: ['./sign-in.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class SignInComponent implements OnDestroy {
    welcomeMessage: string;
    userName: string = "New User";
    repeatPass: any;
    registrationForm = false;
    showSpinner: boolean = false;
    anonymouseStorage: boolean = false;
    @Output() authenticated: EventEmitter<boolean> = new EventEmitter<boolean>();
    @ViewChild("successRegister") successRegister: TemplateRef<any>;
    @ViewChild("askName") askName: TemplateRef<any>;
    templRef: MatDialogRef<any, any>;
    newUser: UserModel = new UserModel();
    private subs: Subscription = new Subscription();

    constructor(public auth: AuthService,
                private tabsSvc: TabsService,
                private sharedSvc: SharedService,
                private storage: ChromeStorageService,
                private http: HttpClient,
                private dialog: MatDialog) {
        this.subs.add(auth.authenticatedSubject.subscribe((result: boolean) => {
            this.authenticated.emit(result);
        }));

        this.subs.add(this.tabsSvc.addTabObserver(base_storage_keys.isAnonymous)
            .subscribe(() => {
                this.storage.get(base_storage_keys.isAnonymous)
                    .pipe(take(1))
                    .subscribe((a: boolean) => {
                        if (a) this.authenticated.emit(true);
                    });
            }));
    }

    ngOnDestroy(): void {
        this.subs.unsubscribe();
    }

    signInAnonymously(): void {
        this.auth.signInAnonymously()
            .then((authenticated) => {
            });
    }

    signInWithFacebook(): void {
        this.auth.signInWithFacebook()
            .then((authenticated) => {
            });
    }

    signInWithGithub(): void {
        this.auth.signInWithGithub()
            .then((authenticated) => {
            });
    }

    signInWithGoogle(): void {
        this.auth.signInWithGoogle()
            .then((authenticated) => {
            });
    }

    signInWithTwitter(): void {
        this.auth.signInWithTwitter()
            .then((authenticated) => {
            });
    }

    goToCreatForm() {
        this.auth.registrationForm = true;
        this.newUser.pass = "";
        this.newUser.email = "";
        this.repeatPass = "";
        this.welcomeMessage = "";
        this.auth.submitName = "Sign Up";
    }

    goToLoginForm() {
        this.auth.registrationForm = false;
        this.newUser.pass = "";
        this.newUser.email = "";
        this.welcomeMessage = "";
        this.auth.submitName = "Login";
    }

    signOut() {
        this.goToLoginForm();
        this.auth.signOut();
    }

    creatOrLogin() {
        if (this.newUser.email.length < 4 || this.newUser.pass.length < 5) {
            this.welcomeMessage = messages.failLogin;
        } else if (this.auth.registrationForm) {
            if (this.repeatPass == this.newUser.pass) {
                this.auth.createNewUser(this.newUser)
                    .then(result => {
                        if (result === true) {
                            this.storage.neutralSet(base_storage_keys.register_time, new Date().toUTCString());
                            this.syncKey(base_storage_keys.register_time);

                            this.storage.neutralGet(base_storage_keys.all_used_keys)
                                .pipe(take(1))
                                .subscribe(keys => {
                                    if (keys && keys.length > 2) {
                                        this.templRef = this.dialog.open(this.successRegister, {
                                            panelClass: "registration-modal-overlay",
                                            backdropClass: "registration-modal-backdrop",
                                            disableClose: true
                                        });
                                        this.templRef.afterClosed()
                                            .pipe(take(1))
                                            .subscribe(() => {
                                                this.askForName();
                                            });
                                    } else {
                                        this.askForName();
                                    }
                                });
                        } else if (result) {
                            this.welcomeMessage = result;
                        } else {
                            this.welcomeMessage = messages.failReg;
                        }
                    });
            }
        } else {
            this.auth.signInWithEmailPassword(this.newUser)
                .then(result => {
                    if (result === true) {

                    } else if (result) {
                        this.welcomeMessage = result;
                    } else {
                        this.welcomeMessage = messages.failLogin;
                    }
                }).catch(error => {
                this.welcomeMessage = messages.failLogin;
            });
        }
    }

    loginAnonymously() {
        this.storage.set(base_storage_keys.isAnonymous, true);
        this.authenticated.emit(true);
        this.tabsSvc.addChangedTabKey(base_storage_keys.isAnonymous);
    }

    useAnonymouseStorage() {
        if (this.anonymouseStorage) {
            if (this.templRef) {
                this.templRef.close();
                this.templRef = null;
            }
        } else {
            this.showSpinner = true;
            this.storage.neutralGet(base_storage_keys.all_used_keys)
                .pipe(take(1))
                .subscribe(keys => {
                    if (keys) {
                        //transfer data from anonymous to registered account
                        let syncKeys = [];
                        this.storage.neutralSet(base_storage_keys.first_local_upload, '1.0.0');
                        this.storage.neutralSet(base_storage_keys.first_fire_upload, '1.0.0');
                        syncKeys.push(base_storage_keys.first_fire_upload);
                        keys.forEach((key, ind, arr) => {
                            syncKeys.push(key);
                            this.storage.neutralGet("l-"+key)
                                .pipe(take(1))
                                .subscribe(value => {
                                    if (value) {
                                        this.storage.neutralSet(key, value);
                                        this.storage.neutralRemove("l-"+key);
                                    }
                                    if (ind == arr.length-1) {
                                        setTimeout(() => {
                                            this.showSpinner = false;
                                            if (this.templRef) {
                                                this.templRef.close();
                                                this.templRef = null;
                                            }
                                        }, 500);
                                    }
                                });
                        });

                        this.storage.neutralGet(base_storage_keys.sync_keys)
                            .pipe(take(1))
                            .subscribe(keys => {
                                if (!keys) keys = {};
                                syncKeys.forEach(key => {
                                    keys[key] = 'save';
                                });

                                this.storage.neutralSet(base_storage_keys.sync_keys, syncKeys);
                            });
                    } else {
                        this.showSpinner = false;
                        if (this.templRef) {
                            this.templRef.close();
                            this.templRef = null;
                        }
                    }
                });
        }
    }

    endRegistration() {
        /*if (!this.userName) this.userName = "New User";
        //let profile: ProfileSettings = defaultProfile;
        profile.name = this.userName;
        this.storage.set(base_storage_keys.profile, profile);
        if (this.templRef) {
            this.templRef.close();
            this.templRef = null;
        }*/
    }

    private syncKey(key: string): Promise<any> {
        return new Promise<any>(resolve => {
            this.storage.neutralGet(base_storage_keys.sync_keys)
                .pipe(take(1))
                .subscribe((k: any) => {
                    let keys = k;
                    if (!keys) keys = {};
                    keys[key] = 'save';
                    this.storage.neutralSet(base_storage_keys.sync_keys, keys);
                    resolve();
                }, error => resolve());
        });
    }

    private postSignIn(): void {
        this.authenticated.emit(true);
    }

    private askForName() {
        if (this.templRef) {
            this.templRef.close();
        }
        this.templRef = this.dialog.open(this.askName, {
            panelClass: "registration-modal-overlay",
            backdropClass: "registration-modal-backdrop",
            disableClose: true
        });
        this.templRef.beforeClose()
            .pipe(take(1))
            .subscribe(() => {
                this.auth.registrationForm = false;
                this.auth.submitName = "Login";
                this.welcomeMessage = messages.successReg;
            });
    }
}
