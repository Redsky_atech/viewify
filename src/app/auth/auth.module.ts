import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignInComponent } from './component/sign-in.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatSlideToggleModule,
    MatTooltipModule
} from "@angular/material";

@NgModule({
  declarations: [
      SignInComponent
  ],
  exports: [SignInComponent],
  imports: [
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      MatButtonModule,
      MatFormFieldModule,
      MatInputModule,
      MatDialogModule,
      MatProgressSpinnerModule,
      MatSlideToggleModule,
      MatTooltipModule
  ]
})
export class AuthModule { }
