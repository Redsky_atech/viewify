import {Injectable} from '@angular/core';

import {from} from 'rxjs';
import {StorageService} from './storage-service';
import {Observable} from "rxjs/Rx";
import {AngularFireAuth} from "@angular/fire/auth";
import {TabsService} from "./tabs.service";
import {base_storage_keys} from "../../model/storage-keys";

@Injectable()
export class ChromeStorageService implements StorageService {
    constructor(private afAuth: AngularFireAuth,
                private tabsSvc: TabsService) {
    }

    get(k: string): Observable<any> {
        return from(new Promise(resolve => {
            let key;
            if (this.afAuth.auth.currentUser) key = k;
            else key = "l-" + k;
            chrome.storage.local.get(key, data => {
                if (data && data[key]) resolve(data[key]);
                else resolve(null);
            });
        }));
    }

    neutralGet(key: string): Observable<any> {
        return from(new Promise(resolve => {
            chrome.storage.local.get(key, data => {
                if (data && data[key]) resolve(data[key]);
                else resolve(null);
            });
        }));
    }

    set(k: string, data: any): Observable<any> {
        return from(new Promise((resolve, reject) => {
            if (!chrome || !chrome.storage) {
                reject('Chrome Storage is not defined');
            } else {
                this.tabsSvc.addChangedTabKey(k);
                let key;
                if (this.afAuth.auth.currentUser) key = k;
                else {
                    key = "l-" + k;
                    this.collectLocalKey(k, 'set');
                }
                chrome.storage.local.set({[key]: data}, () => {
                    resolve();
                });
            }
        }));
    }

    neutralSet(key: string, data: any): Observable<any> {
        return from(new Promise((resolve, reject) => {
            if (!chrome || !chrome.storage) {
                reject('Chrome Storage is not defined');
            } else {
                chrome.storage.local.set({[key]: data}, () => {
                    resolve();
                });
            }
        }));
    }

    remove(k: string): Observable<any> {
        return from(new Promise((resolve, reject) => {
            if (!chrome || !chrome.storage) {
                reject('Chrome Storage is not defined');
            } else {
                this.tabsSvc.addChangedTabKey(k);
                let key;
                if (this.afAuth.auth.currentUser) key = k;
                else {
                    key = "l-" + k;
                    this.collectLocalKey(k, 'remove');
                }
                chrome.storage.local.remove(key, () => {
                    resolve();
                });
            }
        }));
    }

    neutralRemove(key: string): Observable<any> {
        return from(new Promise((resolve, reject) => {
            if (!chrome || !chrome.storage) {
                reject('Chrome Storage is not defined');
            } else {
                chrome.storage.local.remove(key, () => {
                    resolve();
                });
            }
        }));
    }

    private collectLocalKey(newKey: string, type: 'set' | 'remove'): Promise<any> {
        return new Promise<any>(resolve => {
            let storeKey = base_storage_keys.all_used_keys;
            setTimeout(() => {
                chrome.storage.local.get(storeKey, keys => {
                    let currentKeys = [];
                    if (keys && keys[storeKey]) {
                        currentKeys = keys[storeKey];
                    }
                    if (type == 'set') {
                        if (!currentKeys.includes(newKey)) currentKeys.push(newKey);
                    } else if (type == 'remove') {
                        if (currentKeys.includes(newKey)) {
                            currentKeys = currentKeys.filter(key => key != newKey);
                        }
                    }
                    chrome.storage.local.set({[storeKey]: currentKeys}, () => {
                        resolve();
                    });
                });
            }, Math.random()*1000);
        });
    }
}
