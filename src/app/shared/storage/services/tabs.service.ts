import {Injectable, OnDestroy} from "@angular/core";
import {base_storage_keys, widget_storage_keys} from "../../model/storage-keys";
import {take} from "rxjs/internal/operators";
import {Observable, Subject} from "rxjs/Rx";
import {from} from "rxjs/index";
import {AngularFireAuth} from "@angular/fire/auth";

@Injectable()
export class TabsService implements OnDestroy {
    private intervalId;
    tabId: string;
    syncWithTabs: any = {};

    constructor(private afAuth: AngularFireAuth) {}

    ngOnDestroy(): void {
        clearInterval(this.intervalId);
    }

    addChangedTabKey(key: string) {
        if (this.syncWithTabs[key]) {
            this.get(base_storage_keys.sync_tabs_keys)
                .pipe(take(1))
                .subscribe((data: any) => {
                    if (data) {
                        Object.keys(data).forEach(tabKey => {
                           if (tabKey != this.tabId || key.startsWith(widget_storage_keys.todos) || key == base_storage_keys.calendar) {
                               if (!data[tabKey].includes(key)) data[tabKey].push(key);
                           }
                        });
                        this.set(base_storage_keys.sync_tabs_keys, data);
                    }
                });
        }
    }

    addTabObserver(key: string): Observable<any> {
        if (!this.syncWithTabs[key]) {
            this.syncWithTabs[key] = new Subject<any>();
        }
        return this.syncWithTabs[key].asObservable();
    }

    startTabSync() {
        this.tabId = this.uniqueId();

        this.get(base_storage_keys.sync_tabs_keys)
            .pipe(take(1))
            .subscribe((tabs: any) => {
                if (tabs) {
                    if (Object.keys(tabs).length > 5) {
                        tabs = {};
                    }
                    tabs[this.tabId] = [];
                } else {
                    tabs = {};
                    tabs[this.tabId] = [];
                }
                this.remove(base_storage_keys.sync_tabs_keys)
                    .pipe(take(1))
                    .subscribe(() => {
                        this.set(base_storage_keys.sync_tabs_keys, tabs);
                    });
            });

        this.intervalId = setInterval(() => {
            this.get(base_storage_keys.sync_tabs_keys)
                .pipe(take(1))
                .subscribe((tabs: any) => {
                    if (tabs && tabs[this.tabId] && tabs[this.tabId].length > 0) {
                        tabs[this.tabId].forEach(k => {
                            if (this.syncWithTabs[k]) this.syncWithTabs[k].next();
                        });
                        tabs[this.tabId] = [];
                        this.set(base_storage_keys.sync_tabs_keys, tabs);
                    } else if (tabs && !tabs[this.tabId]) {
                        this.tabId = this.uniqueId();
                        tabs[this.tabId] = [];

                        this.remove(base_storage_keys.sync_tabs_keys)
                            .pipe(take(1))
                            .subscribe(() => {
                                this.set(base_storage_keys.sync_tabs_keys, tabs);
                            });
                    }
                });
        }, 10000);
    }

    endTabSync(isLogged: string): Observable<any> {
        this.tabId = null;
        clearInterval(this.intervalId);
        return this.remove(base_storage_keys.sync_tabs_keys, isLogged);
    }

    private uniqueId() {
        return Math.random().toString(36).substr(2, 5);
    }

    private get(k: string): Observable<any> {
        return from(new Promise(resolve => {
            let key;
            if (this.afAuth.auth.currentUser) key = k;
            else key = "l-" + k;
            chrome.storage.local.get(key, data => {
                if (data && data[key]) resolve(data[key]);
                else resolve(null);
            });
        }));
    }

    private set(k: string, data: any): Observable<any> {
        return from(new Promise((resolve, reject) => {
            if (!chrome || !chrome.storage) {
                reject('Chrome Storage is not defined');
            } else {
                let key;
                if (this.afAuth.auth.currentUser) key = k;
                else key = "l-" + k;
                chrome.storage.local.set({[key]: data}, () => {
                    resolve();
                });
            }
        }));
    }

    private remove(k: string, isLogged?: string): Observable<any> {
        return from(new Promise((resolve, reject) => {
            if (!chrome || !chrome.storage) {
                reject('Chrome Storage is not defined');
            } else {
                let key;
                if (isLogged && isLogged == 'logged') {
                    key = k;
                } else if (isLogged && isLogged == 'out') {
                    key = "l-" + k;
                } else if (this.afAuth.auth.currentUser) {
                    key = k;
                } else key = "l-" + k;
                chrome.storage.local.remove(key, () => {
                    resolve();
                });
            }
        }));
    }
}
