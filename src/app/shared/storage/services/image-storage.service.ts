import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Rx";
import {from} from "rxjs/index";

@Injectable()
export class ImageStorageService {
    constructor() {}

    get(key: string): Observable<any> {
        return from(new Promise(resolve => {
            chrome.storage.local.get(key, data => {
                if (data && data[key]) resolve(data[key]);
                else resolve(null);
            });
        }));
    }

    set(key: string, data: any): Observable<any> {
        return from(new Promise((resolve, reject) => {
            if (!chrome || !chrome.storage) {
                reject('Chrome Storage is not defined');
            } else {
                chrome.storage.local.set({ [key]: data}, () => {
                    resolve();
                });
            }
        }));
    }

    remove(key: string): Observable<any> {
        return from(new Promise((resolve, reject) => {
            if (!chrome || !chrome.storage) {
                reject('Chrome Storage is not defined');
            } else {
                chrome.storage.local.remove(key, () => {
                    resolve();
                });
            }
        }));
    }
}
