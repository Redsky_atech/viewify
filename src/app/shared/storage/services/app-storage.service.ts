import {Injectable} from '@angular/core';

import {AppStorageKey} from '../models/app-storage-key';
import {Observable} from "rxjs/Rx";
import {from, of} from "rxjs";
import {AngularFireAuth} from "angularfire2/auth";
import {base_storage_keys} from "../../model/storage-keys";

@Injectable()
export class AppStorageService {
    constructor(public afAuth: AngularFireAuth) {}

    public set<T>(key: AppStorageKey<T>, value: T): Observable<any> {
        if (key) {
            const obj = {};
            if (this.afAuth.auth.currentUser) obj[key.keyName] = value;
            else {
                obj["l-"+key.keyName] = value;
                this.collectLocalKey(key.keyName, 'set');
            }
            return from(new Promise(resolve => {
                chrome.storage.local.set(obj, () => {
                    resolve();
                });
            }));
        } else return of(null);
    }

    public get<T>(k: AppStorageKey<T>): Observable<any> {
        return from(new Promise(resolve => {
            if (k) {
                let key;
                if (this.afAuth.auth.currentUser) key = k.keyName;
                else key = "l-"+k.keyName;
                chrome.storage.local.get(key, data => {
                    if (data && data[key]) resolve(<T>data[key]);
                    else resolve(null);
                });
            } else resolve(null);
        }));
    }

    public remove<T>(k: AppStorageKey<T>): Observable<any> {
        return from(new Promise((resolve, reject) => {
            if (k) {
                if (!chrome || !chrome.storage) {
                    reject('Chrome Storage is not defined');
                } else {
                    let key;
                    if (this.afAuth.auth.currentUser) key = k.keyName;
                    else {
                        key = "l-"+k.keyName;
                        this.collectLocalKey(k.keyName, 'remove');
                    }
                    chrome.storage.local.remove(key, () => {
                        resolve(null);
                    });
                }
            } else resolve(null);
        }));
    }

    private collectLocalKey(newKey: string, type: 'set' | 'remove'): Promise<any> {
        return new Promise<any>(resolve => {
            let storeKey = base_storage_keys.all_used_keys;
            setTimeout(() => {
                chrome.storage.local.get(storeKey, keys => {
                    let currentKeys = [];
                    if (keys && keys[storeKey]) {
                        currentKeys = keys[storeKey];
                    }

                    if (type == 'set') {
                        if (!currentKeys.includes(newKey)) currentKeys.push(newKey);
                    } else if (type == 'remove') {
                        if (currentKeys.includes(newKey)) {
                            currentKeys = currentKeys.filter(key => key != newKey);
                        }
                    }
                    chrome.storage.local.set({[storeKey]: currentKeys}, () => {
                        resolve();
                    });
                });
            }, Math.random()*1000);
        });
    }
}
