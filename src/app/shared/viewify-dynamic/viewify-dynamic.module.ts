import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DynamicAttributesDirective, DynamicComponent, DynamicDirective, DynamicModule} from 'ng-dynamic-component';
import {WidgetTodoModule} from '../../widgets/widget-todo/widget-todo.module';
import {WidgetTodoComponent} from '../../widgets/widget-todo/component/widget-todo.component';
import {WidgetTimeComponent} from '../../widgets/widget-time/component/widget-time.component';
import {WidgetWeatherComponent} from '../../widgets/widget-weather/component/widget-weather.component';
import {WidgetIframeComponent} from '../../widgets/widget-iframe/component/widget-iframe.component';
import {WidgetTimeModule} from '../../widgets/widget-time/widget-time.module';
import {WidgetWeatherModule} from '../../widgets/widget-weather/widget-weather.module';
import {WidgetIframeModule} from '../../widgets/widget-iframe/widget-iframe.module';
import {WidgetCalculatorModule} from "../../widgets/widget-calculator/widget-calculator.module";
import {WidgetCalculatorComponent} from "../../widgets/widget-calculator/component/widget-calculator.component";
import {WidgetQuoteModule} from "../../widgets/widget-quotes/widget-quotes.module";
import {WidgetQuoteComponent} from "../../widgets/widget-quotes/component/widget-quotes.component";
import {WidgetCalendarModule} from "../../widgets/widget-calendar/widget-calendar.module";
import {WidgetCalendarComponent} from "../../widgets/widget-calendar/component/widget-calendar.component";


@NgModule({
    imports: [
        CommonModule,

        WidgetTodoModule,
        WidgetTimeModule,
        WidgetWeatherModule,
        WidgetIframeModule,
        WidgetCalculatorModule,
        WidgetQuoteModule,
        WidgetCalendarModule,

        DynamicModule.withComponents([
            WidgetTodoComponent,
            WidgetTimeComponent,
            WidgetWeatherComponent,
            WidgetIframeComponent,
            WidgetCalculatorComponent,
            WidgetQuoteComponent,
            WidgetCalendarComponent,
        ]),
    ],
    declarations: [],
    exports: [
        DynamicComponent,
        DynamicDirective,
        DynamicAttributesDirective,
        DynamicModule,
    ]
})
export class ViewifyDynamicModule {
}
