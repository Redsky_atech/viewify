import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatDatepickerInputEvent } from '@angular/material';

@Component({
    selector: 'app-date-picker',
    template: `        
        <div class="todo_date-picker">
            <input matInput [matDatepicker]="picker" [value]="date" (dateChange)="onDateChanged($event)">
            <mat-datepicker #picker></mat-datepicker>
            <i class="far fa-calendar-alt" (click)="picker.open(); onOpenDate()"></i>
        </div>

    `,
    styles: [`        
        .todo_date-picker {
            width: 0;
            margin: 0;
        }
    `]
})
export class DatePickerComponent {
    //cdk-global-overlay-wrapper
    @Input() date: Date;
    @Output() dateChange = new EventEmitter<Date>();
    @Output() openCalendar = new EventEmitter<void>();

    public onDateChanged(event: MatDatepickerInputEvent<Date>): void {
        $('.cdk-global-overlay-wrapper').css('z-index', '1000');
        this.dateChange.emit(event.target.value);
    }

    public onOpenDate() {
        $('.cdk-global-overlay-wrapper').css('z-index', '-1');
    }
}
