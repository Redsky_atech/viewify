export class LocationModel {
    postalCode: string;
    countryCode: string;
    city: string;
    latitude: number;
    longitude: number;
    time?: number; //getDate()
    ip?: string;
    woeid?: number;
    type?: string;
}
