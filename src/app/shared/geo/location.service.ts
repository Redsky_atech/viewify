import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";

//import {codes} from "../../../../zipcodes/lib/codes.js";
//import * as ca from "../../../../zipcodes/lib/codesCanada.js"
//import {OAuth} from "oauth";
import * as OAuth from 'oauth-1.0a';
import {Observable} from "rxjs/Rx";
import {ChromeStorageService} from "../storage/services/chrome-storage.service";
import {base_storage_keys} from "../model/storage-keys";
import {take} from "rxjs/internal/operators";
import {LocationModel} from "./location.model";

@Injectable()
export class LocationService {
    readonly usaCodes = ['us', 'usa', 'united states'];
    readonly canadaCodes = ['ca', 'canada', ];
    readonly SanFrancisco: LocationModel = {
        city: "San Francisco",
        countryCode: "US",
        latitude: 33.77,
        longitude: -122.41,
        postalCode: "94101"
    }
    //readonly writeGeo = 'http://localhost:7777/testGeo/location/write_geo.php';
    readonly writeGeo = 'https://api.dashify.me/location/write_geo.php';
    //readonly findGeo = 'http://localhost:7777/testGeo/location/find_location.php';
    readonly findGeo = 'https://api.dashify.me/location/find_location.php?';
    readonly getIP = "https://api.ipify.org?format=json";

    readonly geonamesExtended = "http://api.geonames.org/extendedFindNearby?lat=47.3&lng=9&username=demo";
    readonly geonamesPostal = "http://api.geonames.org/findNearbyPostalCodesJSON?lat=";
    //readonly zipQuery = "http://api.ipstack.com/check?access_key=3589f68db60f0f3e4898489508833085&format=1";
    location: LocationModel;

    constructor(private http: HttpClient, private storage: ChromeStorageService) {}

    /** Fill db with location data */
    /*public sendUSAGeo() {
        let options = {
            headers: {'Content-Type': 'application/json'}
        }
        let geo = [];
        let keys = Object.keys(codes);
        let counter = 0;
        keys.forEach(k => {
            counter++;
            geo.push(codes[k]);
            if (counter > 1000) {
                console.log(geo);
                this.http.post(this.writeGeo, {usa_geo: geo}, options)
                    .subscribe(r => {
                        console.log(r);
                    });
                counter = 0;
                geo = [];
            }
        });
        if (geo.length > 0) {
            console.log(geo);
            this.http.post(this.writeGeo, {usa_geo: geo}, options)
                .subscribe(r => {
                    console.log(r);
                });
        }
    }*/

    /** Fill db with location data */
    /*public sendCAGeo() {
        let options = {
            headers: {'Content-Type': 'application/json'}
        }
        let geo = [];
        let keys = Object.keys(ca.codes);
        let counter = 0;
        keys.forEach(k => {
            counter++;
            ca.codes[k].zip = ca.codes[k].zip? ca.codes[k].zip: "";
            ca.codes[k].city = ca.codes[k].city? ca.codes[k].city: "";
            ca.codes[k].country = ca.codes[k].country? ca.codes[k].country: "";
            ca.codes[k].latitude = ca.codes[k].latitude? ca.codes[k].latitude: 0;
            ca.codes[k].longitude = ca.codes[k].longitude? ca.codes[k].longitude: 0;
            ca.codes[k].state = ca.codes[k].state? ca.codes[k].state: "";
            geo.push(ca.codes[k]);
            if (counter > 1000) {
                console.log(geo);
                this.http.post(this.writeGeo, {ca_geo: geo}, options)
                    .subscribe(r => {
                        console.log(r);
                    });
                counter = 0;
                geo = [];
            }
        });

        if (geo.length > 0) {
            console.log(geo);
            this.http.post(this.writeGeo, {ca_geo: geo}, options)
                .subscribe(r => {
                    console.log(r);
                });
        }
    }*/

    findByCityName(city: string, country: string): Observable<any> {
        return this.http.get(this.findGeo+"country="+country+"&city="+city);
    }

    findByZip(zip: string, country: string): Observable<any> {
        return this.http.get(this.findGeo+"country="+country+"&zip="+zip);
    }

    findByGeo(latitude: number, longitude: number, country: string): Observable<any> {
        return this.http.get(this.findGeo+"country="+country+"&lat="+latitude+"&lon="+longitude);
    }

    updateGeoAfterLatLonChanged(): Promise<any> {
        return new Promise<any>(resolve => {
            this.storage.get(base_storage_keys.my_geo)
                .pipe(take(1))
                .subscribe((loc: LocationModel) => {
                    if (!loc) {
                        this.location = new LocationModel();
                        this.requestLocation().then(() => resolve());
                        /*this.http.get(this.getIP)
                            .pipe(take(1))
                            .subscribe((ip: any) => {
                                if (ip) this.location.ip = ip.ip;
                                this.requestLocation().then(() => resolve());
                            }, error => resolve());*/
                    } else {
                        this.location = loc;
                        this.requestLocation().then(() => resolve());
                        /*this.http.get(this.getIP)
                            .pipe(take(1))
                            .subscribe((ip: any) => {
                                if (ip && this.location.ip != ip.ip) {
                                    this.location.ip = ip.ip;
                                    this.requestLocation().then(() => resolve());
                                } else resolve();
                            }, error => {
                                console.log(error);
                                resolve()
                            });*/
                    }
                });
        });
    }

    public requestLocation(): Promise<any> {
        return new Promise<any>(resolve => {
            //enableHighAccuracy
            navigator.geolocation.getCurrentPosition((position: any) => {
                if (position && position.coords) {
                    if (!this.location.latitude || !this.location.longitude) {
                        this.location.latitude = position.coords.latitude;
                        this.location.longitude = position.coords.longitude;
                        this.getGeonamesLocation(position)
                            .then(() => resolve());
                    } else {
                        if (this.isLatitudeLongitudeChanged(position.coords.latitude, position.coords.longitude)) {
                            this.getGeonamesLocation(position)
                                .then(() => resolve());
                        } else resolve();
                    }
                } else {
                    this.location = this.SanFrancisco;
                    resolve();
                }
            }, (error: any) => {
                console.log(error);
                resolve();
            });
        });
    }

    public checkIfUS(): boolean {
        return this.usaCodes.includes(this.location.countryCode.toLowerCase());
    }

    public checkIfCA(): boolean {
        return this.canadaCodes.includes(this.location.countryCode.toLowerCase());
    }

    private getGeonamesLocation(position: any): Promise<any> {
        return new Promise<any>(resolve => {
            if (position) {
                this.http.get(this.geonamesPostal+this.location.latitude+"&lng="+this.location.longitude+"&username=bezr")
                    .pipe(take(1))
                    .subscribe((geo: any) => {
                        if (geo && geo.postalCodes && geo.postalCodes[0]) {
                            this.location.latitude = position.coords.latitude;
                            this.location.longitude = position.coords.longitude;
                            if (geo.postalCodes[0].placeName) this.location.city = geo.postalCodes[0].placeName;
                            else if (geo.postalCodes[0].adminName1) this.location.city = geo.postalCodes[0].adminName1;
                            else if (geo.postalCodes[0].adminName2) this.location.city = geo.postalCodes[0].adminName2;
                            this.location.countryCode = geo.postalCodes[0].countryCode;
                            this.location.postalCode = geo.postalCodes[0].postalCode;
                            this.location.time = new Date().getDate();
                            this.location.type = "my";
                            this.storage.set(base_storage_keys.my_geo, this.location);
                        } else if (!this.location.latitude || !this.location.longitude) {
                            this.location = this.SanFrancisco;
                        }
                        resolve();
                    });
            } else resolve();
        });
    }

    private isLatitudeLongitudeChanged(lat: number, lon: number): boolean {
        if (Math.round(lat*10) != Math.round(this.location.latitude*10)) return true;
        if (Math.round(lon*10) != Math.round(this.location.longitude*10)) return true;
        return false;
    }
}
