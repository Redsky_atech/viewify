import {Injectable} from "@angular/core";
import {ChromeStorageService} from "../storage/services/chrome-storage.service";
import {base_storage_keys} from "../model/storage-keys";
import {take} from "rxjs/internal/operators";
import {ErrorModel} from "./model/error.model";
import {FireStoreService} from "../../firebase/fire-store.service";
import {AngularFireAuth} from "@angular/fire/auth";

@Injectable()
export class LogService {
    //readonly errorUrl = 'https://api.dashify.me/logs/write-errors.php';
    //readonly dataUrl = 'https://api.dashify.me/logs/write-storage.php';

    constructor(private storage: ChromeStorageService,
                private fireSvc: FireStoreService,
                private auth: AngularFireAuth) {
    }

    sendToServerInInterval() {
        if (this.auth.auth.currentUser) {
            this.storage.get(base_storage_keys.logdata_last)
                .pipe(take(1))
                .subscribe((time: number) => {
                    if (!time) {
                        this.storage.set(base_storage_keys.logdata_last, new Date().getTime());
                    } else {
                        //send log to firestore every 24 hours
                        if (((new Date().getTime()-time) > 1000*60*60*24) && this.fireSvc.authFire.auth.currentUser) {
                            this.storage.get(base_storage_keys.logdata_errors)
                                .pipe(take(1))
                                .subscribe((errors: ErrorModel[]) => {
                                    if (errors && errors.length > 0) this.sendErrors(errors);
                                });

                            //get storage data from fire cache
                            this.fireSvc.getBackupData()
                                .pipe(take(1))
                                .subscribe((data: any) => {
                                    this.storage.get(base_storage_keys.logdata_storage)
                                        .pipe(take(1))
                                        .subscribe((localLog: any) => {
                                            //if here already is stored some local log, then push log to this array and send to firestore
                                            if (localLog && localLog.length > 0) {
                                                if (localLog.length > 4) localLog.shift();
                                                localLog.push({time: new Date().toUTCString(), data: data});

                                                this.storage.set(base_storage_keys.logdata_storage, localLog);
                                                this.sendStorage(localLog);
                                            } else {
                                                //if here isn't any stored log locally, then try to get it from firestore and update both storage
                                                this.fireSvc.getSingleDocData("log")
                                                    .then((fireLog: any) => {
                                                        if (fireLog && fireLog.log.length > 0) {
                                                            if (fireLog.log.length > 4) fireLog.log.shift();
                                                        } else {
                                                            fireLog.log = [];
                                                        }
                                                        fireLog.log.push({time: new Date().toUTCString(), data: data});
                                                        this.storage.set(base_storage_keys.logdata_storage, fireLog.log);
                                                        this.sendStorage(fireLog);
                                                    });
                                            }
                                        });
                                });


                            this.storage.set(base_storage_keys.logdata_last, new Date().getTime());
                        }
                    }
                });
        }
    }

    logErrors(message: string, stack: string) {
        if (this.auth.auth.currentUser) {
            this.storage.get(base_storage_keys.logdata_errors)
                .pipe(take(1))
                .subscribe((errors: ErrorModel[]) => {
                    if (!errors) errors = [];
                    if (!errors.find(err => {
                            if (err.stack && stack) return err.stack.substring(0, 50) == stack.substring(0, 50);
                            else if (err.message && message) return err.message == message;
                        })) {
                        if (errors.length >= 10) errors.shift();
                        errors.push({message: message, stack: stack, time: new Date().toUTCString()});
                    } else errors.find(err => {
                        if (err.stack && stack) return err.stack.substring(0, 50) == stack.substring(0, 50);
                        else if (err.message && message) return err.message == message;
                    }).time = new Date().toUTCString();
                    this.storage.set(base_storage_keys.logdata_errors, errors);
                });
        }
    }

    private sendErrors(err: any[]) {
        this.fireSvc.setDocData("errors", {errors: err});
    }

    private sendStorage(data: any[]) {
        this.fireSvc.setDocData("log", {log: data});
    }
}
