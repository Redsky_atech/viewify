import {ErrorHandler, Injectable, Injector} from "@angular/core";
import {LogService} from "./log.service";

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
    private logSvc: LogService;
    constructor(private inj: Injector){}
    handleError(error: any): void {
        console.error(error);
        this.logError(error);
    }

    private logError(error: any) {
        if (!this.logSvc) this.logSvc = this.inj.get(LogService);
        let stack;
        if (error.stack) stack = error.stack.length < 1000? error.stack: error.stack.substring(0, 1000);
        this.logSvc.logErrors(error.message, stack);
    }
}
