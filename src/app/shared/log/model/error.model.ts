export class ErrorModel {
    message: string;
    stack: string;
    time: string;
}
