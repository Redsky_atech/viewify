import {Injectable} from "@angular/core";
import {MatSnackBar} from "@angular/material";

@Injectable()
export class SnackService {
    constructor(private snackbar: MatSnackBar){}

    public openSnack(message: string, action: string, styleClass: string) {
        this.snackbar.open(message, action, {
            duration: 4000,
            panelClass: ['snack-bar_base', styleClass]
        });
    }
}
