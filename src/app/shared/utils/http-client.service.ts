import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpClientService {

  constructor(private _http: HttpClient) { }

  getData(url: string): Observable<any> {
    return this._http.get(url).pipe(
      map(response => response || {})
    ); 
  }
}
