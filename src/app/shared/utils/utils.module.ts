import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UtilsService} from './utils.service';
import {SnackService} from "./snack.service";

@NgModule({
  imports: [
    CommonModule,
  ],
    providers: [
        UtilsService,
        SnackService
    ],
  declarations: [
  ],
  exports: [
  ]
})
export class UtilsModule { }
