import {Injectable} from "@angular/core";
import {Observable, Subject} from "rxjs/Rx";
import {base_storage_keys} from "../model/storage-keys";
import {take} from "rxjs/internal/operators";
import {ChromeStorageService} from "../storage/services/chrome-storage.service";
import {AngularFireAuth} from "@angular/fire/auth";

@Injectable()
export class SharedService {
    private reloadViewSubj: Subject<string> = new Subject<string>();
    todoSkipSave: boolean = false;
    constructor(private storage: ChromeStorageService,
                private afAuth: AngularFireAuth) {
        if (afAuth.auth.currentUser) {
            this.todoSkipSave = true;
        }
    }

    listenViewChanges(): Observable<string> {
        return this.reloadViewSubj.asObservable();
    }
    reloadView(name: string) {
        this.reloadViewSubj.next(name);
    }

    syncKey(key: string): Promise<any> {
        return new Promise<any>(resolve => {
            this.storage.get(base_storage_keys.sync_keys)
                .pipe(take(1))
                .subscribe((k: any) => {
                    let keys = k;
                    if (!keys) keys = {};
                    keys[key] = 'save';
                    this.storage.set(base_storage_keys.sync_keys, keys);
                    resolve();
                }, error => resolve());
        });
    }
}
