import {Observable, Subject} from 'rxjs';

import {EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';

import { AppStorageKey } from '../storage/models/app-storage-key';
import { AppStorageService } from '../storage/services/app-storage.service';
import { WidgetDefaultSettings } from './widget-default-settings';
import { WidgetSettings } from './widget-settings';
import { WidgetType } from './widget-type';
import {WidgetService} from "../../widgets/widget/widget.service";
import {DomSanitizer, SafeResourceUrl} from "@angular/platform-browser";
import {take} from "rxjs/internal/operators";
import {Subscription} from "rxjs/Rx";
import {FireStoreService} from "../../firebase/fire-store.service";
import {base_storage_keys} from "../model/storage-keys";
import {TabsService} from "../storage/services/tabs.service";

export abstract class WidgetBaseComponent<Settings extends WidgetSettings> implements OnInit, OnDestroy {
    @Input() key: string;
    @Input() viewKey: string;
    @Input() type: WidgetType;
    @Input() defaultSettings: WidgetDefaultSettings;
    @Input() frozen = false;
    @Input() hide: Observable<any>;
    @Input() remove: Observable<any>;
    @Input() open: Observable<any>;
    @Input() opened: () => any;
    @Input() userData: any;
    @Input() position: string;
    @Input() column: number;
    @Input() isFromAddPopUp: boolean;

    @Output() userDataChanged = new EventEmitter<string>();
    @Output() hideWidget = new EventEmitter<void>();
    //using only on creating widget component
    minimizedOnStart: Subject<any> = new Subject<any>();
    url: SafeResourceUrl;
    initialized: boolean = false;
    maxHeaderLength: number = 19;
    protected multiWidget: boolean = true;
    private syncTab: boolean = false;
    public doubleWidth: boolean = false;
    public syncable: boolean = false;
    public settingShowHeader: boolean;
    public settingHeaderName: string = '';
    service: WidgetService<Settings>;

    public settings: Settings = <Settings>{ headerName: '', showHeader: false, minimized: false };

    public storageKey: AppStorageKey<Settings>;
    protected rootSubs: Subscription = new Subscription();
    private tabSub: Subscription = new Subscription();

    constructor(protected appStorage: AppStorageService,
                protected fireStore: FireStoreService,
                protected tabsSvc: TabsService) {}

    ngOnDestroy(): void {
        this.rootSubs.unsubscribe();
        this.tabSub.unsubscribe();
    }

    ngOnInit(): void {
        if (!this.key) {
            throw Error('Widget key is missing!');
        }
        /*if (this.type != 'on-adding') {

        }*/
        if (this.userData) {
            this.storageKey = new AppStorageKey<Settings>(`${this.userData}-settings`);
        } else {
            if (!this.multiWidget || this.key == 'fixed-w-todo') {
                this.userData = this.key;
                this.userDataChanged.emit(this.userData);
                this.storageKey = new AppStorageKey<Settings>(`${this.key}-settings`);
            } else {
                this.userData = this.key+'-'+this.uniqueId();
                this.userDataChanged.emit(this.userData);
                this.storageKey = new AppStorageKey<Settings>(`${this.userData}-settings`);
            }
        }

        if (this.type == 'icon') {
            if (this.key == 'fixed-w-todo' || this.key == 'w-weather') {
                /*this.tabSub.add(this.tabsSvc.addTabObserver(this.key)
                    .subscribe(() => {
                        this.initialized = false;
                        this.syncTab = true;
                        this.widgetInit();
                    }));*/
                this.widgetInit();
            }
        } else {
            this.widgetInit();
        }

        if (this.multiWidget || this.key == 'fixed-w-todo' || this.key == 'w-calendar') this.listenTabSync();
    }

    public abstract widgetInit(): void;
    public abstract reload(): void;
    //public abstract beforeTabSync(): void;

    private listenTabSync() {
        //this.rootSubs.unsubscribe();
        //this.rootSubs = new Subscription();
        //this.beforeTabSync();
        this.tabSub.add(this.tabsSvc.addTabObserver(this.storageKey.keyName)
            .subscribe(() => {
                //this.initialized = false;
                this.syncTab = true;
                this.startWork();
            }));
    }

    /*public syncSettingsStorage(): void {
        this.appStorage.set<Settings>(this.storageKey, this.settingsStorage);
    }*/

    public onOpened(): () => void {
        return () => {
            this.opened();
        };
    }

    public saveSettings(): void {
        if (this.key == 'fixed-w-todo') this.tabsSvc.addChangedTabKey(this.key);
        else if (this.multiWidget || this.key == 'fixed-w-todo' || this.key == 'w-calendar') {
            //this.tabsSvc.addChangedTabKey(this.viewKey);
            this.tabsSvc.addChangedTabKey(this.storageKey.keyName);
        }
        if (this.settingHeaderName) this.settings.headerName = this.settingHeaderName;
        this.settings.showHeader = this.settingShowHeader;
        this.appStorage.set<Settings>(this.storageKey, JSON.parse(JSON.stringify(this.settings)));
        if (!this.syncable && this.fireStore.authFire.auth.currentUser) {
            this.syncable = true;
            this.fireStore.storage.get(base_storage_keys.sync_keys)
                .pipe(take(1))
                .subscribe((k: any) => {
                    let keys = k;
                    if (!keys) keys = {};
                    keys[this.storageKey.keyName] = 'save';
                    this.fireStore.storage.set(base_storage_keys.sync_keys, keys);
                });
        }
    }

    public onMinimize(value): void {
        this.settings.minimized = value;
        this.saveSettings();
    }

    public lengthLimit() {
        if (this.settingHeaderName.length > this.maxHeaderLength) {
            this.settingHeaderName = this.settingHeaderName.substring(0, this.maxHeaderLength);
        }
    }

    public listenSync () {
        if (this.fireStore.authFire.auth.currentUser) {
            this.rootSubs.add(this.fireStore.syncWithFire.subscribe((reset) => {
                if (reset.type == "marker") {
                    this.syncable = false;
                } else if (reset.type == "data" && (!reset.keys || reset.keys.includes(this.storageKey.keyName))) {
                    this.startWork();
                }
            }));
        }
    }

    protected startWork(): Promise<any> {
        return new Promise<any>(resolve => {
            this.appStorage.get(this.storageKey)
                .pipe(take(1))
                .subscribe(storageSettings => {
                    if (this.defaultSettings) {
                        this.settings.showHeader = this.defaultSettings.showHeaderByDefault;
                        this.settings.headerName = this.defaultSettings.headerNameByDefault;
                    } else {
                        this.settings.showHeader = false;
                        this.settings.headerName = "";
                    }

                    if (storageSettings) {
                        this.settings = Object.assign(this.settings, storageSettings);
                    }

                    this.settingShowHeader = this.settings.showHeader;
                    this.settingHeaderName = this.settings.headerName;

                    /*if (!this.service.isWorking) {
                        setTimeout(() => {
                            this.service.isWorking = true;
                        }, 500);
                        this.service.startWork();
                    }*/
                    if (!this.syncTab) this.minimizedOnStart.next(this.settings.minimized);
                    resolve();
                }, error => {
                    resolve()
                });
        });
    }

    public stretchWindow(target) {
        this.doubleWidth = !this.doubleWidth;

        let width = window.innerWidth;
        let stretchLeft = false;
        if (width > 1120 && this.column == 3) stretchLeft = true;
        else if ((width < 1120 && width > 780) && this.column == 2) stretchLeft = true;
        else if (width < 780 && this.column == 1) stretchLeft = true;

        if (this.type == WidgetType.icon) {
            let elem = $(target).closest('.app-widget-modal-overlay');
            if (this.doubleWidth) {
                elem.addClass('icon-widget-doubleWidth');
            } else {
                elem.removeClass('icon-widget-doubleWidth');
            }
        } else {
            let elem = $(target).closest('.column-layer');
            if (this.doubleWidth) {
                elem.addClass('widget-doubleWidth');
                elem.find('.drag-icon').css('z-index', '-999');
                if (stretchLeft) elem.addClass('widget-doubleWidth-left');
            } else {
                elem.removeClass('widget-doubleWidth');
                elem.find('.drag-icon').css('z-index', '999');
                if (stretchLeft) elem.removeClass('widget-doubleWidth-left');
            }
        }
    }

    protected uniqueId() {
        return Math.random().toString(36).substr(2, 6);
    }

    public showIframe(settings: any, sanitizer: DomSanitizer): void {
        if (settings.url) {
            this.url = sanitizer.bypassSecurityTrustResourceUrl(settings.url);
            //this.cdr.markForCheck();
        }
    }
}
