export enum WidgetType {
    icon = 'icon',
    full = 'full',
    settings = 'settings',
    onAdding = 'on-adding',
    native = 'native',
    web = 'web',
    rss = 'rss',
}
