export class ProfileSettings {
    name: string;
    timeWidget: boolean;
    quoteWidget: boolean;
    search: {
        searchHistory: boolean;
        search: {
            google: boolean;
            images: boolean;
            youtube: boolean;
            news: boolean;
            bing: boolean;
            yahoo: boolean;
            baidu: boolean;
        },
        shop: {
            amazon: boolean;
            aliexpress: boolean;
            ebay: boolean;
            etsy: boolean;
            walmart: boolean;
        },
        travel: {
            travelocity: boolean;
            expedia: boolean;
        },
        eat: {
            yelp: boolean;
            ubereats: boolean;
            doordash: boolean;
            grubhub: boolean;
            postmates: boolean;
        },
        knowledge: {
            wikipedia: boolean;
            quora: boolean;
            scholar: boolean;
        }
    }
}
