//local keys
export const image_storage_keys = {
    settings: "bg-image-settings",
    savedImgs: "savedIMGs",
    tomorrow: "tomorrowIMGs",
    today: "todayIMGs",
    oneToday: "imageForToday",
    ownDaily: "daily-uploaded-image",
    flickrDaily: "daily-flickr-image"
}

export const widget_storage_keys = {
    geo: "geolocation", //local
    //don't change this key, it will cause an error
    todos: "all-todos-", //local & firestore
    allStockSymbols: "all-stock-symbols", //local
    stockUsersCompanies: "companies", //local & firestore
    weather_temp_type: "widget-weather-temp-type"
}

export const base_storage_keys = {
    all_used_keys: "all_anonymous-user_keys", //local
    first_fire_upload: "fire-extension-version", //firestore
    first_local_upload: "lo-extension-version",//local
    sync_last: 'last_synchronization', //local
    sync_keys: "sync_keys", //local
    sync_daily: "daily-sync", //local
    sync_tabs_keys: "sync_tabs_keys", //local
    desk: "desk", //local & firestore
    fullView_key: "full-view-", //local & firestore
    iconView_key: "icon-view-", //local & firestore
    search_bar: "categories", //local & firestore
    search_history: "search_history",
    pages: "all-pages-data", //local & firestore
    isAnonymous: "logged-anonymously", //local
    logdata_last: "last-send_error-storage", //local
    logdata_errors: "error-log-data", //local & firestore
    logdata_storage: "storage-log-data", //local & firestore
    links: "quick-links", //local & firestore
    calendar: "calendar_events", //local & firestore
    my_geo: "my-geolocation", //local
    profile: "profile-settings", //local & firestore
    register_time: "registration-time" //local & firestore
}

export const default_storage_keys = {
    icon_left: "icon-view-left", //local & firestore
    icon_bottom: "icon-view-bottom", //local & firestore
    full_0: "full-view-0", //local & firestore
    full_1: "full-view-1", //local & firestore
    full_2: "full-view-2", //local & firestore
    full_3: "full-view-3", //local & firestore
    full_4: "full-view-4", //local & firestore
    full_5: "full-view-5", //local & firestore
    fixed_todo: "fixed-w-todo-settings" //local & firestore
}
